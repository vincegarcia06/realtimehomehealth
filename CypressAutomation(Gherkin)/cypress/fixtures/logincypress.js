describe('Medisource Workflow Login Page', () => {

    it('Login the Account', function() 
    {
        cy.visit('https://qado.medisource.com/login'); //visit page            
        cy.get('#loginemail').type('vincent@geeksnest'); //input username
        cy.get('#loginpassword').type('Tester2021!'); //input password
        cy.get('.btn').click(); //click Login Button

        cy.url().should('be.equal', 'https://qado.medisource.com/dashboard') //Validation if redirects to Dashboard    
})

})