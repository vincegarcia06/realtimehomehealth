Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport
Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});


// Given('I Login', () => {
//   cy.login('superagent@geekers', 'Tester2021@'); //cy.login - command located in command.js
//             cy.viewport(1920, 924);//setting your windows size
//   })

//   And('I visit admission page and Modal will Display', () => {
//     cy.visit('https://qado.medisource.com/patient'); //visit page
//     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('be.exist')
//   });

//   When('I click skip', () => { 
//         cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div > div:nth-child(3) > div > a').click()
//   })
  
//   Then('Modal will disappear', () => {
//     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div').should('not.be.exist')
//   });

//   When('I input referral date and hours, click auto assign and input planned SOC date', () => { 
//     const dayjs = require('dayjs')
//     cy.get('#refDate').type('05132022')
//     cy.get('#referral_time > input').type('1200')
//     cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td.global__table-question > div > label > input').click()
//     cy.get('#pre_admission_date').type(dayjs().format('MM/DD/YYYY'));
//   })

//   And('I input in information', (datatable) => { 
//     datatable.hashes().forEach(element => {
//       cy.get('#last_name').type(element.firstname);
//       cy.get('#first_name').type(element.lastname);
//       cy.get('#mi').type(element.middleinitial);
//       cy.get('#suffix').type(element.suffix);
//       cy.get('#birthdate').type(element.birthdate);
//       cy.wait(5000)
//     })
//   })

//   And('I check male', () =>{
//       cy.get('.custom-input-radio > :nth-child(1) > .ng-pristine').check();
//   })

//   And('I select marital status "Single", ethnicity "Asian" and language "English"', () =>{
//     cy.get('#marital_status_chosen > .chosen-single').click();
//     cy.get('#marital_status_chosen > div > ul > li:nth-child(1)').click();
//     cy.get('#ethnicities_chosen > .chosen-choices > .search-field > .default').click();
//     cy.get('#ethnicities_chosen > div > ul > li:nth-child(1)').click();
//     cy.get('#language_spoken > div:nth-child(1) > input').click()
//     cy.get('#ui-select-choices-row-0-0 > span').click()
//     cy.wait(5000)
//   })
//   And('I input Social Security Number', () =>{
//     cy.get('#ssNumber').type('111111111');
// })

// And('I input in address', (datatable) => { 
//   datatable.hashes().forEach(element => {
//     cy.get('#main_line1').type(element.streetaddress);
//     cy.get('#main_line2').type(element.additionaldirections);
//     cy.get('#main_street').type(element.majorcrossstreet);
//     cy.get('#main_city').type(element.city);
//   })
// })


Given('I Login 1', () => {
  cy.login1('superagent@geekers', 'Tester2021@'); //cy.login - command located in command.js
            cy.viewport(1920, 924);//setting your windows size
  })

  When('I Visit Patient List', () => {
  cy.visit('https://app.medisource.com/patient/admitted'); //visit patient list
     cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
     cy.wait(5000) 
     cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
     cy.wait(5000) 
  });
  
  When('I search the patient "WoundAutomated123"', () => {
    cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
    cy.wait(10000)
  });

  And('Click the patient', () => {
    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
    cy.wait(5000)
  });

  And('I click wound management tab', () => {
    cy.get('#profile-main-header > div > ul > li:nth-child(8) > a').click();//click wound mgmt tab
    cy.wait(5000)
  });

  And('I click OASIS wound reference', () => {
    cy.get('.sampletd > :nth-child(2)').click();//click oasis wound reference or the first onthe list
    cy.wait(5000)
  });

  And('I click Edit Button', () => {
    cy.get('tr > :nth-child(2) > .btn').click();//click EDIT button
    cy.wait(5000)
  });

  And('I click on human image or pin wound', () => {
    cy.get('#dragball > div').click(374, 251);//click on human image or pin wound
    cy.wait(5000)
  });

  And('I click yes', () => {
    cy.get('.mhc__wound > .mhc__btn-affirmative').click();//click yes
    cy.wait(5000)
  });

  And('Select location and wound type', () => {
    //click location dropdown
    cy.get('.li-location > .labeled-height > fieldset > table-inputs-v4.ng-pristine > .input-drp > .fg-line > .global__txtbox').click();
    //select location
    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div > div > table-column-v4 > ul > li.li-height-0.li-cont.li-location > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click();
    //click wound type dropdown
    cy.get('.li-woundtype > .labeled-height > fieldset > table-inputs-v4.ng-pristine > .input-drp > .fg-line > .global__txtbox').click()
    //select wound type
    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div > div > table-column-v4 > ul > li.li-height-0.li-cont.li-woundtype > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click()
    cy.wait(5000)
  });

  And('Upload wound image', () => {
    const imgFile = 'wound.png';
    cy.get('#myImg').attachFile(imgFile , { subjectType: 'drag-n-drop' }) //Drop and Drop uploading of an image
    cy.wait(20000)
    cy.scrollTo(0, 1500)
  });

  And('I click digital measurement', () => {
    cy.scrollTo(0, 1500)
    cy.get('[style="display: flex;justify-content: center; left: 1.5%; top: 100%;"] > .col-sm-12 > .p-l-0 > .text-center > .m-l-0').click({ force: true }) //clicking digital measurement
    cy.wait(10000)
  });

  And('I click edit digital measurement', () => {
    cy.get('[style="display: flex;justify-content: center; left: 1.5%; top: 100%;"] > .col-sm-12 > .p-l-0 > .text-center > .m-l-0').click({ force: true }) //clicking edit digital measurement
  });

  Then('Modal will display for wound digital instrument', () => {
    cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form').should('be.exist')
    cy.wait(10000)
  });

  When('I trace the wound image', () => {
    cy.get('#canvas')
    .click(480, 155, { force: true })
    .click(580, 152, { force: true })
    .click(680, 175, { force: true })
    .click(780, 235, { force: true })
    .click(815, 310, { force: true })
    .click(820, 340, { force: true })
    .click(780, 420, { force: true })
    .click(700, 480, { force: true })
    .click(620, 525, { force: true })
    .click(540, 550, { force: true })
    .click(480, 560, { force: true })
    .click(420, 555, { force: true })
    .click(320, 545, { force: true })
    .click(220, 500, { force: true })
    .click(160, 460, { force: true })
    .click(138, 400, { force: true })
    .click(135, 370, { force: true })
    .click(140, 330, { force: true })
    .click(150, 300, { force: true })
    .click(190, 260, { force: true })
    .click(250, 220, { force: true })
    .click(310, 190, { force: true })
    .click(360, 170, { force: true })
    .click(480, 155, { force: true })
    cy.wait(10000)
  });

  And('I click save Button', () => {
    cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form > div > div.modal-body.p-10 > div > div > table > tbody > tr > td:nth-child(2) > div.text-right.p-0.m-l-50.m-t-10 > button.btn.mhc__btn-affirmative.btn__success.waves-effect').click() //clicking save button on wound digital measurements 
  });

  Then('Modal will disapper', () => {
    cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form').should('not.be.exist')
  });

  And('It will notify wound digital measurement has been set', () => {
    cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated > span:nth-child(4)').should('be.exist')
  });
  

  And('I click Save Button for wound#1', () => {
    cy.get('#tdTitleAction > td:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click({ multiple: true }) //clicking save button
    cy.wait(5000)
  });

  Then('It should display notification "successfully save"', () => {
    cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated').should('be.visible')
  });
    
// Given('I Login 2', () => {
//   cy.login('vincegarcia@geekers', 'Tester2021@'); //cy.login - command located in command.js
//             cy.viewport(1920, 924);//setting your windows size
//   })

//   When('I Visit Patient List', () => {
//   cy.visit('https://app.medisource.com/patient/admitted'); //visit patient list
//      cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
//      cy.wait(5000) 
//      cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
//      cy.wait(5000) 
//   });
  
//   When('I search the patient "WoundAutomated123"', () => {
//     cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
//     cy.wait(10000)
//   });

//   And('Click the patient', () => {
//     cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
//     cy.wait(10000)
//   });

//   And('I click wound management tab', () => {
//     cy.get('#profile-main-header > div > ul > li:nth-child(8) > a').click();//click wound mgmt tab
//     cy.wait(5000)
//   });

//   And('I click OASIS wound reference', () => {
//     cy.get('.sampletd > :nth-child(2)').click();//click oasis wound reference or the first onthe list
//     cy.wait(5000)
//   });

//   And('I click Edit Button', () => {
//     cy.get('tr > :nth-child(2) > .btn').click();//click EDIT button
//     cy.scrollTo(0, 200)
//     cy.wait(10000)
//     cy.get('#goTabSection > div > wound-table-v4 > div.text-center.w-100.ng-scope > div.btn-group.tab-btn-group.btn__tab_group.text-center.p-t-5.p-b-5 > label:nth-child(2)').click()
//     cy.wait(5000)
//     cy.scrollTo(0, 1500)
//   });

//   And('I click on human image or pin wound', () => {
//     cy.get('#dragball > div').click(550, 251, { force: true });//click on human image or pin wound
//     cy.wait(5000)
//   });

//   And('I click yes', () => {
//     cy.get('.mhc__wound > .mhc__btn-affirmative').click();//click yes
//     cy.wait(5000)
//   });

//   And('Select location and wound type', () => {
//     //click location dropdown
//     cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-location > span > fieldset > table-inputs-v4 > div > div.fg-line > input').click()    //select location
//     cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-location > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click();
//     //click wound type dropdown
//     cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-woundtype > span > fieldset > table-inputs-v4 > div > div.fg-line > input').click()    //select wound type
//     cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-woundtype > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click()
//     cy.wait(5000)
//   });

//   And('Upload wound image', () => {
//     const imgFile = 'wound1.png';
//     cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-1.li-cont.li-img-block.li-photo > table-column-photo-v4')
//     .invoke('show') // call jquery method 'show' on the '#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-1.li-cont.li-img-block.li-photo > table-column-photo-v4'
//     .should('be.visible') // element is visible now
//     .find('#myImg') // drill down into a child "img" element
//     .attachFile(imgFile , { subjectType: 'drag-n-drop' }, {force: true}) //Drop and Drop uploading of an image // drill down into a child "input" element
//     cy.wait(20000)
//     cy.scrollTo(0, 1500)
//   });

//   And('I click digital measurement', () => {
//     cy.scrollTo(0, 1500)
//     cy.get('[style="display: flex;justify-content: center; left: 1.5%; top: 100%;"] > .col-sm-12 > .p-l-0 > .text-center > .m-l-0').eq(1).click({ force: true}) //clicking digital measurement
//     cy.wait(10000)
//   });

//   And('I click edit digital measurement', () => {
//     cy.scrollTo(0, 1500)
//     cy.get('[style="display: flex;justify-content: center; left: 1.5%; top: 100%;"] > .col-sm-12 > .p-l-0 > .text-center > .m-l-0').eq(1).click({ force: true }) //clicking edit digital measurement
  
//   });

//   Then('Modal will display for wound digital instrument', () => {
//     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form').should('be.exist')
//     cy.wait(5000)
//   });

//   When('I trace the wound image', () => {
//     cy.get('#canvas')
//     .click(480, 200, { force: true })
//     .click(560, 201, { force: true })
//     .click(640, 220, { force: true })
//     .click(705, 275, { force: true })
//     .click(725, 325, { force: true })
//     .click(725, 375, { force: true })
//     .click(690, 425, { force: true })
//     .click(640, 470, { force: true })
//     .click(580, 500, { force: true })
//     .click(520, 525, { force: true })
//     .click(460, 535, { force: true })
//     .click(400, 530, { force: true })
//     .click(340, 515, { force: true })
//     .click(278, 488, { force: true })
//     .click(255, 465, { force: true })
//     .click(230, 420, { force: true })
//     .click(220, 380, { force: true })
//     .click(223, 340, { force: true })
//     .click(235, 300, { force: true })
//     .click(270, 270, { force: true })
//     .click(330, 235, { force: true })
//     .click(400, 215, { force: true })
//     .click(480, 200, { force: true })
//     cy.wait(10000)
//   });

//   And('I click save Button', () => {
//     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form > div > div.modal-body.p-10 > div > div > table > tbody > tr > td:nth-child(2) > div.text-right.p-0.m-l-50.m-t-10 > button.btn.mhc__btn-affirmative.btn__success.waves-effect').click() //clicking save button on wound digital measurements 
//   });

//   Then('Modal will disapper', () => {
//     cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form').should('not.be.exist')
//   });

//   And('It will notify wound digital measurement has been set', () => {
//     cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated > span:nth-child(4)').should('be.exist')
//   });
  

//   And('I click Save Button for wound#2', () => {
//     cy.get('#tdTitleAction > td:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click({ multiple: true }) //clicking save button
//     cy.wait(5000)
//   });

//   Then('It should display notification "successfully save"', () => {
//     cy.get('body > div.alert.alert-success.alert-dismissable.growl-animated').should('be.visible')
//   });