Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of ST - Initial Eval', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Peralta M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                
                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(15) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click PT - Initial Eval
        })
           
        it('Automating Entry Form of Assessment Tab', function() {
            cy.wait(10000)
            cy.get('#timeIn').type('1200') //inputting time in
            cy.get('#timeOut').type('1900') //inputting time out
            cy.get('#Company').type('Test') //inputting company
            cy.wait(2000)
            //Medical Diagnosis
            cy.get('[width="40%"] > .form-control').type('Test')
            cy.get('[width="15%"] > .checkbox > .ng-valid').click() //clicking Exacerbation
            cy.get('[width="10%"] > .checkbox > .ng-pristine').click() //clicking onset
            cy.get('[width="35%"] > .form-control').type('Test')
            //Relevant Medical History and Previous Therapies
            cy.get(':nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Prior Level of Functioning
            cy.get(':nth-child(4) > td > .fg-line > .form-control').type('Test')
            //Patient's Goals
            cy.get(':nth-child(6) > td > .fg-line > .form-control').type('Test')
            //Homebound Status
            cy.get('[rname="hbs_multi"]').click({multiple:true})
            cy.get(':nth-child(5) > :nth-child(1) > .cont-opt > div > .global__txtbox').type('Test') //inputting other
            //Living Arrangements/Caregiver Availability
            cy.get('#parent > div > div.ng-scope.global__form_center-true > div > div.p-15.bg__wt > form > fieldset:nth-child(3) > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > label > input').click() //clicking lives alone
            //Vital Signs
            cy.get('.p-5 > .global__inner-nb > tbody > tr > [style="width: 23%;"] > .cont-opt > div > .global__txtbox').type('120/170') //inputting BP
            cy.get('.p-5 > .global__inner-nb > tbody > tr > :nth-child(2) > .cont-opt > div > .global__txtbox').type('120') //inputting Heart Rate
            cy.get(':nth-child(3) > .cont-opt > div > .global__txtbox').type('120') //inputting Respiration
            cy.get(':nth-child(4) > .cont-opt > div > .global__txtbox').type('120') //inputting o2 Saturation
            //Pain Assesment
            cy.get(':nth-child(2) > .p-r-5 > .cont-opt > div > .global__txtbox').type('Test') //inputting pain location
            cy.get(':nth-child(2) > :nth-child(2) > .cont-opt > :nth-child(1) > .ng-pristine').click() //clicking acute type of pain
            cy.get(':nth-child(3) > td > .cont-opt > :nth-child(2) > .ng-valid').click() //clicking 1 present level
            cy.get('.lopt-left > :nth-child(2) > .ng-valid').click() //clicking yes for Does the pain impact the plan of care?
            cy.get('.m-lopt-240 > .cont-opt > .global__txtbox').type('Test') //inputting Does the pain impact the plan of care?
            //Safe swallowing evaluation?
            cy.get(':nth-child(1) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > :nth-child(2) > .ng-valid').click() //clicking yes
            cy.get('#sse0002').type('05252022') //inputting date
            cy.get(':nth-child(1) > :nth-child(2) > .global__inner-nb > tbody > tr > [style="width: 26%;"] > .cont-opt > .global__txtbox').type('Test') //inputting facility
            cy.get(':nth-child(1) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(4) > .cont-opt > .global__txtbox').type('Test') //inputting MD
            //Video Fluoroscopy?
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > :nth-child(2) > .ng-pristine').click() //clicking yes
            cy.get('#sse0006').type('05252022') //inputting date
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > [style="width: 26%;"] > .cont-opt > .global__txtbox').type('Test') //inputting facility
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(4) > .cont-opt > .global__txtbox').type('Test') //inputting MD
            cy.get('.global__inner-wb > :nth-child(1) > :nth-child(3) > :nth-child(2) > .global__txtbox').type('Test') //inputting Current diet texture
            cy.get(':nth-child(4) > :nth-child(2) > .global__inner-nb > tbody > tr > [style="width: 23%;"] > .m-r-15 > .ng-valid').click() //clicking thin
            cy.get('[style="width: 38%;"] > .cont-opt > .global__txtbox').type('Test') //inputting specify
            cy.get(':nth-child(4) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(3) > .cont-opt > .global__txtbox').type('Test') //inputting other

            //Speech/Language Evaluation
            //Cognition Function
            cy.get(':nth-child(5) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Orientation (Time/Person/Place)
            cy.get(':nth-child(5) > :nth-child(3) > .checkbox > .ng-valid').click() //clicking tx Orientation (Time/Person/Place)
            cy.get(':nth-child(6) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Attention span
            cy.get(':nth-child(6) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Attention span
            cy.get(':nth-child(7) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Short term memory
            cy.get(':nth-child(7) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Short term memory
            cy.get(':nth-child(8) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Long term memory
            cy.get(':nth-child(8) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Long term memory
            cy.get(':nth-child(9) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Judgement
            cy.get(':nth-child(9) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Judgement
            cy.get(':nth-child(10) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Problem solving
            cy.get(':nth-child(10) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Problem solving
            cy.get(':nth-child(11) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Organization
            cy.get(':nth-child(11) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Organization
            cy.get(':nth-child(12) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(12) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(12) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get('[rowspan="8"] > .form-control').type('Test') //inputting notes

            //Swallowing Function
            cy.get(':nth-child(14) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Chewing ability
            cy.get(':nth-child(14) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Chewing ability
            cy.get(':nth-child(15) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Oral stage management
            cy.get(':nth-child(15) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Oral stage management
            cy.get(':nth-child(16) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Pharyngeal stage management
            cy.get(':nth-child(16) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Pharyngeal stage management
            cy.get(':nth-child(17) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Reflex time
            cy.get(':nth-child(17) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Reflex time
            cy.get(':nth-child(18) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(18) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(18) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get(':nth-child(14) > [rowspan="5"] > .form-control').type('Test') //inputting notes

            //Speech/Voice Function
            cy.get(':nth-child(20) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Oral/Facial exam
            cy.get(':nth-child(20) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Oral/Facial exam
            cy.get(':nth-child(21) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Articulation
            cy.get(':nth-child(21) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Articulation
            cy.get(':nth-child(22) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Prosody
            cy.get(':nth-child(22) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Prosody
            cy.get(':nth-child(23) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Voice/Respiration
            cy.get(':nth-child(23) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Voice/Respiration
            cy.get(':nth-child(24) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Speech intelligibility
            cy.get(':nth-child(24) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Speech intelligibility
            cy.get(':nth-child(25) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(25) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(25) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get(':nth-child(20) > [rowspan="6"] > .form-control').type('Test') //inputting notes

            //Verbal Expression
            cy.get(':nth-child(27) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Augmentative methods
            cy.get(':nth-child(27) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking txAugmentative methods
            cy.get(':nth-child(28) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Naming
            cy.get(':nth-child(28) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Naming
            cy.get('.cont-opt > .m-r-15 > .ng-pristine').click() //clicking yes Appropriate
            cy.get(':nth-child(29) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Appropriate
            cy.get(':nth-child(29) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Appropriate
            cy.get(':nth-child(30) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Complex sentences
            cy.get(':nth-child(30) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Complex sentences
            cy.get(':nth-child(31) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Conversation
            cy.get(':nth-child(31) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Conversation
            cy.get(':nth-child(32) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(32) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(32) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get(':nth-child(27) > [rowspan="6"] > .form-control').type('Test') //inputting notes

            //Auditory Comprehension
            cy.get(':nth-child(34) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Word discrimination
            cy.get(':nth-child(34) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Word discrimination
            cy.get(':nth-child(35) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score One step direction
            cy.get(':nth-child(35) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx One step direction
            cy.get(':nth-child(36) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Two step directions
            cy.get(':nth-child(36) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Two step directions
            cy.get(':nth-child(37) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Complex sentences
            cy.get(':nth-child(37) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Complex sentences
            cy.get(':nth-child(38) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Conversation
            cy.get(':nth-child(38) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Conversation
            cy.get(':nth-child(39) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Speech reading
            cy.get(':nth-child(39) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Speech reading
            cy.get(':nth-child(40) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(40) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(40) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get(':nth-child(34) > [rowspan="7"] > .form-control').type('Test') //inputting notes

            //Reading Function
            cy.get(':nth-child(42) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Letters/Numbers
            cy.get(':nth-child(42) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Letters/Numbers
            cy.get(':nth-child(43) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Words
            cy.get(':nth-child(43) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Words
            cy.get(':nth-child(44) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Simple sentences
            cy.get(':nth-child(44) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Simple sentences
            cy.get(':nth-child(45) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Complex sentences
            cy.get(':nth-child(45) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Complex sentences
            cy.get(':nth-child(46) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(46) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(46) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get(':nth-child(42) > [rowspan="5"] > .form-control').type('Test') //inputting notes

            //Writing Function
            cy.get(':nth-child(48) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Letters/Numbers
            cy.get(':nth-child(48) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Letters/Numbers
            cy.get(':nth-child(49) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Words
            cy.get(':nth-child(49) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Words
            cy.get(':nth-child(50) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Sentences
            cy.get(':nth-child(50) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Sentences
            cy.get(':nth-child(51) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Spelling
            cy.get(':nth-child(51) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Spelling
            cy.get(':nth-child(52) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Formulation
            cy.get(':nth-child(52) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Formulation
            cy.get(':nth-child(53) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score Simple addition/subtraction
            cy.get(':nth-child(53) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx Simple addition/subtraction
            cy.get(':nth-child(54) > .text-left > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(54) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //inputting score other
            cy.get(':nth-child(54) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking tx other
            cy.get(':nth-child(48) > [rowspan="7"] > .form-control').type('Test') //inputting notes

            //Summary of problems identified that need therapeutic intervention
            cy.get(':nth-child(4) > :nth-child(2) > .cont-opt > div > .global__txtbox').type('Test') //inputting other

            //Plan of Care (click all)
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .radio > .ng-valid').click()
            cy.get(':nth-child(3) > :nth-child(1) > .radio > .ng-valid').click()
            cy.get(':nth-child(4) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(8) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(9) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(10) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(11) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(12) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(13) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(14) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(15) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(16) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(17) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(18) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(19) > :nth-child(1) > .radio > .ng-pristine').click()
            //Therapeutic Goals (input all)
            cy.get(':nth-child(2) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(3) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(4) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(5) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(6) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(7) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(8) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(9) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(10) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(11) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(12) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(13) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(14) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(15) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(16) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(17) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(18) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            cy.get(':nth-child(19) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Cognitive Function')
            //Time Frame
            cy.get(':nth-child(2) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(3) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(4) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(5) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(6) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(7) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(8) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(9) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(10) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(11) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(12) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(13) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(14) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(15) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(16) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(17) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(18) > :nth-child(3) > .global__txtbox').type('12')
            cy.get(':nth-child(19) > :nth-child(3) > .global__txtbox').type('12')
            cy.wait(5000)
            
            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.get('.swal2-cancel').click() //clicking no to save information
            cy.wait(10000)
            
    
        })

        it.only('Automating Entry Form of Short Term and Long Term Goals Tab', function() {
            
            cy.get('.btn__success').click() //clicking Save Button
            cy.get('.swal2-cancel').click() //clicking no to save information
            cy.wait(5000)
            cy.get('.pmbb-edit > .btn-group > :nth-child(2)').click() //clicking Short Term and Long Term Goals Tab
            cy.wait(5000)
            
            //Cognition Function
            cy.get(':nth-child(1) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get('.pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get('[ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Swallowing Function
            cy.get(':nth-child(2) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(2) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(2) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(2) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Speech/Voice Function
            cy.get(':nth-child(3) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(3) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(3) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(3) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Verbal Expression
            cy.get(':nth-child(4) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(4) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(4) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(4) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Auditory Comprehension
            cy.get(':nth-child(5) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(5) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(5) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(5) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Reading Function
            cy.get(':nth-child(6) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(6) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(6) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(6) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Writing Function
            cy.get(':nth-child(7) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(7) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(7) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(7) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals
            //Other Problems
            cy.get(':nth-child(8) > .table-responsive > #notestable > tbody > .ng-scope > td > .btn__primary_icon-bordered').click() //clicking add blank row
            cy.get(':nth-child(8) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(1) > .pull-right > .table-input').type('Test') //inputting Problems Identified
            cy.get(':nth-child(8) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(2) > .table-input').type('Test') //inputting Short Term Goals
            cy.get(':nth-child(8) > .table-responsive > #notestable > tbody > [ng-repeat="list in customData"] > :nth-child(3) > .table-input').type('Test') //inputting Long Term Goals

            //Treatment/Skilled interventions provided during this visit
            cy.get(':nth-child(9) > .table-responsive > #notestable > tbody > :nth-child(2) > .p-b-0 > .table-input').type('Test')
            //Additional observations and recommendations
            cy.get(':nth-child(10) > .table-responsive > #notestable > tbody > :nth-child(2) > .p-b-0 > .table-input').type('Test')
            //Patient/Caregiver response to plan of care
            cy.get(':nth-child(11) > .table-responsive > #notestable > tbody > :nth-child(2) > .p-b-0 > .table-input').type('Test')
            cy.wait(5000)
            cy.get(':nth-child(1) > :nth-child(2) > .global__inner-nb > tbody > tr > td > .global__txtbox').type('Test') //inputting Visit frequency and duration
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > td > .global__txtbox').type('Test') //inputting Equipment needed
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr > td > label.radio.radio-inline.m-0.m-r-15.ng-isolate-scope > input').click({force:true}) //clicking good for Rehabilitation potential
            //Discharge plan
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div.lopt-left > label > input').click()
            cy.get('[style="border: none !important;padding-right: 5px; width: 50%;"] > .cont-opt > .global__txtbox').type('Test')
            cy.get(':nth-child(4) > :nth-child(2) > .global__inner-nb > tbody > tr > [style="border: none !important;"] > .cont-opt > .global__txtbox').type('Test')
            //Care coordination with
            cy.get('[opt="data.freq0007"] > .ng-pristine').click()
            cy.get('[opt="data.freq0009"] > .ng-pristine').click()
            cy.get('[opt="data.freq0010"] > .ng-pristine').click()
            cy.get('[opt="data.freq0011"] > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(2) > .global__inner-nb > tbody > tr > [style="border: none !important;"] > .cont-opt > .global__txtbox').type('Test')

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button


        })

    })