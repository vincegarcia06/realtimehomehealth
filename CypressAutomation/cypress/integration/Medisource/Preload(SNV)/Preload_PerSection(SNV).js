Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Load SNV Per Section', () => {
           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('GarciaTest')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)

                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(4) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click LVN/LPN - Wound Visit
                    cy.get('.btn__warning').click({force: true}) //click Edit Button
                    cy.wait(10000)
        })
        
        it('Automating Preload Entry Form of VS/Sensory/Integumentary/Endocrine', function() {
            //Prefill Vital Signs Section
            cy.get('#VS0066_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#VS0066_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Frequency of Pain Section
            cy.get('#PA0036_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#PA0036_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Sensory Status Section
            cy.get('#SENSORY0095_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#SENSORY0095_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Integumentary Status Section
            cy.get('#INTEG0028_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#INTEG0028_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Endocrine System Section
            cy.get('#ENDO0028_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#ENDO0028_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button
            cy.get('.swal2-cancel').click() //clicking access later to cancel assessment

            
        })

        it('Automating Preload Entry Form of Cardiopulmonary/Nutrition/Elimination', function() {
            cy.get(':nth-child(2) > .oasis-section-tabs').click() //clicking Cardiopulmonary/Nutrition/Elimination
            //Prefill When is the patient dyspneic or noticeably Short of Breath?
            cy.get('#RESP0168_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#RESP0168_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load\
            //Prefill Respiratory Status
            cy.get('#RESP0148_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#RESP0148_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Cardiovascular
            cy.get('#CARDIO0110_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#CARDIO0110_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Upper GI Status
            cy.get('#UPGASTRO0022_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#UPGASTRO0022_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Nutrition/Diet
            cy.get('#NUTRI0055_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#NUTRI0055_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Lower GI Status
            cy.get('#GENSTAS0061_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#GENSTAS0061_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Genitourinary Status 
            cy.get('#GENSTAS0061l_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#GENSTAS0061l_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button

            
        })

        it('Automating Preload Entry Form of Neurologic/Musculoskeletal', function() {
            cy.get(':nth-child(3) > .oasis-section-tabs').click() //clicking Neurologic/Musculoskeletal
            //Prefill Neurological Status
            cy.get('#NEUSTAT0053_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#NEUSTAT0053_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Knowledge Deficit
            cy.get('#PSYFAC01001_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#PSYFAC01001_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Thought Process, Affect and Behavioral Status 
            cy.get('#PSYFAC0100_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#PSYFAC0100_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Musculoskeletal Status
            cy.get('#MUSCOLO0064_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#MUSCOLO0064_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button

            
        })

        it.only('Automating Preload Entry Form of Care Management/Interventionsl', function() {
            cy.get(':nth-child(4) > .oasis-section-tabs').click() //clicking Care Management/Interventions
            //Prefill Home Medication Management
            cy.get('#HOMEHEALTH0015_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#HOMEHEALTH0015_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Venous Access and IV Theraphy
            cy.get('#VAAIT0129_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#VAAIT0129_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill DME
            cy.get('#DMEREQ0123_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#DMEREQ0123_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Reason for Home Health
            cy.get('#HOMEHEALTH0013_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#HOMEHEALTH0013_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            //Prefill Homebound Status
            cy.get('#HOMSTA0004_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#HOMSTA0004_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button

            
        })
    })
