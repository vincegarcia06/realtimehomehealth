Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Load SNV Per Tab', () => {
           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('PeraltaTest')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)

                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(4) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click LVN/LPN - Wound Visit
                    cy.get('.btn__warning').click({force: true}) //click Edit Button
                    cy.wait(10000)
        })
        
        it('Automating Preload Entry Form of VS/Sensory/Integumentary/Endocrine', function() {
            //Clicking Prefill for VS/Sensory/Integumentary/Endocrine Tab
            cy.get('#VS0087_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#VS0087_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button
            cy.get('.swal2-cancel').click() //clicking access later to cancel assessment

            
        })

        it('Automating Preload Entry Form of Cardiopulmonary/Nutrition/Elimination', function() {
            //Clicking Prefill for Cardiopulmonary/Nutrition/Elimination
            cy.get(':nth-child(2) > .oasis-section-tabs').click() //clicking Cardiopulmonary/Nutrition/Elimination
            cy.get('#RESP0182_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#RESP0182_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button

            
        })

        it('Automating Preload Entry Form of Neurologic/Musculoskeletal', function() {
            //Clicking Prefill for Neurologic/Musculoskeletal
            cy.get(':nth-child(3) > .oasis-section-tabs').click() //clicking Neurologic/Musculoskeletal
            cy.get('#NEUSTAT0060_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#NEUSTAT0060_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button

            
        })

        it('Automating Preload Entry Form of Care Management/Interventions', function() {
            //Clicking Prefill for Care Management/Interventions
            cy.get(':nth-child(4) > .oasis-section-tabs').click() //clicking Care Management/Interventions
            cy.get('#VAAIT0173_chosen > .chosen-single').click() //select prefill data dropdown
            cy.get('#VAAIT0173_chosen > div > ul > li').click() //select prefill data result
            cy.wait(5000)
            cy.get('.swal2-confirm').click() //clicking yes to load
            cy.get('.btn__success').click() //clicking save button

            
        })
    })
