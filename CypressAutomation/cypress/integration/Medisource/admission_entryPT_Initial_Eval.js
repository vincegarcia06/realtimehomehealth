Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of PT - Initial Eval', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Peralta M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                
                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(13) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click PT - Initial Eval
        })
           
        it('Automating Entry Form of Assessment 1', function() {
            cy.wait(10000)
            cy.get('#visitDate').type('05172022', {force: true}) //inputting visit date
            cy.get('#timeIn').type('1200') //inputting time in
            cy.get('#timeOut').type('1900') //inputting time out
            cy.get('#Company').type('Test') //inputting company
            cy.wait(2000)
            cy.get(':nth-child(2) > .table-input').type('Test') //inputting physical therapht diagnoses
            //Relevant Medical History (Select All)
            cy.get('[rname="rmh_multi"]').click({multiple:true})
            cy.get('[style="width:50%;"] > .m-t-0 > :nth-child(1) > :nth-child(1) > td > .global__txtbox').type('Test') //inputting joint replacement
            cy.get('.m-t-0 > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > tr > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting fall history within 60 days
            cy.get('.m-t-0 > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > tr > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting fall history within last year
            cy.get(':nth-child(2) > .m-t-0 > :nth-child(1) > :nth-child(1) > td > .global__txtbox').type('Test') //inputting contractures
            cy.get('[style="width: 13%"] > .radio > .ng-valid').click() //clicking new
            cy.get(':nth-child(5) > .fg-line > .global__txtbox').type('Test') //inputting location
            //Prior Level of Functioning
            cy.get('[style="width: 50%;"] > .fg-line > .table-input').type('Test')
            //Patient's Goal
            cy.get(':nth-child(2) > .fg-line > .table-input').type('Test')
            cy.wait(5000)
            
            //Homebound Status
            cy.get('[rname="hbs_multi"]').click({multiple:true})
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > div > div > table:nth-child(4) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > div > div > table:nth-child(4) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other
            cy.wait(5000)
            //Patient living situation and availability of assistance (Check one box only)
            cy.get(':nth-child(3) > :nth-child(2) > .radio > .ng-valid').click() //clicking around the clock on patient live alone
            
            //Home environment safety and risk factors predisposing for fall
            cy.get('[rname="hes_multi"]').click({multiple:true , force:true})
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > div > div > table:nth-child(6) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking no hazards identified
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > div > div > table:nth-child(6) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > div > div > table:nth-child(6) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other
            cy.wait(5000)

            //Vitals Sign Information Section
            cy.get(':nth-child(2) > [colspan="2"] > .pull-left > .global__txtbox').type('98') // inputting temperature
            cy.get(':nth-child(2) > :nth-child(3) > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-pristine').click() //clicking oral for temperature
            cy.get(':nth-child(3) > [colspan="2"] > .pull-left > .global__txtbox').type('3') //inputting Pulse/HR
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-nb > tbody > tr > :nth-child(3) > .radio > .ng-pristine').click() //clicking radial for Pulse/HR
            cy.get(':nth-child(4) > [colspan="2"] > .pull-left > .global__txtbox').type('3') //inputting respiration
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-pristine').click() //clicking regular for Pulse/HR
            cy.get('[name="vs0005"]').type('180') //inputting Systolic - BP Left Arm
            cy.get('[name="vs0015"]').type('120')  //inputting Diastolic - BP Left Arm
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-pristine').click() //clicking sitting
            cy.get('[name="vs0007"]').type('180') //inputting Systolic - BP Right Arm
            cy.get('[name="vs0016"]').type('120') //inputting Diastolic - BP Right Arm
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-pristine').click() //clicking sitting
            cy.get(':nth-child(7) > [colspan="3"] > .pull-left > .global__txtbox').type('2') //inputting O2 Saturation % on room air
            cy.get(':nth-child(8) > [colspan="3"] > :nth-child(1) > .global__txtbox').type('2') //inputting O2 Saturation % on O2
            cy.get('.m-l-10 > .global__txtbox').type('2') //inputting lPM on o2 Saturation
            cy.get('.m-l-20 > .ng-valid').click() //clicking nasal cannula
            cy.get('[style="width: 72%;"] > .cont-opt > .global__txtbox').type('Test') //inputting intervention
            cy.get('[chk="data.vs0019"] > .ng-valid').click() //clicking MD
            cy.wait(5000)

            //Physical Assessment
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-valid').click() //clicking WFL for speech
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting speech
            cy.get(':nth-child(3) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-valid').click() //clicking WFL for vision
            cy.get(':nth-child(3) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting vision
            cy.get(':nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for hearing
            cy.get(':nth-child(4) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting hearing
            cy.get(':nth-child(2) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-valid').click() //clicking WFL for skin
            cy.get(':nth-child(2) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting skin
            cy.get(':nth-child(3) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for orientation
            cy.get(':nth-child(3) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting orientation
            cy.get(':nth-child(4) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for cognitive
            cy.get(':nth-child(4) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting cognitive
            cy.wait(5000)
            //Pain Short Terms Goals (STG)
            cy.get(':nth-child(1) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(1) > [style="width: 6%;"] > .fg-line > .global__txtbox').type('2') //inputting Pain Short Terms Goals (STG)
            cy.get(':nth-child(1) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(1) > :nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting comment
            //Pain Long Terms Goals (LTG)
            cy.get(':nth-child(1) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //Pain Long Terms Goals (LTG)
            cy.get(':nth-child(1) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(2) > :nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting comment
            cy.wait(5000)
            
            //Pain Assessment
            cy.get(':nth-child(2) > :nth-child(1) > .cont-opt > .global__txtbox').type('Test') //inputting pain location 1
            cy.get(':nth-child(2) > :nth-child(2) > .select > .select-c > .select_cus').select('Acute') //clicking type 1
            cy.get(':nth-child(2) > :nth-child(3) > .select > .select-c > .select_cus').select('1') //clicking present level 1
            cy.get(':nth-child(2) > :nth-child(4) > .select > .select-c > .select_cus').select('1') //clicking worst level 1
            cy.get(':nth-child(2) > :nth-child(5) > .input-drp-treatment > .fg-line > .table-input').type('Test') //inputting treatment 1
            cy.get(':nth-child(2) > :nth-child(6) > .select > .select-c > .select_cus').select('1') //clicking short term 1
            cy.get(':nth-child(2) > :nth-child(7) > .select > .select-c > .select_cus').select('1') //clicking long term 1
            cy.get(':nth-child(3) > :nth-child(1) > .cont-opt > .global__txtbox').type('Test') //inputting pain location 2
            cy.get(':nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('Acute') //clicking type 2
            cy.get(':nth-child(3) > :nth-child(3) > .select > .select-c > .select_cus').select('1') //clicking present level 2
            cy.get(':nth-child(3) > :nth-child(4) > .select > .select-c > .select_cus').select('1') //clicking worst level 2
            cy.get(':nth-child(3) > :nth-child(5) > .input-drp-treatment > .fg-line > .table-input').type('Test') //inputting treatment 2
            cy.get(':nth-child(3) > :nth-child(6) > .select > .select-c > .select_cus').select('1') //clicking short term 2
            cy.get(':nth-child(3) > :nth-child(7) > .select > .select-c > .select_cus').select('1') //clicking long term 2
            cy.wait(5000)

            //What makes the pain worse?
            cy.get('[rname="pain_worse_multi"]').click({multiple:true})
            cy.get(':nth-child(5) > .cont-opt > div > .global__txtbox').type('Test')
            //What makes the pain better?	
            cy.get('[rname="pain_better_multi"]').click({multiple:true})
            cy.get(':nth-child(7) > .cont-opt > div > .global__txtbox').type('Test')
            cy.wait(5000)

            //Musculoskeletal Assessment: ROM and Strength
            //Short Terms Goals (STG)
            cy.get('.p-0 > .global__inner-nb > tbody > :nth-child(1) > [style="width: 6%;"] > .fg-line > .global__txtbox').type('2') //inputting Short Terms Goals (STG)
            cy.get('.p-0 > .global__inner-nb > tbody > :nth-child(1) > :nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting comment
            //Long Terms Goals (LTG)
            cy.get('.p-0 > .global__inner-nb > tbody > :nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //Pain Long Terms Goals (LTG)
            cy.get('.p-0 > .global__inner-nb > tbody > :nth-child(2) > :nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting comment
            cy.wait(5000)

            //NECK Start---------------------------------------------
            //Neck(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Lateral Flexion
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Rotation
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Neck(Strength)
            //Flexion
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Lateral Flexion
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Rotation
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //NECK End---------------------------------------------

            //SHOULDER Start---------------------------------------------
            cy.get('[title="Shoulder"]').click() //clicking shoulder tab
            //Shoulder(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Abduction
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Adduction
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right            
            //External Rotation
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Internal Rotation
            cy.get(':nth-child(7) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(7) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Shoulder(Strength)
            //Flexion
            cy.get(':nth-child(9) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Abduction
            cy.get(':nth-child(11) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(11) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Adduction
            cy.get(':nth-child(12) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(12) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right            
            //External Rotation
            cy.get(':nth-child(13) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(13) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Internal Rotation
            cy.get(':nth-child(14) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(14) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //SHOULDER End---------------------------------------------

            //ELBOW Start---------------------------------------------
            cy.get('[title="Elbow"]').click() //clicking elbow tab
            //Elbow(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Pronation
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Supination
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right            
            //Elbow(Strength)
            //Flexion
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Pronation
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Supination
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right            
            cy.wait(5000)
            //ELBOW End---------------------------------------------

            //WRIST Start---------------------------------------------
            cy.get('[title="Wrist"]').click() //clicking wrist tab
            //Wrist(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Radial Deviation
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Ulnar Deviation
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right            
            //Wrist(Strength)
            //Flexion
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Radial Deviation
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Ulnar Deviation
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right            
            cy.wait(5000)
            //WRIST End---------------------------------------------

            //Lumbar/Thoracic Start---------------------------------------------
            cy.get('[title="Lumbar/Thoracic"]').click() //clicking Lumbar/Thoracic tab
            //Lumbar/Thoracic(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Lateral Flexion
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Rotation
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Lumbar/Thoracic(Strength)
            //Flexion
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Lateral Flexion
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Rotation
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //Lumbar/Thoracic End---------------------------------------------

            //HIP Start---------------------------------------------
            cy.get('[title="Hip"]').click() //clicking hip tab
            //Hip(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Abduction
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Adduction
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right            
            //External Rotation
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Internal Rotation
            cy.get(':nth-child(7) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(7) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Hip(Strength)
            //Flexion
            cy.get(':nth-child(9) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Abduction
            cy.get(':nth-child(11) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(11) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Adduction
            cy.get(':nth-child(12) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(12) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right            
            //External Rotation
            cy.get(':nth-child(13) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(13) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Internal Rotation
            cy.get(':nth-child(14) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(14) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //HIP End---------------------------------------------

            //KNEE Start---------------------------------------------
            cy.get('[title="Knee"]').click() //clicking knee tab
            //Knee(ROM)
            //Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Knee(Strength)
            //Flexion
            cy.get(':nth-child(5) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(5) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Extension
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(6) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //KNEE End---------------------------------------------

            //ANKLE Start---------------------------------------------
            cy.get('[title="Ankle"]').click() //clicking ankle tab
            //Ankle(ROM)
            //Plant. Flexion
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(2) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            cy.get('#tp_rom_chosen > ul').click() //clicking dropdown
            cy.get('#tp_rom_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Dorsiflexion
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(3) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Inversion
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(4) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Eversion
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('10') //inputting left
            cy.get(':nth-child(5) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('10') //inputting right
            //Ankle(Strength)
            //Plant. Flexion
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > [style="width:22%;"] > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.get('#tp_str_chosen > ul').click() //clicking dropdown
            cy.get('#tp_str_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result
            //Dorsiflexion
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Inversion
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(9) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            //Eversion
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(1) > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(3) > .global__inner-wb > tbody > tr > :nth-child(3) > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //ANKLE End---------------------------------------------

            //SAVE BUTTON
            cy.get('.btn__success').click() //clicking save button
            cy.wait(10000)
        })

        it.only('Automating Entry Form of Assessment 2', function() {
            cy.get('.btn__warning').click() //clickig Edit Button
            cy.get(':nth-child(2) > .oasis-section-tabs').click() //clicking Assessment 2 Tab
            cy.wait(20000)

            //Functional Assessment Codes
            //Short Terms Goals (STG)
            cy.get('[style="width: 6%;"] > .fg-line > .global__txtbox').type('2') //inputting Short Terms Goals (STG)
            cy.get(':nth-child(1) > :nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting comment
            //Long Terms Goals (LTG)
            cy.get('.global__inner-nb > tbody > :nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('2') //Pain Long Terms Goals (LTG)
            cy.get(':nth-child(2) > :nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting comment
            cy.wait(5000)

            cy.get(':nth-child(1) > :nth-child(2) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Trapeze{enter}') //inputting assistive device
            //Bed Mobility
            cy.get('.global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //roll left and right code
            cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //sit to lying code
            cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(4) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //lying to sitting on side of bed code
            cy.get(':nth-child(2) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(5) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //scooting code
            cy.get('#tp_bedmobility_chosen > ul').click() //clicking dropdown for treatment plan
            cy.get('#tp_bedmobility_chosen > div > ul > li').click() //clicking result for treatment plan
            cy.get(':nth-child(2) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //roll left and right stg yes
            cy.get(':nth-child(1) > :nth-child(3) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //sit to lying stg yes
            cy.get(':nth-child(1) > :nth-child(4) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //lying to sitting on side of bed stg yes
            cy.get(':nth-child(1) > :nth-child(5) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //scooting stg yes
            cy.get(':nth-child(9) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //roll left and right ltg yes
            cy.get(':nth-child(1) > :nth-child(3) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //sit to lying ltg yes
            cy.get(':nth-child(1) > :nth-child(4) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //lying to sitting on side of bed ltg yes
            cy.get(':nth-child(1) > :nth-child(5) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //scooting ltg yes
        
            cy.get(':nth-child(2) > :nth-child(2) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Walker{enter}') //inputting assistive device
            //Transfer
            cy.get(':nth-child(2) > :nth-child(2) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //Sit to stand code
            cy.get(':nth-child(2) > :nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Chair/bed-to-chair transfer code
            cy.get(':nth-child(2) > :nth-child(4) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Shower or tub code
            cy.get(':nth-child(2) > :nth-child(5) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Car transfer code
            cy.get('#tp_transfer_chosen > ul').click() //clicking dropdown for treatment plan
            cy.get('#tp_transfer_chosen > div > ul > li.active-result.highlighted').click() //clicking result for treatment plan
            cy.get(':nth-child(2) > :nth-child(2) > :nth-child(8) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Sit to stand stg yes
            cy.get(':nth-child(2) > :nth-child(3) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Chair/bed-to-chair transfer stg yes
            cy.get(':nth-child(2) > :nth-child(4) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Shower or tub stg yes
            cy.get(':nth-child(2) > :nth-child(5) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Car transfer stg yes
            cy.get(':nth-child(2) > :nth-child(2) > :nth-child(10) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Sit to stand ltg yes
            cy.get(':nth-child(2) > :nth-child(3) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Chair/bed-to-chair transfer ltg yes
            cy.get(':nth-child(2) > :nth-child(4) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Shower or tubd ltg yes
            cy.get(':nth-child(2) > :nth-child(5) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Car transfer ltg yes

            cy.get(':nth-child(3) > :nth-child(2) > :nth-child(2) > .input-drp-treatment > .fg-line > .table-input').type('Quad-cane{enter}') //inputting assistive device
            //Gait/Ambulation
            cy.get(':nth-child(3) > :nth-child(2) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //Walk 10 feet code
            cy.get(':nth-child(3) > :nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Walk 50 feet with two turns code
            cy.get(':nth-child(3) > :nth-child(4) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Walk 150 feet code
            cy.get(':nth-child(3) > :nth-child(5) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Walk 10 ft on uneven surface code
            cy.get(':nth-child(3) > :nth-child(6) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //1 step (curb) go up and down code
            cy.get(':nth-child(3) > :nth-child(7) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //4 steps (go up and down) code
            cy.get(':nth-child(3) > :nth-child(8) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //12 steps (go up and down) code
            cy.get(':nth-child(3) > :nth-child(9) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Picking up object code
            cy.get('#tp_gaitambulation_chosen > ul').click() //clicking dropdown for treatment plan
            cy.get('#tp_gaitambulation_chosen > div > ul > li').click() //clicking result for treatment plan
            cy.get(':nth-child(8) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 10 feet stg yes
            cy.get(':nth-child(3) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 50 feet with two turns stg yes
            cy.get(':nth-child(4) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 150 feet stg yes
            cy.get(':nth-child(5) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 10 ft on uneven surface stg yes
            cy.get(':nth-child(6) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //1 step (curb) go up and down stg yes
            cy.get(':nth-child(7) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //4 steps (go up and down) stg yes
            cy.get(':nth-child(8) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //12 steps (go up and down) stg yes
            cy.get(':nth-child(9) > :nth-child(5) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Picking up object stg yes
            cy.get(':nth-child(10) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 10 feet ltg yes
            cy.get(':nth-child(3) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 50 feet with two turns ltg yes
            cy.get(':nth-child(4) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 150 feet ltg yes
            cy.get(':nth-child(5) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Walk 10 ft on uneven surface ltg yes
            cy.get(':nth-child(6) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //1 step (curb) go up and down ltg yes
            cy.get(':nth-child(7) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //4 steps (go up and down) ltg yes
            cy.get(':nth-child(8) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //12 steps (go up and down) ltg yes
            cy.get(':nth-child(9) > :nth-child(7) > .m-tb-0 > .m-l-10 > .ng-pristine').click() //Picking up object ltg yes

            //Wheelchair Mobility
            cy.get(':nth-child(4) > tbody > :nth-child(2) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Manual – wheel 50 ft. with two turns code
            cy.get(':nth-child(4) > tbody > :nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Manual WC – wheel 150 feet code
            cy.get(':nth-child(4) > tbody > :nth-child(4) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Motorized – wheel 50 ft. with two turns code
            cy.get(':nth-child(4) > tbody > :nth-child(5) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Motorized WC – wheel 150 feet code
            cy.get(':nth-child(4) > tbody > :nth-child(6) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Propulsion/retropulsion on uneven surface code
            cy.get(':nth-child(4) > tbody > :nth-child(7) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Propulsion/retropulsion on ramp
            cy.get(':nth-child(4) > tbody > :nth-child(8) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Safety locks code
            cy.get(':nth-child(4) > tbody > :nth-child(9) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Leg/foot rests code
            cy.get('#tp_wheelchairmobility_chosen > ul').click() //clicking dropdown
            cy.get('#tp_wheelchairmobility_chosen > div > ul > li').click() //clicking treatment plan result

            //Balance
            cy.get(':nth-child(5) > tbody > :nth-child(2) > :nth-child(2) > .select > .select-c > .select_cus').select('Fair-Plus') //Sitting (Static) Assessment
            cy.get(':nth-child(5) > tbody > :nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('Fair-Plus') //Sitting (Dynamic) Assessment
            cy.get(':nth-child(5) > tbody > :nth-child(4) > :nth-child(2) > .select > .select-c > .select_cus').select('Fair-Plus') //Standing (Static) Assessment
            cy.get(':nth-child(5) > tbody > :nth-child(5) > :nth-child(2) > .select > .select-c > .select_cus').select('Fair-Plus') //Standing (Dynamic) Assessment
            cy.get(':nth-child(5) > tbody > :nth-child(2) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //Sitting (Static) Code
            cy.get(':nth-child(3) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //Sitting (Dynamic) Code
            cy.get(':nth-child(4) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //Standing (Static) Code
            cy.get(':nth-child(5) > :nth-child(3) > .select > .select-c > .select_cus').select('05') //Standing (Dynamic) Code
            cy.get('#tp_balance_chosen > ul').click() //clicking dropdown
            cy.get('#tp_balance_chosen > div > ul > li.active-result.highlighted').click() //clicking treatment plan result

            //Deviation
            cy.get('.global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //Antalgic Functional Code
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(3) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //WBOS Functional Code
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(4) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //NBOS Functional Code
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(5) > :nth-child(2) > .select > .select-c > .select_cus').select('05') //List Functional Code
            cy.get(':nth-child(2) > :nth-child(4) > .select > .select-c > .select_cus').select('05') //Shuffle Functional Code
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(3) > :nth-child(4) > .select > .select-c > .select_cus').select('05') //Ataxic Functional Code
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(4) > :nth-child(4) > .select > .select-c > .select_cus').select('05') //Limited TKE/SLS Functional Code
            cy.get('.text-center > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(5) > :nth-child(4) > .select > .select-c > .select_cus').select('05') //Other Functional Code
            cy.get('.b-b-n > .table-input').type('Test') //inputting comment

            //Weight Bearing Status
            cy.get(':nth-child(2) > .p-0 > .global__inner-wb > tbody > tr > :nth-child(2) > .radio > .ng-valid').click() //clicking FWB Left lower extremities
            cy.get('[colspan="9"] > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting note Left lower extremities
            cy.get('.global__inner-wb > tbody > tr > :nth-child(2) > .radio > .ng-pristine').click() //clicking FWB Right lower extremities
            cy.get(':nth-child(3) > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting note Right lower extremities
        
            //DME (Click all (Have))
            cy.get(':nth-child(2) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(2) > :nth-child(5) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(5) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(5) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(5) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(5) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(5) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(2) > :nth-child(8) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(8) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(8) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(8) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(7) > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(6) > :nth-child(8) > .checkbox > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(7) > .cont-opt > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(7) > :nth-child(8) > .checkbox > .ng-pristine').click()
            //DME working properly?
            cy.get('.inner__table-nb > tbody > tr > :nth-child(2) > .radio > .ng-valid').click() //clicking yes
            cy.get('[style="width: 40%"] > .global__txtbox').type('Test')
            cy.get(':nth-child(5) > .checkbox > label > .ng-valid').click() //clicking Malfunction reported to DME vendor

            //Standardized / Validated and reliable test and measurements assessment (Per agency’s protocol)
            cy.get(':nth-child(2) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Timed Up & Go
            cy.get(':nth-child(2) > .p-0 > .global__inner-wb > tbody > :nth-child(2) > :nth-child(5) > .table-input').type('Test')  //inputting notes Timed Up & Go
            cy.get(':nth-child(3) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Tinetti (POMA)
            cy.get(':nth-child(3) > :nth-child(5) > .table-input').type('Test')  //inputting notes Tinetti (POMA)
            cy.get(':nth-child(4) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Berg Balance Test
            cy.get(':nth-child(4) > :nth-child(5) > .table-input').type('Test')  //inputting notes Berg Balance Test
            cy.get(':nth-child(5) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Functional Reach
            cy.get(':nth-child(5) > :nth-child(5) > .table-input').type('Test')  //inputting notes Functional Reach
            cy.get(':nth-child(6) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Fall Efficacy Scale
            cy.get(':nth-child(6) > :nth-child(5) > .table-input').type('Test')  //inputting notes Fall Efficacy Scale
            cy.get(':nth-child(7) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Dynamic Gait Index
            cy.get(':nth-child(7) > :nth-child(5) > .table-input').type('Test')  //inputting notes Dynamic Gait Index
            cy.get(':nth-child(8) > :nth-child(4) > .global__txtbox').type('Test') //inputting result Timed Walking Test
            cy.get(':nth-child(8) > :nth-child(5) > .table-input').type('Test')  //inputting notes Timed Walking Test
            cy.get('.cont-opt > .ng-isolate-scope > .ng-valid').click() //clicking high for risk for fall
            cy.get('td > :nth-child(2) > .table-input').type('Test') //inputting comment

            //Safety
            cy.get('.table.ng-scope > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > :nth-child(2) > :nth-child(2) > .table-input').type('Test') //inputting interventions for Test
            cy.get(':nth-child(3) > :nth-child(2) > .table-input').type('Test') //inputting interventions for Test
            cy.get('.table.ng-scope > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > :nth-child(2) > :nth-child(3) > .lopt-left > :nth-child(1) > .ng-pristine').click() //clicking yes
            cy.get(':nth-child(3) > :nth-child(3) > .lopt-left > :nth-child(1) > .ng-pristine').click() //clicking yes
            cy.get('.table.ng-scope > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > :nth-child(2) > :nth-child(3) > .cont-opt > .global__txtbox').type('Test')
            cy.get(':nth-child(3) > :nth-child(3) > .cont-opt > .global__txtbox').type('Test')
            //Knowledge Deficit
            cy.get(':nth-child(11) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(1) > .table-input').type('Test') //inputting Knowledge Deficit
            cy.get(':nth-child(11) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(2) > .table-input').type('Test') //inputting Interventions
            cy.get(':nth-child(11) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(3) > .lopt-left > :nth-child(1) > .ng-pristine').click() //clicking yes
            cy.get(':nth-child(11) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(3) > .cont-opt > .global__txtbox').type('Test')
            //Other Problems
            cy.get(':nth-child(12) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(1) > .table-input').type('Test') //inputting Other Problems
            cy.get(':nth-child(12) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(2) > .table-input').type('Test') //inputting Interventions
            cy.get('.lopt-left > :nth-child(1) > .ng-pristine').click() //clicking yes
            cy.get(':nth-child(12) > :nth-child(1) > :nth-child(1) > .p-0 > .global__inner-wb > tbody > .ng-scope > :nth-child(3) > .cont-opt > .global__txtbox').type('Test')

            cy.get(':nth-child(1) > .p-0 > .cont-opt > div > .global__txtbox').type('Test') //inputting Frequency and duration of therapy visits
            //Discharge plan (Click all)
            cy.get(':nth-child(2) > .p-0 > :nth-child(3) > .ng-scope').click()
            cy.get('.p-0 > :nth-child(5) > .ng-scope').click()
            cy.get('.p-0 > :nth-child(7) > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .cont-opt > div > .global__txtbox').type('Test') //inputting other
            cy.get('.cont-opt > div > :nth-child(2) > .ng-valid').click() //clicking fair for Rehabilitation potential
            cy.get('[chk="data.rp0002"] > .ng-valid').click() //clicking patient
            cy.get('.p-0 > .m-l-5 > .ng-valid').click() //clicking caregiver
            cy.get('.p-0 > :nth-child(4) > .ng-valid').click() //clicking no
            cy.get(':nth-child(5) > .p-0 > .cont-opt > div > .global__txtbox').type('Test') //inputting explaination if no
            cy.get('.checkbox > .ng-isolate-scope > .ng-valid').click() //clicking Physician notified and agrees with POC, frequency and duration.
            cy.get('.p-0 > .table-input').type('Test') //inputting Other assessments, observations and interventions

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.get('.btn-danger').click() //clicking no to save information





        })

    })