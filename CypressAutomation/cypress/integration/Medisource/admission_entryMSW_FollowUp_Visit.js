Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of MSW - Follow-Up Visit', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Peralta M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(17) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click MSW - Follow-Up Visit
                    cy.wait(10000)

                })
           
        it('Automating Entry Form of MSW - Follow-Up Visit', function() {
            cy.wait(10000)
            cy.get('#timeIn').type('1200') //inputting time in
            cy.get('#timeOut').type('1900') //inputting time out
            cy.wait(2000)

            cy.get('#diagnosis').type('Test') //inputting diagnoses
            cy.get('#diagnosisOther').type('Test') //inputting other diagnoses/history

            //HCPCS (click all)
            cy.get('[rname="q5001"]').click() 
            cy.get('[rname="q5002"]').click() 
            cy.get('[rname="q5009"]').click() 

            //Living Situation
            cy.get(':nth-child(1) > .radio > .ng-pristine').click() //clicking Unchange from the last visit
            cy.get('[name="upPriCar"]').click() //clicking Update to primary caregiver
            cy.get('[name="chEnvCon"]').click() //clicking Change of environment conditional

            cy.get('#parent > div > form > div > fieldset > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > div > textarea').type('Test') //inputting Psychosocial Update/Note

            //Mental Status (Check all that apply)
            cy.get('[rname="mentalStatus"]').click({multiple:true})

            //Emotional Status (Check all that apply)
            cy.get('[rname="emoStat"]').click({multiple:true})
            cy.get('[style="width: 8%"] > .checkbox > .ng-scope').click() //clicking other
            cy.get(':nth-child(5) > :nth-child(3) > .fg-line > .global__txtbox').type('Test') //inputting other

            //Visit Goals (click all)
            cy.get('[name="visitGoals"]').click({multiple:true})
            cy.get(':nth-child(4) > [style="width: 20%"] > .checkbox > .ng-scope') //clicking other
            cy.get('#visitGoalsTxt').type('Test') //inputting other
            cy.get(':nth-child(5) > .p-t-5 > .fg-line > .form-control').type('Test') //inputting Visit goal details

            //Visit Interventions (click all)
            cy.get('[rname="visitInterventions"]').click({multiple:true})
            cy.get(':nth-child(6) > [style="width: 20%"] > .checkbox > .ng-scope') //clicking other
            cy.get(':nth-child(7) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(7) > .p-t-5 > .fg-line > .form-control').type('Test') //inputting Intervention details

            //Progress Towards Goals (click all)
            cy.get('[rname="proTowGoa"]').click({multiple:true})
            cy.get(':nth-child(8) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(5) > td > .checkbox > .ng-scope').click() //clicking good
            cy.get(':nth-child(8) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(7) > .p-t-5 > .fg-line > .form-control').type('Test')

            //Follow-Up Plan (Mark all that apply)
            cy.get('[rname="folUpPlan"]').click({multiple:true})

            //SAVE BUTTON
            cy.get('.btn__success').click() //clicking save button
            cy.wait(10000)
    
        })
    })