Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />

describe('Automating Entry Form of OASIS - Discharge from Agency', () => {
         
  beforeEach(() => {
        cy.login1('vincent@geeksnest', 'Tester2021!'); //cy.login - command located in command.js
        
        cy.viewport(1920, 924);//setting your windows size
        cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
        cy.wait(5000) 
           const dayjs = require('dayjs')
              cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(3) > a').click() //clicking Discharged Tab
              cy.get('#searchbar__wrapper > div > input').type('Will')  //search the added patient 
              cy.wait(5000) 
              //click the added patient                    
              cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
              cy.wait(5000)

              cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr.sampletd.pointer.globallist-item.tbl__row-default.ng-scope.lupa_second_half > td:nth-child(2) > a.ng-binding.ng-scope').click() //click OASIS - Discharge from Agency
              cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button').click() //click Edit Button
})
      
      it('Automating Entry Form of Demographics / Clinical Record', function() {     
    
           
        // Demographics / Clinical Record TAB Start -----------
            cy.get('#ti').type('1200') //inputting time in
            cy.get('#to').type('1900') //inputting time out
            cy.wait(2000)
            cy.get('#M0090_INFO_COMPLETED_DT').type('0802022')  //inputting date assessment completed
            //(M0150) - Current Payment Sources for Home Care
            cy.get('#M0150_CPAY_NONE > input').click()
            //(M1041) - Influenza Vaccine Data Collection Period
            cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(26) > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click() //clicking 1
            //(M1046) - Influenza Vaccine Received
            cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(28) > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click() //clicking 1
            //(M1051) - Pneumococcal Vaccine:
            cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(30) > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click() //clicking NO
            //(M1056) - Reason Pneumococcal Vaccine not received
            cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(32) > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click() //clicking 2
            cy.wait(5000)
            //Save Button
            cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
            cy.get(10000)
        // Demographics / Clinical Record TAB End -----------
    
    });
         
     it('Automating Entry Form of Vital Signs/Sensory Tab', function()
    {   

         // Vital Signs/Sensory TAB Start -----------
         cy.get('#oasis-tabs > label:nth-child(2)').click() //clicking Vital Signs/Sensory tab
         cy.wait(5000)
         //Vitals Sign Information Section
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > input').type('98') // inputting temperature
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking oral for temperature
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > input').type('3') //inputting Pulse/HR
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking radial for Pulse/HR
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > input').type('3') //inputting respiration
         cy.get('[name="SOOVS0009"]').type('180') //inputting Systolic - BP Left Arm
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120')  //inputting Diastolic - BP Left Arm
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
         cy.get('[name="SOOVS0012"]').type('180') //inputting Systolic - BP Right Arm
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - BP Right Arm
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr > td.ng-isolate-scope > div > label:nth-child(1) > input').click() //clicking sitting
         cy.get('[name="SOOVS0015"]').type('180') //inputting Systolic - BP Left Leg
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - BP Left Leg
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
         cy.get('[name="SOOVS0018"]').type('180') //inputting Systolic - BP Right Leg
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - Right Leg
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr > td:nth-child(2) > input').type('2') //inputting O2 Saturation % on room air
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('2') //inputting O2 Saturation % on O2
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(3) > input').type('2') //inputting lPM on o2 Saturation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting blood sugar
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking FBS
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(12) > td > table > tbody > tr > td.p-l-5 > input').type('100') //inputting weight
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(12) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking actual
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(13) > td > table > tbody > tr > td:nth-child(2) > input').type('60') //inputting height
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(13) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking actual
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-5.b-r-n.b-l-n > div.m-t-2.p-0.display-ib.ng-isolate-scope > label:nth-child(2) > input').click() //clicking Yes for evidence for infection
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-5.b-r-n.b-l-n > div:nth-child(2) > input').type('test') //inputting description
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > label:nth-child(1) > input').click() //clicking MD in notified
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > label:nth-child(2) > input').click() //clicking CM/Supervisor
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div.m-t-2.p-0.display-ib.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes for new/change/hold medications
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(2) > input').type('Test') //inputting description
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         // (M1242) Frequency of Pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1 for frequency pain
                    
         // Pain Location
         // Location 1
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1) > input').type('Test') //inputting pain location
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > a').click() //clicking dropdown for type
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > a').click() //clicking dropdown for present level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(5)').click() //clicking personal level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > a').click() //clicking dropdown worst level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(5)').click() //clicking worst level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > a').click() //clicking acceptable level dropdown
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(5)').click() //clicking acceptable level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > a').click() //clicking level after meds dropdown
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(4)').click() //clicking level after meds
         cy.wait(5000)
         // Location 2
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1) > input').type('Test') //inputting pain location
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > a').click() //clicking dropdown for type
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > a').click() //clicking dropdown for present level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(2)').click() //clicking personal level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > a').click() //clicking dropdown worst level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(6)').click() //clicking worst level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > a').click() //clicking acceptable level dropdown
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(6)').click() //clicking acceptable level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > a').click() //clicking level after meds dropdown
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(7)').click() //clicking level after meds
         cy.wait(5000)
         //Character Pain(Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(4) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label > input').click()
         cy.wait(5000)
         //Non-verbal signs of pain (Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > label > input').click()
         cy.wait(5000)
         //What makes the pain better?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > input').type('Test')
         cy.wait(5000)
         //What makes the pain worse?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div.m-0.m-b-5.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(4) > div > input').type('Test')
         //Pain medication
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div.display-ib.m-l-15 > label > input').click() // clicking yes for pain medication
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > div > input').type('Test') // inputting medication profile
         //How often pain meds needed?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking how often pain meds needed
         // Pain medication effectiveness
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label > input').click() //clicking Pain medication effectiveness
         //Physician aware of pain?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(8) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label > input').click() //clicking yes for Physician aware of pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > input').type('Test') //inputting obsevation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td.p-5.b-l-n > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Sensory Status
         //Eyes
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > label > input').click() // clicking corrective glasses
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking left for cataract
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click() //clicking right for cataract
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking left for glaucoma
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click() //clicking right for glaucoma
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking left for redness
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > label > input').click() //clicking right for redness
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking left for pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(3) > label > input').click() //clicking right for pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking left for itching
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(3) > label > input').click() //clicking right for itching
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click() //clicking left for ptsosis
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > label > input').click() //clicking Right for ptosis
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //clicking left for Sclera reddened
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(6) > label > input').click() //clicking Right for Sclera reddened
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //clicking left for Edema of eyelids
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(6) > label > input').click() //clicking Right for Edema of eyelids
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //clicking left for Blurred vision
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(6) > label > input').click() //clicking Right for Blurred vision
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > label > input').click() //clicking left for Blind	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(6) > label > input').click() //clicking Right for Blind	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(8) > label > input').click() //clicking left for Exudate from eyes	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(9) > label > input').click() //clicking Right for Exudate from eyes	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(8) > label > input').click() //clicking left for Excessive tearing
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(9) > label > input').click() //clicking Right for Excessive tearing
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(8) > label > input').click() //clicking left for Macular degeneration
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(9) > label > input').click() //clicking Right for Macular degeneration
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(8) > label > input').click() //clicking left for Retinopathy
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(9) > label > input').click() //clicking Right for Retinopathy
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(7) > input').type('Test') //inputting other status
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(8) > label > input').click() //clicking left for other status
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(9) > label > input').click() //clicking Right for other status
         cy.wait(5000)
         //Ears
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking left for HOH
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click() //clicking Right for HOH
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking left for Hearing aid
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click() //clicking Right for Hearing aid
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click() //clicking left for Deaf
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click() //clicking Right for Deaf
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > label > input').click() //clicking left for Tinnitus
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > div > label > input').click() //clicking Right for Tinnitus
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > label > input').click() //clicking left for Drainage
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(6) > div > label > input').click() //clicking Right for Drainage
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > label > input').click() //clicking left for Pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(6) > div > label > input').click() //clicking Right for Pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(7) > input').type('Test')
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(7) > div > div > label > input').click()
         cy.wait(5000)
         //Mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > label > input').click() //clicking dentures
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label:nth-child(1) > input').click() //clicking upper
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label:nth-child(2) > input').click() //clicking lower
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label.checkbox.checkbox-inline.m-r-5.ng-isolate-scope > input').click() //clicking partial
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking poor dentition for mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking gingivitis for mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click() //clicking toothache for mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking loss of taste for mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking lesions mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > input').type('Test') //inputting lesions
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking mass/tumor for mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div.cont-opt.m-lopt-105.ng-isolate-scope > input').type('Test') //inputting mass/tumor
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td.p-b-5.p-l-5 > div > label > input').click() // clicking Difficulty of chewing/swallowing for mouth
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td.p-b-5.p-l-5 > div > div.cont-opt.ng-isolate-scope > input').type('Test') //inputting others for mouth
         cy.wait(5000)
         //Nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking congestion for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking rhinitis for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click() //clicking sinus problem for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking loss of smell for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking epistaxis for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking noisy breathing for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.lopt-left > div > label > input').click() //clicking lesions for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > input').type('Test') //inputting lesions for nose
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > div > input').type('Test') //inputting others for nose
         cy.wait(5000)
         //Throat
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking sore throat for throat
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking dysphagia for throat
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking hoarseness for throat
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking lesions for throat
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > fieldset > input').type('Test') //inputting lesions for throat
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td.p-b-5.p-l-5 > div > div.cont-opt > div > input').type('Test') //inputting other for throat
         cy.wait(5000)
         //Speech
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking slow speech for speech
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking slured speech for speech
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking aphasia for speech
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking mechanical assistance for speech
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.cont-opt > input').type('Test')  //inputting other for speech
         //Touch
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > div > div > label > input').click() //clicking paresthesia for touch
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > div > div > label > input').click() //clicking hyperesthesia for touch
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-l-5.p-b-5 > div > div > div > label > input').click()  //clicking peripheral neuropathy for touch
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td.p-l-5 > div > div.cont-opt > input').type('Test') //inputting other for touch
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
         //SAVE BUTTON
         cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
         cy.wait(5000)

         // Vital Signs/Sensory TAB End -----------
    })

    it('Automating Entry Form of Integumentary/Endocrine Tab', function()
    {
      // Integumentary/Endocrine TAB Start -----------
         cy.get('#oasis-tabs > label:nth-child(3)').click() //clicking Integumentary/Endocrine TAB
         cy.wait(5000)
         //Integumentary Status
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-5.ng-isolate-scope > div > label > input').click() //clicking skin color
         cy.get('#SOOINT0060').type('Test') //inputting other in skin color
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking skin temp
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > div > label > input').click() //clicking moisture
         cy.get('#SOOINT0074').type('Test') //inputting other in moisture
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking turgor
         //Skin Integrity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > div > label > input').click() //clicking skin intact
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > div > label > input').click() //clicking lesion
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > div > a > i').click() //clicking add lesion
         //Other Form for Lesion
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > div > div').click() //clicking dropdown (select one lesion)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > div > div > div > ul > li:nth-child(2)').click() //selecting result dropdown (select one lesion)
         cy.wait(2000)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(5) > input').type('Test') //inputting location
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(7) > input').type('Test') //inputting comment

         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(4) > div > label > input').click() //clicking wound
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(4) > div > a > i').click() //clicking add wound
         //Other Form for Wound
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(3) > div > div.fg-line > input').type('Pressure Ulcer') //inputting result on wound #1
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(5) > div > div.fg-line > input').type('Buttock (R)') //inputting result (Location)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(7) > input').type('Test') //inputting comment on wound #1
         cy.get('[name="SOOINT0079"]').type('Test') //inputting other in skin integrity
         cy.get('#SOOINT0078').type('Test') //inputting observation
         cy.get('#SOOINT0080').type('Test') //inputting intervention
         cy.wait(5000)
         //Braden Scale for Predicting Pressure Sore Risk
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking sensory perception
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking moisture
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking activity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking mobility
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking nutrition
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td.p-0 > table > tbody > tr:nth-child(3) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking friction and shear
         //(M1306)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //(M1311) - Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
         cy.get('#M1311_NBR_PRSULC_STG2_A1').type('2') //inputting A1
         cy.get('#M1311_NBR_ULC_SOCROC_STG2_A2').type('2') //inputting A1
         cy.get('#M1311_NBR_PRSULC_STG3_B1').type('2') //inputting B1
         cy.get('#M1311_NBR_ULC_SOCROC_STG3_B2').type('2') //inputting B2
         cy.get('#M1311_NBR_PRSULC_STG4_C1').type('2') //inputting C1
         cy.get('#M1311_NBR_ULC_SOCROC_STG4_C2').type('2') //inputting C2
         cy.get('#M1311_NSTG_DRSG_D1').type('2') //inputting D1
         cy.get('#M1311_NSTG_DRSG_SOCROC_D2').type('2') //inputting D2
         cy.get('#M1311_NSTG_CVRG_E1').type('2') //inputting E1
         cy.get('#M1311_NSTG_CVRG_SOCROC_E2').type('2') //inputting E2
         cy.get('#M1311_NSTG_DEEP_TSUE_F1').type('2') //inputting F1
         cy.get('#M1311_NSTG_DEEP_TSUE_SOCROC_F2').type('2') //inputting F2
         //(M1322) - Current Number of Stage 1 Pressure Injuries
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click()
         //(M1324) - Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //(M1334) - Status of Most Problematic Stasis Ulcer that is Observable
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(6) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //(M1340) - Does this patient have a Surgical Wound?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(7) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //M1342) - Status of Most Problematic Surgical Wound that is Observable
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(8) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
         cy.wait(5000)
         //Endocrine System (Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#SOOENDO0092').type('Test') //inputting other in endocrine system
         cy.wait(5000)
         //Diabetes
         cy.get('#SOOENDO0003').type('Test') //inputting Hgb A1C
         cy.get('#SOOENDO0004').type('04/23/2022') //inputting date tested
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking new for onset
         cy.get('#SOOENDO0008').type('04/02/2022') //inputting onset date
         //Diabetes management (Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-b-5 > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-l-15.p-b-5 > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-l-15.-b-5 > div > label > input').click()
             
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label:nth-child(2) > input').click() //clicking yes for Signs of hypoglycemia?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label:nth-child(2) > input').click() //clicking yes for Signs of hyperglycemia?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting for observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting for intervention
         // Foot Assessment
         cy.wait(5000)
         cy.get('[name="SOOENDO0030"]').click() //clicking left for Thick or ingrown toenail
         cy.get('[name="SOOENDO0031"]').click() //clicking right for Thick or ingrown toenail
         cy.get('[name="SOOENDO0032"]').click() //clicking left for Calluses or fissures
         cy.get('[name="SOOENDO0033"]').click() //clicking right for Calluses or fissures
         cy.get('[name="SOOENDO0034"]').click() //clicking left for Interdigital macerations
         cy.get('[name="SOOENDO0035"]').click() //clicking right for Interdigital macerations
         cy.get('[name="SOOENDO0036"]').click() //clicking left for Signs of fungal infection
         cy.get('[name="SOOENDO0037"]').click() //clicking right for Signs of fungal infection
         cy.get('[name="SOOENDO0040"]').click() //clicking left for Absent pedal pulses
         cy.get('[name="SOOENDO0041"]').click() //clicking right for Absent pedal pulses
         cy.get('[name="SOOENDO0044"]').click() //clicking left for Hot, red, swollen foot
         cy.get('[name="SOOENDO0045"]').click() //clicking right for Hot, red, swollen foot
         cy.get('[name="SOOENDO0028"]').click() //clicking left for Foot deformity (hammer/claw toes)
         cy.get('[name="SOOENDO0029"]').click() //clicking right for Foot deformity (hammer/claw toes)
         cy.get('[name="SOOENDO0038"]').click() //clicking left for Limited range of motion of joints
         cy.get('[name="SOOENDO0039"]').click() //clicking right for Limited range of motion of joints
         cy.get('[name="SOOENDO0042"]').click() //clicking left for Decreased circulation (cold foot)
         cy.get('[name="SOOENDO0043"]').click() //clicking right for Decreased circulation (cold foot)	
         cy.get('[name="SOOENDO0046"]').click() //clicking left for Burning/tingling sensation, numbness	
         cy.get('[name="SOOENDO0047"]').click() //clicking right for Burning/tingling sensation, numbness
         cy.get('[name="SOOENDO0048"]').click() //clicking left for Loss of sensation to heat or cold
         cy.get('[name="SOOENDO0049"]').click() //clicking right for Loss of sensation to heat or cold
         cy.get('[name="SOOENDO0103"]').type('Test') //inputting other in foot assessment
         cy.get('[name="SOOENDO0103L"]').click() //clicking left for other foot assessment
         cy.get('[name="SOOENDO0103R"]').click() //clicking right for other foot assessment
         cy.wait(5000)    
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n.b-r-n > div > label:nth-child(1) > input').click() //clicking foot exam frequency
         cy.get('#SOOENDO0019').type('Test') //inputting other for foot exam frequency
         //Regularly done by (Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label.checkbox.checkbox-inline.m-l-15.ng-isolate-scope > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
             
         //Patient/Caregiver Competence
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent with glucometer use including control testing?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent with glucometer use including control testing?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('Test')
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent with insulin preparation and administration?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent with insulin preparation and administration?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(4) > input').type('Test')
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent after instructions given and performed return demo?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent after instructions given and performed return demo?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(4) > input').type('Test')
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking yes for patient on Level of knowledge of disease process and management
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n > label:nth-child(4) > input').click() //clicking yes for caregiver on Level of knowledge of disease process and management
         cy.wait(5000)
         //Blood glucose testing
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking testing frequency
         cy.get('[name="SOOENDO0016"]').type('Test') //inputting other in testing frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div.cont-opt > div > div.fg-line > input').click() //clicking input box
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div.cont-opt > div > div.opt-list.ng-isolate-scope > ul > li:nth-child(1)').click() //clicking CGM Brand
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > div > label > input').click() //clicking control testing done
         //Other Form of Glucometer/CGM (Control Test Result)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > input').type('1') //inputting level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting mg/dl
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking yes for within range?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > input').type('1') //inputting level
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting mg/dl
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking yes for within range?
         cy.get('#SOOENDO0102_chosen > ul').click() //clicking inputbox/dropdown
         cy.get('#SOOENDO0102_chosen > div > ul > li:nth-child(1)').click() //selecting result
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2) > textarea').type('Test') //inputting action taken
         cy.wait(5000)    
         //Save Button
         cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
         cy.wait(5000)
         cy.get('body > div.swal2-container > div.swal2-modal.show-swal2 > button.swal2-cancel.styled').click() //clicking access later in modal
         cy.wait(5000)
      // Integumentary/Endocrine TAB End -----------

    });

    it('Automating Entry Form of Cardiopulmonary Tab', function()
    {
      // Cardiopulmonary TAB Start -----------
         
         cy.get('#oasis-tabs > label:nth-child(4)').click() //clicking Cardiopulmonary TAB
         cy.wait(5000)
         //(M1400)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //Respiratory Status
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > div.cont-opt.m-lopt-185.p-r-5 > span > label > input').click() //clicking No Breath sounds clear, bilateral
         cy.get('[name="SOOCARDIO0171"]').click() //clicking left for Diminished
         cy.get('[name="SOOCARDIO0172"]').click() //clicking Right for Diminished
         cy.get('[name="SOOCARDIO0204"]').click() //clicking Anterior for Diminished
         cy.get('[name="SOOCARDIO0205"]').click() //clicking Posterior for Diminished
         cy.get('[name="SOOCARDIO0206"]').click() //clicking Upper for Diminished
         cy.get('[name="SOOCARDIO0207"]').click() //clicking Middle for Diminished
         cy.get('[name="SOOCARDIO0208"]').click() //clicking Lower for Diminished
         cy.get('[name="SOOCARDIO0175"]').click() //clicking left for Absent
         cy.get('[name="SOOCARDIO0176"]').click() //clicking Right for Absent
         cy.get('[name="SOOCARDIO0209"]').click() //clicking Anterior for Absent
         cy.get('[name="SOOCARDIO0210"]').click() //clicking Posterior for Absent
         cy.get('[name="SOOCARDIO0211"]').click() //clicking Upper for Absent
         cy.get('[name="SOOCARDIO0212"]').click() //clicking Middle for Absent
         cy.get('[name="SOOCARDIO0213"]').click() //clicking Lower for Absent
         cy.get('[name="SOOCARDIO0179"]').click() //clicking left for Rales (crackles)
         cy.get('[name="SOOCARDIO0180"]').click() //clicking Right for Rales (crackles)
         cy.get('[name="SOOCARDIO0214"]').click() //clicking Anterior for Rales (crackles)
         cy.get('[name="SOOCARDIO0215"]').click() //clicking Posterior for Rales (crackles)
         cy.get('[name="SOOCARDIO0216"]').click() //clicking Upper for Rales (crackles)
         cy.get('[name="SOOCARDIO0217"]').click() //clicking Middle for Rales (crackles)
         cy.get('[name="SOOCARDIO0218"]').click() //clicking Lower for Rales (crackles)
         cy.get('[name="SOOCARDIO0268"]').click() //clicking left for Rhonchi
         cy.get('[name="SOOCARDIO0269"]').click() //clicking Right for Rhonchi
         cy.get('[name="SOOCARDIO0270"]').click() //clicking Anterior for Rhonchi
         cy.get('[name="SOOCARDIO0271"]').click() //clicking Posterior for Rhonchi
         cy.get('[name="SOOCARDIO0272"]').click() //clicking Upper for Rhonchi
         cy.get('[name="SOOCARDIO0273"]').click() //clicking Middle for Rhonchi
         cy.get('[name="SOOCARDIO0274"]').click() //clicking Lower for Rhonchi
         cy.get('[name="SOOCARDIO0183"]').click() //clicking left for Wheeze
         cy.get('[name="SOOCARDIO0184"]').click() //clicking Right for Wheeze
         cy.get('[name="SOOCARDIO0219"]').click() //clicking Anterior for Wheeze
         cy.get('[name="SOOCARDIO0220"]').click() //clicking Posterior for Wheeze
         cy.get('[name="SOOCARDIO0221"]').click() //clicking Upper for Wheeze
         cy.get('[name="SOOCARDIO0222"]').click() //clicking Middle for Wheeze
         cy.get('[name="SOOCARDIO0223"]').click() //clicking Lower for Wheeze
         cy.get('[name="SOOCARDIO0187"]').click() //clicking left for Stridor
         cy.get('[name="SOOCARDIO0188"]').click() //clicking Right for Stridor
         cy.get('[name="SOOCARDIO0224"]').click() //clicking Anterior for Stridor
         cy.get('[name="SOOCARDIO0225"]').click() //clicking Posterior for Stridor
         cy.get('[name="SOOCARDIO0226"]').click() //clicking Upper for Stridor
         cy.get('[name="SOOCARDIO0227"]').click() //clicking Middle for Stridor
         cy.get('[name="SOOCARDIO0228"]').click() //clicking Lower for Stridor
         cy.wait(5000)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td > div.cont-opt.m-lopt-185.p-r-5 > span > label > input').click() //clicking yes for Abnormal breathing patterns
         //Abnormal breathing patterns (Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div:nth-child(2) > div > input').type('Test')
         cy.wait(5000)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td > div.cont-opt.p-r-5 > span > span > label > input').click() //clicking yes for cough
         //Cough
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking character of cough
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking sputum color of cough
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking sputum character of cough
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click() //clicking sputum amount of cough
         //Special Procedure (Select All)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > input').type('Test') //inputting other for special procedure
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         // Other form of Oxygen therapy
         //Oxygen Risk Assessment
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td:nth-child(2) > label > input').click() //click yes for Is the patient using oxygen equipment?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //click yes for Does anyone in the home smoke?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //click yes for Are oxygen signs posted in the appropriate places?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //click yes for Are oxygen tanks/concentrators stored safely?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(6) > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input').click() //click yes for Is backup O2 tank available?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input').click() //click yes for Does patient/PCG know how to use backup O2?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //click yes for Are all cords near or related to oxygen intact, secure & properly used?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //click yes for Is patient educated re: substances that can cause O2 to be flammable?
         //Are there potential sources of open flames identified? (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(4) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(5) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(6) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //click yes for Are there potential sources of open flames identified?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
         //Oxygen Theraphy
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking type
         //Oxygen delivery (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(3) > td.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting other oxygen delivery
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(4) > td.p-b-5.b-l-n > table > tbody > tr > td > div > input').type('10') //inputting liters/minute
         //Oxygen source (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(3) > div > input').type('Test') //inputting other for oxygen source
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(1) > label:nth-child(1) > input').click() //clicking yes for backup O2 tank
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking vendor notified
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking for observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking for intervention
         cy.wait(5000)
         //Other form for Tracheostomy
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-t-n.b-b-n.ng-isolate-scope > span > input').type('Test') //inputting brand/model
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.ng-isolate-scope > div > input').type('4')
         cy.get('#SOOCARDIO0089').type('12042021') //inputting date last change (mm/dd/yyyy)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.ng-isolate-scope > span > span > input').type('Test') //inputting inner cannula
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention
         //Other form for BiPAP/CPAP
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.ng-isolate-scope > div > input').type('Test') //inputting device brand/model
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Device working properly?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Compliant with use of device?\
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting intervention
         //Other form for Suctioning
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > label:nth-child(1) > input').click() //clicking oral
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td > label:nth-child(1) > input').click() //clicking yes for Is the person performing the suctioning proficient? 
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td > label:nth-child(2) > input').click() //clicking yes for Is the suction equipment setup always ready for use?
         //Suction procedure done by (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(4) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
         //Other form for Ventilator
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td.b-b-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting Brand/Model
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td.b-b-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting Tidal volume
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(2) > div > input').type('Test') //inputting FiO2
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(2) > div > input').type('Test') //inputting Assist control
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td.b-t-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting PEEP
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting SIMV	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(4) > div > input').type('Test') //inputting Pressure control
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(4) > div > input').type('Test') //inputting PRVC
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention
         //Other form for PleurX
         cy.get('#SOOCARDIO0292').type('12042021') //inputting date catheter inserted (mm/dd/yyyy)
         cy.get('#SOOCARDIO0293').click() //clicking daily for drainage frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting other for drainage frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > input').type('2') //inputting ml for amount drained
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > div > label > input').click() //clicking done
         //Procedure done by (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(7) > td.ng-isolate-scope > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Cardiovascular
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(1) > label > input').click() //clicking regular for Heart Rhythm
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr > td.p-r-5 > label > input').click() //clicking < 3 sec for Capillary refill
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for JVD	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for Peripheral edema
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for Chest Pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking yes for Cardiac device
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td.p-0 > div > div > div').click() //clicking dropdown for cardiac device result
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td.p-0 > div > div > div > div > ul > li:nth-child(1)').click() //clicking result on cardiac device
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > label.radio.radio-inline.m-l-20.ng-isolate-scope > input').click() //clicking yes for weight gain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('Test') //inputting other for weight gain
         cy.wait(5000)
         //Pulses
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(2) > label > input').click() //clicking pedal-left for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(3) > label > input').click() //clicking pedal-right for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(4) > label > input').click() //clicking Popliteal-left for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(5) > label > input').click() //clicking Popliteal-right for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(6) > label > input').click() //clicking Femoral-left for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(7) > label > input').click() //clicking Femoral-right for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(8) > label > input').click() //clicking Brachial-left for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(9) > label > input').click() //clicking Brachial-right for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(10) > label > input').click() //clicking Radial-left for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(11) > label > input').click() //clicking Radial-right for Bounding
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(15) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Peripheral edema
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking +1 for Pedal edema - Left
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking +1 for Pedal edema - Right	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking +1 for Ankle edema - Left	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking +1 for Ankle edema - Right	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking +1 for Leg edema - Left
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking +1 for Leg edema - Right
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(9) > td:nth-child(2) > label > input').click() //clicking +1 for Sacral edema	
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(10) > td:nth-child(2) > label > input').click() //clicking +1 for Generalized edema
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(11) > td.ng-isolate-scope > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(12) > td.ng-isolate-scope > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Chest Pain
         //Character (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-r-5.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to shoulder
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to jaw
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to neck
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to arm
         //Accompanied by (Select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td.p-r-5.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input').type('Test') //inputting frequency of pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting duration of pain
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td.p-b-5.ng-isolate-scope > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(8) > td.p-b-5.ng-isolate-scope > input').type('Test') //inputting intervention
         //Pace Maker
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('Test') //inputting brand/model
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('20') //inputting Rate setting
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > input').type('12/24/2021') //inputting Date implanted
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('12/03/2021') //inputting Date last tested
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking no
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Save Button
         cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
         cy.wait(5000)

      // Cardiopulmonary TAB End -----------
    });

    it('Automating Entry Form of Nutrition/Elimination Tab', function()
    {
      // Nutrition/Elimination TAB Start -----------
         
         cy.get('#oasis-tabs > label:nth-child(5)').click() //clicking Nutrition/Elimination TAB
         cy.wait(5000)
         //Upper GI Status
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-b-n > label > input').click() //click soft for abdomen
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('2') //inputting inches in girt
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-t-n.b-b-n > label > input').click() //clicking active for bowel sounds
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-t-n.b-b-n > label > input').click() //clicking good for appetite
         cy.wait(5000)
         //Other sypmtoms (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n.b-b-n.b-t-n.b-r-n > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.b-r-n > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(4) > input').type('Test') //inputting other symptom
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td.b-l-n.b-t-n.observation_intervention > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n.observation_intervention > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Other form for Vomiting
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking projectile for type
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking small for amount
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.b-t-n.b-l-n > label:nth-child(1) > input').click() //clicking watery for character\
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.b-t-n.b-l-n > span > input').type('Test') //inputting other character
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n > div > input').type('Test') //inputting frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.b-t-n.b-l-n.observation_intervention > div > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n.observation_intervention > div > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Nutrition/Diet (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div.pull-left.m-tb-0.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div.m-l-120 > div > div > input').type('6') //inputting ml forfluid restriction
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(4) > div.cont-opt.m-lopt-45 > input').type('Test') //inputting other nutrition/diet
         cy.wait(5000)

         //Enteral Nutrition
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking dobhoff for feeding via
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('12/06/2021') //inputting tube insertion date (mm/dd/yyyy)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking pump for formula delivery system
             
         cy.get('[name="SOONUTRI0043"]').type('4') //inputting amout for feeding formula
         cy.get('[name="SOONUTRI0044"]').type('4') //inputting ml for feeding formula
         cy.get('[name="SOONUTRI0045"]').type('4') //inputting amout for liquid supplemet
         cy.get('[name="SOONUTRI0046"]').type('4') //inputting ml for liquid supplemet
         cy.get('[name="SOONUTRI0053"]').type('4') //inputting ml for Pump rate per hour
         cy.get('[name="SOONUTRI0054"]').type('24') //inputting hours per day for Pump rate per hour
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking open system for enteral feeding system
         cy.get('[name="SOONUTRI0077"]').type('4') //inputting hours if residual volume over for hold feeling for
         cy.get('[name="SOONUTRI0078"]').type('4') //inputting ml for hold feeling for
         cy.get('[name="SOONUTRI0049"]').type('4') //inputting ml for Gastric residual amount
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td:nth-child(2) > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Tolerating feedings well?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for NPO?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > input').type('Test') //inputting NPO
         cy.wait(5000)
         //Ostomy care/feedings by: (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(12) > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(13) > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Genitourinary Status
         //Urine clarity (select all except clear)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td.p-b-5.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.p-b-5 > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.p-b-5 > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td.b-l-n.p-b-5.ng-isolate-scope > input').type('Test') //inputting other for urine clarity
         //Urine color
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking straw for urine color
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(3) > input').type('Test') //inputting other urine color
         //Urine Odor
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n > table > tbody > tr > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking yes for urine odor
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting description
         cy.wait(5000)
         //Abnormal elimination
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.subheadv3.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes Abnormal elimination
         //Abnormal elimination (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div.m-l-50 > div > input').type('Test') //inputting other for abnormal elimination
         cy.wait(5000)
         //Special procedures
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(7) > td.b-l-n.subheadv3.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes for Special procedures
         //Special procedures (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div:nth-child(2) > div > input').type('Test') //inputting other for special procedure
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td.p-5.b-l-n > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Indwelling catheter
         cy.get('#SOOELIMINATION0017').click() //clicking urethral for catheter type
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr.catheter__row > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 14 for catheter size
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr.balloon_inflation__row > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 5 for balloon inflation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 2-way for catheter lumens
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('3') //inputting days for catheter change
         cy.get('#sooelimination0156').type('05232021') //date last changed for catheter change
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > div.display-ib.checkbox.m-0.m-t-5.m-l-15 > label > input').click() //clicking done for catherter change
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking bedside bag for drainage bag
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking leg bag for drainage bag
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('3') //inputting days for MD-ordered irrigation frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > label:nth-child(4) > input').click() //clicking as needed for MD-ordered irrigation frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking done for MD-ordered irrigation frequency\
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > div > label.p-l-18.m-l-5.ng-isolate-scope > input').click() //clicking none for MD-ordered irrigation frequency
         cy.get('[name="SOOELIMINATION0055"]').type('24') //inputting amount for MD-ordered irrigation solution
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td:nth-child(2) > input:nth-child(2)').type('4') //inputting ml for MD-ordered irrigation solution
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td:nth-child(2) > div > label > input').click() //clicking none for MD-ordered irrigation solution
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         //Intermittent Catheterization
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 16 for catheter size
         cy.get('[name="SOOELIMINATION0153"]').type('3') //inputting for frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking day for frequency
         cy.get('[name="SOOELIMINATION0183"]').type('Test') //inputting other for frequency
         //Done By (select all)
         cy.get('[name="SOOELIMINATION0207"]').click()
         cy.get('[name="SOOELIMINATION0208"]').click()
         cy.get('[name="SOOELIMINATION0209"]').click()
         cy.get('[name="SOOELIMINATION0187"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0210"]').type('Test') //inputting intervention
         cy.wait(5000)
         //Nephrostomy
         cy.get('[name="SOOELIMINATION0174"]').type('3') //inputting days for Nephrostomy dressing change
         cy.get('[rname="SOOELIMINATION0197"]').click() //clicking as needed for Nephrostomy dressing change
         cy.get('[rname="SOOELIMINATION0214"]').click() //clicking done for Nephrostomy dressing change
         cy.get('[name="SOOELIMINATION0177"]').type('3') //inputting days for Nephrostomy bag change
         cy.get('[rname="SOOELIMINATION0198"]').click() //clicking as needed for Nephrostomy bag change
         cy.get('[rname="SOOELIMINATION0215"]').click() //clicking done for Nephrostomy bag change
         cy.get('[name="SOOELIMINATION0175"]').type('3') //inputting days for MD-ordered irrigation frequency
         cy.get('[rname="SOOELIMINATION0176"]').click() //clicking as needed for MD-ordered irrigation frequency
         cy.get('[rname="SOOELIMINATION0216"]').click() //clicking done for MD-ordered irrigation frequency
         cy.get('[rname="SOOELIMINATION0223"]').click() //clicking none for  MD-ordered irrigation frequency
         cy.get('[name="SOOELIMINATION0178"]').type('3') //inputting amount for
         cy.get('[name="SOOELIMINATION0179"]').type('3') //inputting ml for
         cy.get('[rname="SOOELIMINATION0224"]').click() //clicking none for
         cy.get('[name="SOOELIMINATION0185"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0200"]').type('Test') //inputting intervention
         cy.wait(5000)
         //Urostomy
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > input').type('3') //inputting days for Urostomy pouch change frequency:
         cy.get('[name="SOOELIMINATION0186"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0201"]').type('Test') //inputting intervention
         cy.wait(5000)
         //External Urinary Catheter
         cy.get('[rname="SOOELIMINATION0150"]').click() //clicking daily for Change frequency
         cy.get('[name="SOOELIMINATION0151"]').type('Test') //inputting other in Change frequency
         //Drainage bag (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(3) > td:nth-child(2) > label.checkbox.checkbox-inline.m-r-10.ng-isolate-scope > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(2) > input').click()
         cy.get('[name="SOOELIMINATION0188"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0202"]').type('Test') //inputting intervention
         cy.wait(5000)
         //Hemodialysis
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking av shunt for AV access
         cy.get('[name="SOOELIMINATION0032"]').type('Test') //inputting location for AV access
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(2) > td:nth-child(2) > label.checkbox.checkbox-inline.m-l-20.ng-isolate-scope > input').click() //clicking Permacath for AV access
         cy.get('[name="SOOELIMINATION0180"]').type('Test') //inputting other in AV access
         cy.get('#SOOELIMINATION0034').click() //clicking yes for Bruit present?
         cy.get('#SOOELIMINATION0033').click() //clicking yes for Thrill strong?
         cy.wait(5000)
         //Dialysis schedule (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(4) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(5) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(6) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(7) > input').click()
         cy.get('[name="SOOELIMINATION0036"]').type('Test') //inputting dialysis center
         cy.get('[name="SOOELIMINATION0037"]').type('09123456789') //inputting phone
         cy.get('[name="SOOELIMINATION0190"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0203"]').type('Test') //inputting intervention
         //Peritoneal Dialysis
         cy.get('#SOOELIMINATION0096').click() //clicking type
         cy.get('[name="SOOELIMINATION0094"]').type('Test') //inputting APD machine
         //Dialysate (select all)
         cy.get('[rname="SOOELIMINATION0097"]').click()
         cy.get('[rname="SOOELIMINATION0098"]').click()
         cy.get('[rname="SOOELIMINATION0099"]').click()
         cy.get('[rname="SOOELIMINATION0100"]').click()
         cy.get('[name="SOOELIMINATION0066"]').type('12:00') //inputting dwell time
         cy.get('[name="SOOELIMINATION0093"]').type('4') //inputting hours
         //Peritoneal dialysis done by (select all)
         cy.get('[name="SOOELIMINATION0158"]').click()
         cy.get('[name="SOOELIMINATION0159"]').click()
         cy.get('[name="SOOELIMINATION0160"]').click()
         cy.get('[name="SOOELIMINATION0193"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0204"]').type('Test') //inputting intervention
         //(M1600) - Has this patient been treated for a Urinary Tract Infection in the past 14 days?
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(17) > table > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
         //Lower GI Status
         cy.get('#SOOELIMINATION0171').type('12/04/2021') //inputting date last BM (mm/dd/yyyy)
         cy.wait(5000)
         //Bowel movement (select all)
         cy.get('[name="SOOELIMINATION0161"]').click()
         cy.get('[name="SOOELIMINATION0083"]').click()
         cy.get('[name="SOOELIMINATION0163"]').click()
         cy.get('[name="SOOELIMINATION0164"]').click()
         cy.get('[name="SOOELIMINATION0043"]').type('5')
         cy.get('[name="SOOELIMINATION0157"]').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking soft for stool character
         cy.get('[name="SOOELIMINATION0104_OTHER"]').type('Test') //inputting other for stool character
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking yellow/brown for stool color
         cy.get('[name="SOOELIMINATION0088"]').type('Test') //inputting other for stool character
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking effective for
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking MD notified for
         cy.get('[name="SOOELIMINATION0222"]').type('Test') //inputting MD notified
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(18) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking as needed for Laxative/Enema
         cy.get('[name="SOOELIMINATION0046_OTHER"]').type('Test') //inputting other for Laxative/Enema
         cy.get('[name="SOOELIMINATION0196"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0205"]').type('Test') //inputting intervention
         cy.wait(5000)
         //Lower GI Ostomy
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(2) > td:nth-child(2) > label.radio.radio-inline.m-r-40.ng-isolate-scope > input').click() //clicking colostomy for ostomy type
         cy.get('[name="SOOELIMINATION0068"]').type('12') //inputting cm for stoma diameter
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking healed for ostomy wound
         //Care done by (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(3) > input').click()
         cy.get('[name="SOOELIMINATION0227"]').type('Test') //inputting observation
         cy.get('[name="SOOELIMINATION0211"]').type('Test') //inputting intervention
         //(M1620) - Bowel Incontinence Frequency
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(20) > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click()
         //SAVE BUTTON
         cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
         cy.wait(5000)

      // Nutrition/Elimination TAB End -----------

    });

    it('Automating Entry Form of Neurologic/Behavioral Tab', function()
    {
      // Neurologic/Behavioral TAB Start -----------
         
         cy.get('#oasis-tabs > label:nth-child(6)').click() //clicking Neurologic/Behavioral TAB
         cy.wait(5000)
         //Neurological Status
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td.b-b-n.b-t-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left>right for size unequal
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(4) > td.b-t-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for non-reactive
         //Mental status (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(4) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-b-5.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(1) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td.p-b-5.ng-isolate-scope > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > label > input').click() //clicking adequate for sleep/rest
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > span:nth-child(4) > label > input').click() //clicking MD notified for sleep/rest
         //Hand grips
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td.b-b-n.b-t-n > label:nth-child(2) > input').click() //clicking left for strong
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td.b-b-n.b-t-n > label:nth-child(3) > input').click() //clicking right for strong
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(8) > td > label:nth-child(2) > input').click() //clicking left for weak
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(8) > td > label:nth-child(3) > input').click() //clicking right for weak
         cy.wait(5000)
         //Other signs (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(1) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(2) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(3) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(4) > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(5) > input').type('Test') //inputting other test
         cy.wait(5000)
         //Weakness
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(2) > input').click() //clicking left for Upper extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(3) > input').click() //clicking right for Upper extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label:nth-child(2) > input').click() //clicking left for Lower extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label:nth-child(3) > input').click() //clicking right for Lower extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td > label:nth-child(2) > input').click() //clicking left for Hemiparesis 
         //Paralysis
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking left for hemiplegia
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label > input').click() //clicking left for Paraplegia
         //Tremors
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking left for Upper extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking right for Upper extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking fine for Upper extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking left for Lower extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking right for Lower extremity
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking fine for Lower extremity
         //Seizure
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking grand mal
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label.ng-isolate-scope > input').type('12/06/2021') //inputting date of last seizure
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label.m-l-10 > input').type('50') //inputting duration (in seconds)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(14) > td:nth-child(2) > input').type('Test') //inputting observation
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(15) > td:nth-child(2) > input').type('Test') //inputting intervention
         cy.wait(5000)
         //(M1700) - Cognitive Functioning
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //(M1710) - When Confused (Reported or Observed Within the Last 14 Days)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //(M1720) - When Anxious (Reported or Observed Within the Last 14 Days)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr.molocked.ng-isolate-scope > td:nth-child(2) > table > tbody > tr > td > div:nth-child(2) > label > input').click()
         //(M1740) - Cognitive, behavioral, and psychiatric symptoms
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr:nth-child(2) > td > div > label > input').click()
         //(M1745) - Frequency of Disruptive Behavior Symptoms (Reported or Observed)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
         cy.wait(5000)
         //Thought Process, Affect and Behavioral Status (select all)
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.p-0 > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
         cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > input').type('Test') //inputting other in Thought Process, Affect and Behavioral Status
         cy.wait(5000)
         //Save Button
         cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
         cy.wait(5000)

      // Neurologic/Behavioral TAB End -----------

    });

    it.only('Automating Entry Form of ADL/IADL/Musculoskeletal Tab', function()
    {

      // ADL/IADL/Musculoskeletal TAB Start -----------

        cy.get('#oasis-tabs > label:nth-child(7)').click() //clicking Neurologic/Behavioral TAB
        cy.wait(5000)
        //Musculoskeletal Status
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > label > input').click() //clicking strong for muscle strength
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking limited for Range of motion\
        //limited (select all)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(2) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(3) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(4) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(5) > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking independent for Bed mobility
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking independent for Transfer ability
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.b-l-n > label > input').click() //clicking steady for Gait/Ambulation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.b-l-n > label > input').click() //clicking good for balance
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > span.m-l-5 > input').type('10') //inputting seconds for Timed Up & Go
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click() //clicnking Practiced once before actual test for Timed Up & Go
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > label:nth-child(4) > input').click() //clicnking Unable to perform for Timed Up & Go
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td.b-l-n > label > input').click() //clicking low for risk for falls
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for amputation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(2) > input').click() //clicking right for amputation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click() //clicking BK
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(4) > input').click() //clicking AK
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(5) > input').click() //clicking UE
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting other for amputation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking new for fracture
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting location for fracture
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click() //clicking cast?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(11) > td.p-l-5.p-r-5 > input').type('Test') //inputting observation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(12) > td.p-l-5.p-r-5 > input').type('Test') //inputting intervention
        cy.wait(5000)
        //If cast is present, assessment of extremity distal to cast
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking pink for color
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking strong for pulses
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > label.radio.radio-inline.m-l-5.ng-isolate-scope > input').click() //clicking < 3 sec for Capillary refill	
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking warm for temperature
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking normal for sensation
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking able to move for motor function
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking yes for swelling
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > input').type('Test') //inputting for intervention
        // Functional Limitations (select all)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.valign-top.ng-isolate-scope > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for functional limitations
        cy.wait(5000)
        //Activities Permitted (select all)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting others for activities permitted
        cy.wait(5000)
        //(M1800) - Grooming
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1810) - Current Ability to Dress Upper Body
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1820) - Current Ability to Dress Lower Body
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1830) - Bathing
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1840) - Toilet Transferring
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1850) - Transferring
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(6) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1860) - Ambulation/Locomotion
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(7) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        //(M1870) - Feeding or Eating
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(8) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
        cy.wait(5000)
        //Section GG Functional Abilities and Goals
        //GG0130. Self‐Care
        cy.get('#GG0130A3_chosen > a').click() //clicking input box of A for 1 SOC/ROC Performance
        cy.get('#GG0130A3_chosen > div > ul > li:nth-child(1)').click() //clicking result
        cy.get('#GG0130B3_chosen > a').click() //clicking input box of B for 1 SOC/ROC Performance
        cy.get('#GG0130B3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0130C3_chosen > a').click() //clicking input box of C for 1 SOC/ROC Performance
        cy.get('#GG0130C3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0130E3_chosen > a').click() //clicking input box of E for 1 SOC/ROC Performance
        cy.get('#GG0130E3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0130F3_chosen > a').click() //clicking input box of F for 1 SOC/ROC Performance
        cy.get('#GG0130F3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0130G3_chosen > a').click() //clicking input box of G for 1 SOC/ROC Performance
        cy.get('#GG0130G3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0130H3_chosen > a').click() //clicking input box of H for 1 SOC/ROC Performance
        cy.get('#GG0130H3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.wait(5000)
        //GG0170. Mobility
        cy.get('#GG0170A3_chosen > a').click() // clicking input box of A for 1 SOC/ROC Performance
        cy.get('#GG0170A3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170B3_chosen > a').click() // clicking input box of B for 1 SOC/ROC Performance
        cy.get('#GG0170B3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170C3_chosen > a').click() // clicking input box of C for 1 SOC/ROC Performance
        cy.get('#GG0170C3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170D3_chosen > a').click() // clicking input box of D for 1 SOC/ROC Performance
        cy.get('#GG0170D3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170E3_chosen > a').click() // clicking input box of E for 1 SOC/ROC Performance
        cy.get('#GG0170E3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170F3_chosen > a').click() // clicking input box of F for 1 SOC/ROC Performance
        cy.get('#GG0170F3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170G3_chosen > a').click() // clicking input box of G for 1 SOC/ROC Performance
        cy.get('#GG0170G3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170I3_chosen > a').click() // clicking input box of I for 1 SOC/ROC Performance
        cy.get('#GG0170I3_chosen > div > ul > li.active-result.highlighted').click() //clicking result
        cy.get('#GG0170J3_chosen > a').click() // clicking input box of J for 1 SOC/ROC Performance
        cy.get('#GG0170J3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170K3_chosen > a').click() // clicking input box of K for 1 SOC/ROC Performance
        cy.get('#GG0170K3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170L3_chosen > a').click() // clicking input box of L for 1 SOC/ROC Performance
        cy.get('#GG0170L3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170M3_chosen > a').click() // clicking input box of M for 1 SOC/ROC Performance
        cy.get('#GG0170M3_chosen > div > ul > li.active-result.highlighted').click() //clicking result
        cy.get('#GG0170N3_chosen > a').click() // clicking input box of N for 1 SOC/ROC Performance
        cy.get('#GG0170N3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170O3_chosen > a').click() // clicking input box of O for 1 SOC/ROC Performance
        cy.get('#GG0170O3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170P3_chosen > a').click() // clicking input box of P for 1 SOC/ROC Performance
        cy.get('#GG0170P3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170Q3_chosen > a').click() // clicking input box for Q
        cy.get('#GG0170Q3_chosen > div > ul > li:nth-child(2)').click() // clicking result
        cy.get('#GG0170R3_chosen > a').click() // clicking input box of R for 1 SOC/ROC Performance
        cy.get('#GG0170R3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170RR3_chosen > a').click() // clicking input box of RR3 for 1 SOC/ROC Performance
        cy.get('#GG0170RR3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170S3_chosen > a').click() // clicking input box of S for 1 SOC/ROC Performance
        cy.get('#GG0170S3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        cy.get('#GG0170SS3_chosen > a').click() // clicking input box of SS3 for 1 SOC/ROC Performance
        cy.get('#GG0170SS3_chosen > div > ul > li:nth-child(2)').click() //clicking result
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)
        
      // ADL/IADL/Musculoskeletal End Start -----------
    });

    it('Automating Entry Form of Medication Tab', function()
    {

      // Medication TAB Start -----------

        cy.get('#oasis-tabs > label:nth-child(8)').click() //clicking Medication TAB
        // (M2005) - Medication Intervention
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        // (M2016) - Patient/Caregiver Drug Education Intervention:
        cy.get('#M2016_DRUG_EDCTN_INTRVTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //(M2020) - Management of Oral Medications
        cy.get('#M2020_CRNT_MGMT_ORAL_MDCTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //Save Button
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)
      // Medication TAB End -----------
    });

    it('Automating Entry Form of Care Management', function()
    {

      // Care Management TAB Start -----------

        cy.get('#oasis-tabs > label:nth-child(9)').click() //clicking Care Management TAB
        cy.wait(5000)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking Caregiver always available and reliable for caregiving status
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > input').type('Test') //inputting caregiver name
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting relationship
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > input').type('1234567890') //inputting phone
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > input').type('Test') //inputting name of facility
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('1234567890') //inputting phone
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > input').type('Test') //inputting contact person
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > div > input').type('Test') //inputting Community resources utilized
        //(M2102) - Types and Sources of Assistance
        //a. ADL assistance
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //c. Medication administration
        cy.get('#M2102_CARE_TYPE_SRC_MDCTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //d. Medical procedures/ treatments
        cy.get('#M2102_CARE_TYPE_SRC_PRCDR > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //f. Supervision and safety
        cy.get('#M2102_CARE_TYPE_SRC_SPRVSN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //SAVE Button
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)
        // Care Management TAB Start -----------
    });

    it('Automating Entry Form of Emergent Care', function()
    {

      // Emergent Care TAB Start -----------

        cy.get('#oasis-tabs > label:nth-child(10)').click() //clicking Emergent Care TAB
        cy.wait(5000)
        //(M2301) - Emergent Care
        cy.get('#M2301_EMER_USE_AFTR_LAST_ASMT > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
        //(M2310) - Reason for Emergent Care (mark all that apply)
        cy.get('#M2310_ECR_MEDICATION').click()
        cy.get('#M2310_ECR_HYPOGLYC').click()
        cy.get('#M2310_ECR_OTHER').click()
        
        //DATA ITEMS COLLECTED AT INPATIENT FACILITY ADMISSION OR AGENCY DISCHARGE ONLY
        //(M2401) - Intervention Synopsis (clicking yes to all)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(7) > td:nth-child(3) > div > label > input').click()
        cy.wait(5000)
        // /(M2410) - To which Inpatient Facility has the patient been admitted?
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(9) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1 - Hospital
        //(M2420) - Discharge Disposition
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(11) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1
        //(M0906) - Discharge/Transfer/Death Date
        cy.get('#M0906_DC_TRAN_DTH_DT_DATEPICKER').type('07292022')
        
        //Section J: Health Conditions
        //J1800 - Any Falls Since SOC/ROC
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(16) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-YES
        //J1900 - Number of Falls Since SOC/ROC
        cy.get('[data-ng-model="data.J1900A"]').type('1')
        cy.get('#J1900B').type('1') //inputting 1 B. Injury (except major)
        cy.get('#J1900C').type('1') //inputting 1 C. Major injury
        //Additional Assessments
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(20) > td:nth-child(2) > div > div > textarea').type('Test')
        //Reasons for Discharge (mark all)
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(4) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(5) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(6) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(7) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(4) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(5) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div:nth-child(6) > label > input').click()
        cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(22) > td.oasis__answer > table > tbody > tr > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for reasons for discharge
        //SAVE BUTTON
        cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
        cy.wait(5000)
      
      // Emergent Care TAB Start -----------

    });
    
});