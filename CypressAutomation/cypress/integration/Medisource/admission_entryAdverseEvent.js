Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of Adverse Event', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(10000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Will M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                
                    cy.get(':nth-child(6) > .navtitle').click();// Adverse Event
                    cy.get('#parent > div > div > div > div > div.btn-group.pull-right.ng-scope.dropdown > button').click() //clicking new
                    
        })
           
        it('Automating Entry Form of Fail', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(1) > a').click() //clicking Fail
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get('.m-l-15 > .ng-valid').click() //clicking see MDO
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Fail - Post Fail Assessment', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(1) > li > a').click() //clicking Date Post Fail Assessment
            cy.wait(10000)
            cy.get('.btn__warning').click() //clicking EDIT button
            //Cause of Fall
            cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > div.oasis-cont.ng-scope > div > fieldset > table:nth-child(3) > tbody > tr:nth-child(2) > td > div')
            .invoke('show') // call jquery method 'show'
            .should('be.visible') // element is visible now
            .find('[name="fallcause"]')
            .type('Test', {force:true})
            //Assessment
            cy.get('[name="bp1"]').type('120') //inputting bp1
            cy.get('[name="bp2"]').type('180') //inputting bp2
            cy.get('tr > :nth-child(2) > .global__txtbox').type('80') //inputting hr
            cy.get(':nth-child(3) > .global__txtbox').type('80') //inputting resp
            cy.get(':nth-child(4) > .global__txtbox').type('80') //inputting temp
            cy.get(':nth-child(5) > .global__txtbox').type('80') //inputting o2 sat
            cy.get(':nth-child(6) > .global__txtbox').type('80') //inputting blood sugar
            cy.get(':nth-child(4) > :nth-child(1) > :nth-child(3) > td > .fg-line > .form-control').type('Test') //inputting Medications taken prior to fall
            cy.wait(5000)
            //Injury sustained
            cy.get('.p-t-5 > :nth-child(2) > .ng-scope').click() //clicking abrasions
            cy.get('.p-t-5 > .global__txtbox').type('Test') //inputting other
            cy.get('.p-t-5 > .form-control').type('Test') //inputting location of injury
            cy.get('[style="margin-left: 90px"] > .global__txtbox').type('Test') //inputting physician
            cy.get(':nth-child(3) > .b-t-n > .fg-line > .form-control').type('Test') //inputting administrative follow-up
            //Reviewed by
            cy.get('#cmReview_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#cmReview_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#cmReviewDateEsign').type('05302022') //inputting date
            cy.get('.d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(3) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })


        it('Automating Entry Form of Sign and Symptoms of Infection', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(2) > a').click() //clicking Sign and Symptoms of Infection
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Sign and Symptoms of Infection - Infection Log', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(3) > td:nth-child(2) > div:nth-child(2) > li > a').click() //clicking Date Infection Log
            cy.wait(10000)
            cy.get('.btn__warning').click() //clicking EDIT button
            //Reporting of identified infection
            //Reported by
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(1) > :nth-child(2) > :nth-child(1) > .ng-scope').click() //clicking MD
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(1) > :nth-child(2) > :nth-child(2) > .ng-scope').click() //clicking SN
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(1) > :nth-child(2) > :nth-child(3) > .ng-scope').click() //clicking Patient/PCG
            //Reported to
            cy.get('.p-t-5 > :nth-child(1) > .ng-scope').click() //clicking MD
            cy.get('.p-t-5 > :nth-child(2) > .ng-scope').click() //clicking SN
            cy.get('.p-t-5 > :nth-child(3) > .ng-scope').click() //clicking Other

            //Method of determination
            //Infection acquired from (click all)
            cy.get('[rname="infectionAcquiredfrom"]').click({multiple:true})
            //Identified thru
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(1) > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking Antibiotic therapy
            cy.get('#antibioticTherapyDate').type('05302022') //inputting date Antibiotic therapy
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking Urine Culture
            cy.get('#urineCultureDate').type('05302022') //inputting date Urine Culture
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(3) > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking Sputum Culture
            cy.get('#sputumCultureDate').type('05302022') //inputting date Sputum Culture
            cy.get(':nth-child(4) > :nth-child(1) > .global__inner-nb > tbody > :nth-child(4) > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking Wound Culture
            cy.get('#woundCultureDate').type('05302022') //inputting date Wound Culture
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-pristine').click() //clicking Other Culture
            cy.get('#otherCultureDate').type('05302022') //inputting date Other Culture
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-pristine').click() //clicking Lab Test
            cy.get('#labTestDate').type('05302022') //inputting date Lab Test
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-pristine').click() //clicking Chest-X-ray
            cy.get('#chestXrayDate').type('05302022') //inputting date Chest-X-ray
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-pristine').click() //clicking MRI
            cy.get('#mriDate').type('05302022') //inputting date MRI
            cy.wait(5000)
            cy.get(':nth-child(2) > :nth-child(1) > :nth-child(6) > td > .fg-line > .form-control').type('Test') //inputting sign and symptoms

            //Site of infection
            //EENT (click all)
            cy.get('[rname="eent"]').click({multiple:true})
            cy.get(':nth-child(2) > :nth-child(3) > .fg-line > .form-control').type('Test') //inputting notes
            //Integumentary (click all)
            cy.get('[rname="integumentary"]').click({multiple:true})
            cy.get(':nth-child(3) > :nth-child(3) > .fg-line > .form-control').type('Test') //inputting notes
            //Respiratory (click all)
            cy.get('[rname="respiratory"]').click({multiple:true})
            cy.get(':nth-child(4) > :nth-child(3) > .fg-line > .form-control').type('Test') //inputting notes
            //Genitourinary (click all)
            cy.get('[rname="genitourinary"]').click({multiple:true})
            cy.get(':nth-child(5) > :nth-child(3) > .fg-line > .form-control').type('Test') //inputting notes
            //Gastrointestinal (click all)
            cy.get('[rname="gastrointestinal"]').click({multiple:true})
            cy.get(':nth-child(6) > :nth-child(3) > .fg-line > .form-control').type('Test') //inputting notes
            //Musculoskeletal (click all)
            cy.get('[rname="musculoskeletal"]').click({multiple:true})
            cy.get(':nth-child(7) > :nth-child(3) > .fg-line > .form-control').type('Test') //inputting notes
            cy.get('[colspan="2"] > .fg-line > .form-control').type('Test') //inputting other
            //Patient access devices and/or ostomy present (click all)
            cy.get('[rname="patientAccessdeviceOstomy"]').click({multiple:true})
            cy.get('.m-l-35 > .global__txtbox').type('Test') //inputting other
            //rganism identified causing the infection (click all)
            cy.get('[rname="organismIdentified"]').click({multiple:true})
            cy.get(':nth-child(4) > :nth-child(1) > :nth-child(2) > td > .global__txtbox').type('Test') //inputting other
            //Treatment and interventions
            cy.get('.global__inner-nb > tbody > :nth-child(1) > :nth-child(2) > .global__txtbox').type('Test') //inputting Medication 1
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .global__txtbox').type('Test') //inputting Medication 2
            cy.get(':nth-child(3) > :nth-child(2) > .global__txtbox').type('Test') //inputting Medication 3
            cy.get(':nth-child(4) > :nth-child(2) > .global__txtbox').type('Test') //inputting Other treatment
            cy.get(':nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') //inputting Patient education
            cy.wait(5000)
            //Follow-up
            cy.get('#followup1Date').type('05302022') //inputting date
            cy.get(':nth-child(3) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for resolve
            cy.get(':nth-child(3) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking yes for continue rx
            cy.get(':nth-child(3) > :nth-child(4) > .fg-line > .form-control').type('Test') //inputting notes
            cy.get('#followup2Date').type('05302022') //inputting date
            cy.get(':nth-child(4) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for resolve
            cy.get(':nth-child(4) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking yes for continue rx
            cy.get(':nth-child(4) > :nth-child(4) > .fg-line > .form-control').type('Test') //inputting notes
            cy.get('#followup3Date').type('05302022') //inputting date
            cy.get(':nth-child(5) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for resolve
            cy.get('tbody > :nth-child(5) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking yes for continue rx
            cy.get(':nth-child(5) > :nth-child(4) > .fg-line > .form-control').type('Test') //inputting notes

            //Prepared by
            cy.get(':nth-child(6) > tbody > tr > :nth-child(2) > .global__txtbox').type('Test')

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })


        it('Automating Entry Form of Emergency Room Service', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(3) > a').click() //clicking Emergency Room Service
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Emergency Room Service - Comm/MDO', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(4) > td:nth-child(2) > div:nth-child(3) > li > a').click() //clicking Date Emergency Room Service - Comm/MDO
            cy.wait(10000)
            cy.get('#orderTime > .time-mask').type('1200') //inputting order time
            cy.get('#sentDate').type('05302022') //inputting sent date
            cy.get('#receiveDate').type('05302022') //inputting receiving date
            //Subject Matter (click all)
            cy.get('.m-b-5 > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .global__txtbox').type('Test') //inputting other
            //Comm. Mode (click all)
            cy.get('.oasis__answer > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            //Communication Note
            //Use Template
            cy.get('#mdordertemplate_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#mdordertemplate_chosen > div > ul > li.active-result.highlighted').click() //clicking result
            //Physician Order
            cy.get('#physicianordernotes').type('Test') //inputting physician order
            cy.wait(5000)
            //Notification and Care Coordination
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking MD
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MD
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking Result MD
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get('tbody > :nth-child(4) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown LVN
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result LVN
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(5) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > ul > li').click() //clicking Result PT
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(6) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result PTA
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get(':nth-child(7) > :nth-child(2) > .select > .form-control').type('Test') //inputting caregiver
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MSW
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result MSW
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get(':nth-child(2) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Dietician
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result Dietician
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown CHHA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result CHHA
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Healthcare Vendor
            cy.get(':nth-child(4) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Healthcare Vendor
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > div > div > ul > li:nth-child(2)').click() //clicking Result Healthcare Vendor
            cy.get(':nth-child(5) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result OT
            cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result OTA
            cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown ST
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result ST
            cy.get(':nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking other
            cy.get(':nth-child(8) > :nth-child(5) > .select > .form-control').type('Test') //inputting other
            cy.wait(5000)
            //Link Task
            cy.get('.chosen-choices').click() //clicking Dropdown
            cy.get('#tasks_chosen > div > ul > li:nth-child(1)').click() //clicking Result

            cy.get('.m-r-5 > .global__txtbox').type('Vince1206') //inputting e-signature
            //Click Sign Button
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div.hh-esign.ng-scope.ng-isolate-scope > div > div.ng-scope > form > div:nth-child(2) > div:nth-child(2) > button').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Re-Hospitalization', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(4) > a').click() //clicking Re-Hospitalization
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Re-Hospitalization - Comm/MDO', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(5) > td:nth-child(2) > div:nth-child(3) > li > a').click() //clicking Date Re-Hospitalization - Comm/MDO
            cy.wait(10000)
            cy.get('#orderTime > .time-mask').type('1200') //inputting order time
            cy.get('#sentDate').type('05302022') //inputting sent date
            cy.get('#receiveDate').type('05302022') //inputting receiving date
            //Subject Matter (click all)
            cy.get('.m-b-5 > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .global__txtbox').type('Test') //inputting other
            //Comm. Mode (click all)
            cy.get('.oasis__answer > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            //Communication Note
            //Use Template
            cy.get('#mdordertemplate_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#mdordertemplate_chosen > div > ul > li.active-result.highlighted').click() //clicking result
            //Physician Order
            cy.get('#physicianordernotes').type('Test') //inputting physician order
            cy.wait(5000)
            //Notification and Care Coordination
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking MD
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MD
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking Result MD
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get('tbody > :nth-child(4) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown LVN
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result LVN
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(5) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > ul > li').click() //clicking Result PT
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(6) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result PTA
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get(':nth-child(7) > :nth-child(2) > .select > .form-control').type('Test') //inputting caregiver
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MSW
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result MSW
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get(':nth-child(2) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Dietician
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result Dietician
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown CHHA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result CHHA
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Healthcare Vendor
            cy.get(':nth-child(4) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Healthcare Vendor
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > div > div > ul > li:nth-child(2)').click() //clicking Result Healthcare Vendor
            cy.get(':nth-child(5) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result OT
            cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result OTA
            cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown ST
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result ST
            cy.get(':nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking other
            cy.get(':nth-child(8) > :nth-child(5) > .select > .form-control').type('Test') //inputting other
            cy.wait(5000)
            //Link Task
            cy.get('.chosen-choices').click() //clicking Dropdown
            cy.get('#tasks_chosen > div > ul > li:nth-child(1)').click() //clicking Result

            cy.get('.m-r-5 > .global__txtbox').type('Vince1206') //inputting e-signature
            //Click Sign Button
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div.hh-esign.ng-scope.ng-isolate-scope > div > div.ng-scope > form > div:nth-child(2) > div:nth-child(2) > button').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })


        it('Automating Entry Form of Medication Related Problems', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(5) > a').click() //clicking Medication Related Problem
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Medication Related Problems - Comm/MDO', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(6) > td:nth-child(2) > div:nth-child(3) > li > a').click() //clicking Date Medication Related Problems - Comm/MDO
            cy.wait(10000)
            cy.get('#orderTime > .time-mask').type('1200') //inputting order time
            cy.get('#sentDate').type('05302022') //inputting sent date
            cy.get('#receiveDate').type('05302022') //inputting receiving date
            //Subject Matter (click all)
            cy.get('.m-b-5 > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .global__txtbox').type('Test') //inputting other
            //Comm. Mode (click all)
            cy.get('.oasis__answer > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            //Communication Note
            //Use Template
            cy.get('#mdordertemplate_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#mdordertemplate_chosen > div > ul > li.active-result.highlighted').click() //clicking result
            //Physician Order
            cy.get('#physicianordernotes').type('Test') //inputting physician order
            cy.wait(5000)
            //Notification and Care Coordination
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking MD
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MD
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking Result MD
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get('tbody > :nth-child(4) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown LVN
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result LVN
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(5) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > ul > li').click() //clicking Result PT
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(6) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result PTA
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get(':nth-child(7) > :nth-child(2) > .select > .form-control').type('Test') //inputting caregiver
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MSW
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result MSW
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get(':nth-child(2) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Dietician
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result Dietician
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown CHHA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result CHHA
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Healthcare Vendor
            cy.get(':nth-child(4) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Healthcare Vendor
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > div > div > ul > li:nth-child(2)').click() //clicking Result Healthcare Vendor
            cy.get(':nth-child(5) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result OT
            cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result OTA
            cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown ST
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result ST
            cy.get(':nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking other
            cy.get(':nth-child(8) > :nth-child(5) > .select > .form-control').type('Test') //inputting other

            //Link Task
            cy.get('.chosen-choices').click() //clicking Dropdown
            cy.get('#tasks_chosen > div > ul > li:nth-child(1)').click() //clicking Result

            cy.get('.m-r-5 > .global__txtbox').type('Vince1206') //inputting e-signature
            //Click Sign Button
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div.hh-esign.ng-scope.ng-isolate-scope > div > div.ng-scope > form > div:nth-child(2) > div:nth-child(2) > button').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Critical Lab Result', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(6) > a').click() //clicking Critical Lab Result
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Critical Lab Result - Comm/MDO', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(7) > td:nth-child(2) > div:nth-child(3) > li > a').click() //clicking Date Critical Lab Result - Comm/MDO
            cy.wait(10000)
            cy.get('#orderTime > .time-mask').type('1200') //inputting order time
            cy.get('#sentDate').type('05302022') //inputting sent date
            cy.get('#receiveDate').type('05302022') //inputting receiving date
            //Subject Matter (click all)
            cy.get('.m-b-5 > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .global__txtbox').type('Test') //inputting other
            //Comm. Mode (click all)
            cy.get('.oasis__answer > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            //Communication Note
            //Use Template
            cy.get('#mdordertemplate_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#mdordertemplate_chosen > div > ul > li.active-result.highlighted').click() //clicking result
            //Physician Order
            cy.get('#physicianordernotes').type('Test') //inputting physician order
            cy.wait(5000)
            //Notification and Care Coordination
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking MD
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MD
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking Result MD
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get('tbody > :nth-child(4) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown LVN
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result LVN
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(5) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > ul > li').click() //clicking Result PT
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(6) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result PTA
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get(':nth-child(7) > :nth-child(2) > .select > .form-control').type('Test') //inputting caregiver
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MSW
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result MSW
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get(':nth-child(2) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Dietician
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result Dietician
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown CHHA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result CHHA
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Healthcare Vendor
            cy.get(':nth-child(4) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Healthcare Vendor
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > div > div > ul > li:nth-child(2)').click() //clicking Result Healthcare Vendor
            cy.get(':nth-child(5) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result OT
            cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result OTA
            cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown ST
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result ST
            cy.get(':nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking other
            cy.get(':nth-child(8) > :nth-child(5) > .select > .form-control').type('Test') //inputting other

            //Link Task
            cy.get('.chosen-choices').click() //clicking Dropdown
            cy.get('#tasks_chosen > div > ul > li:nth-child(1)').click() //clicking Result

            cy.get('.m-r-5 > .global__txtbox').type('Vince1206') //inputting e-signature
            //Click Sign Button
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div.hh-esign.ng-scope.ng-isolate-scope > div > div.ng-scope > form > div:nth-child(2) > div:nth-child(2) > button').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })


        it('Automating Entry Form of Vital Signs Beyond Parameter', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(7) > a').click() //clicking Vital Signs Beyond Parameter
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Vital Signs Beyond Parameter - Comm/MDO', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(8) > td:nth-child(2) > div:nth-child(3) > li > a').click() //clicking Date Vital Signs Beyond Parameter
            cy.wait(10000)
            cy.get('#orderTime > .time-mask').type('1200') //inputting order time
            cy.get('#sentDate').type('05302022') //inputting sent date
            cy.get('#receiveDate').type('05302022') //inputting receiving date
            //Subject Matter (click all)
            cy.get('.m-b-5 > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .global__txtbox').type('Test') //inputting other
            //Comm. Mode (click all)
            cy.get('.oasis__answer > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            //Communication Note
            //Use Template
            cy.get('#mdordertemplate_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#mdordertemplate_chosen > div > ul > li.active-result.highlighted').click() //clicking result
            //Physician Order
            cy.get('#physicianordernotes').type('Test') //inputting physician order
            cy.wait(5000)
            //Notification and Care Coordination
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking MD
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MD
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking Result MD
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get('tbody > :nth-child(4) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown LVN
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result LVN
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(5) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > ul > li').click() //clicking Result PT
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(6) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result PTA
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get(':nth-child(7) > :nth-child(2) > .select > .form-control').type('Test') //inputting caregiver
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MSW
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result MSW
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get(':nth-child(2) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Dietician
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result Dietician
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown CHHA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result CHHA
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Healthcare Vendor
            cy.get(':nth-child(4) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Healthcare Vendor
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > div > div > ul > li:nth-child(2)').click() //clicking Result Healthcare Vendor
            cy.get(':nth-child(5) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result OT
            cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result OTA
            cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown ST
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result ST
            cy.get(':nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking other
            cy.get(':nth-child(8) > :nth-child(5) > .select > .form-control').type('Test') //inputting other

            //Link Task
            cy.get('.chosen-choices').click() //clicking Dropdown
            cy.get('#tasks_chosen > div > ul > li:nth-child(1)').click() //clicking Result

            cy.get('.m-r-5 > .global__txtbox').type('Vince1206') //inputting e-signature
            //Click Sign Button
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div.hh-esign.ng-scope.ng-isolate-scope > div > div.ng-scope > form > div:nth-child(2) > div:nth-child(2) > button').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })


        it('Automating Entry Form of Other', function() {
            cy.get('.btn-group > .dropdown-menu > :nth-child(8) > a').click() //clicking Other Event
            cy.wait(10000)
            //Physician
            cy.get('#physician_chosen > .chosen-single').click() //clicking dropdown physician
            cy.get('#physician_chosen > div > ul > li.active-result.highlighted').click() //clicking result

            cy.get('#incidentDate').type('05302022') //inputting date of incident
            cy.get('.time-mask').type('1200') //inputting time
            cy.get('[width="56%"] > .fg-line > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting Name of witness, if any
            cy.get('[style="margin-left: 65px;"] > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test') //inputting Description of incident including extent of injury, if any:
            //Notifications
            cy.get('[style="width:40%;"] > .pull-left > .radio > .ng-valid').click() //clicking physician
            cy.get('[style="width:20%;"] > .m-b-5 > .global__txtbox').type('05/30/2022') //inputting date
            cy.get(':nth-child(1) > :nth-child(3) > .m-b-5 > .global__txtbox').type('Test') //inputting by
            cy.wait(5000)
            //Follow up action 
            cy.get(':nth-child(5) > tbody > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Resolution – Was the incident resolved?
            cy.get('#resolutionDateResolved').type('05302022') //inputting date resolve
            cy.get(':nth-child(4) > .radio > .ng-valid').click() //clicking NO
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(2) > td > .fg-line > .form-control').type('Test')
            //Clinicians involved
            cy.get('#initialReportBy_chosen > .chosen-single').click() //clicking dropdown clinician
            cy.get('#initialReportBy_chosen > div > ul > li:nth-child(20)').click() //clicking result
            cy.get('#initialReportByDateEsign').type('05302022') //inputting date
            cy.get(':nth-child(2) > :nth-child(4) > .ng-scope > .d-ib > .global__txtbox').type('Vince1206') //inputting e-signature

            //Click Sign Button
            cy.get(':nth-child(2) > :nth-child(5) > .ng-scope > .btn').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })

        it('Automating Entry Form of Other - Comm/MDO', function() {
            cy.get('#parent > div > div > div > div > fieldset > table > tbody > tr:nth-child(9) > td:nth-child(2) > div:nth-child(3) > li > a').click() //clicking Date Other
            cy.wait(10000)
            cy.get('#orderTime > .time-mask').type('1200') //inputting order time
            cy.get('#sentDate').type('05302022') //inputting sent date
            cy.get('#receiveDate').type('05302022') //inputting receiving date
            //Subject Matter (click all)
            cy.get('.m-b-5 > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.m-b-5 > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .global__txtbox').type('Test') //inputting other
            //Comm. Mode (click all)
            cy.get('.oasis__answer > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            cy.get('.oasis__answer > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click()
            //Communication Note
            //Use Template
            cy.get('#mdordertemplate_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#mdordertemplate_chosen > div > ul > li.active-result.highlighted').click() //clicking result
            //Physician Order
            cy.get('#physicianordernotes').type('Test') //inputting physician order
            cy.wait(5000)
            //Notification and Care Coordination
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking MD
            cy.get('tbody > :nth-child(2) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MD
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking Result MD
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get('tbody > :nth-child(4) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown LVN
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result LVN
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(5) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > ul > li').click() //clicking Result PT
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(6) > :nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown PTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking Result PTA
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get(':nth-child(7) > :nth-child(2) > .select > .form-control').type('Test') //inputting caregiver
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown MSW
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result MSW
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get(':nth-child(2) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Dietician
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result Dietician
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown CHHA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result CHHA
            cy.get(':nth-child(4) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Healthcare Vendor
            cy.get(':nth-child(4) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown Healthcare Vendor
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > div > div > ul > li:nth-child(2)').click() //clicking Result Healthcare Vendor
            cy.get(':nth-child(5) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OT
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > div > div > div > ul > li').click() //clicking Result OT
            cy.get(':nth-child(6) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown OTA
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result OTA
            cy.get(':nth-child(7) > :nth-child(4) > .checkbox > .ng-isolate-scope > .ng-valid').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(5) > .select > .chosen-container > .chosen-single').click() //clicking Dropdown ST
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div:nth-child(1) > div > table.table.global__table.m-t-15 > tbody > tr:nth-child(10) > td > table > tbody > tr:nth-child(7) > td:nth-child(5) > div > div > div > ul > li:nth-child(1)').click() //clicking Result ST
            cy.get(':nth-child(4) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking other
            cy.get(':nth-child(8) > :nth-child(5) > .select > .form-control').type('Test') //inputting other

            //Link Task
            cy.get('.chosen-choices').click() //clicking Dropdown
            cy.get('#tasks_chosen > div > ul > li:nth-child(1)').click() //clicking Result

            cy.get('.m-r-5 > .global__txtbox').type('Vince1206') //inputting e-signature
            //Click Sign Button
            cy.get('#parent > div > ng-form > div > fieldset > div.card.b-a.b-1.b-lightgray > div > div.hh-esign.ng-scope.ng-isolate-scope > div > div.ng-scope > form > div:nth-child(2) > div:nth-child(2) > button').click()

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
            cy.wait(10000)
        })






    })