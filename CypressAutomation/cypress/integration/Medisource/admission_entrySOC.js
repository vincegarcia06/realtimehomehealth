Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of OASIS - Start of Care (SOC)', () => {
           
        beforeEach(() => {
            cy.login('superagent@geekers', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://app.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
            cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
               cy.get('.searchbar__content > .ng-pristine').type('Peralta')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)

                    //Visit the Added Patient
                    // cy.visit('https://app.medisource.com/patientcare/D9956A06-1A8B-4E80-81CD-8AB99C2CA8F9/0FED06A0-D9D6-4701-B259-59104C49F117/overview')

                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(2) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click SOC
            
        })
        
        it('Automating Entry Form of Demographics / Clinical Record', function() {

                //DEMOGRAPHICS/CLINICAL RECORD TAB Start -------------
                
                    const dayjs = require('dayjs')
                    cy.get('#ti').type('1200') //inputting time in
                    cy.get('#to').type('1900') //inputting time out
                    cy.get('#M0064_SSN').type('111111111') //iNputting social security number
                    cy.get('#M0090_INFO_COMPLETED_DT').type(dayjs().format('MM/DD/YYYY'))  //inputting date assessment completed
                    cy.get('#M0102_PHYSN_ORDRD_SOCROC_DT_NA > input').click()// clicking No Specific SOC date ordered by physician
                    cy.get('#M0104_PHYSN_RFRL_DT').type(dayjs().format('MM/DD/YYYY')) //inputting referral date
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(28) > td.oasis__answer > table > tbody > tr > td.oasis-w-30 > div > div:nth-child(1) > label > input').click() //select early
                    
                     // selecting all that apply for Race/Ethnicity
                    cy.get('#M0140_ETHNIC_AI_AN > input').click()
                    cy.get('#M0140_ETHNIC_ASIAN > input').click()
                    cy.get('#M0140_ETHNIC_BLACK > input').click()  
                    cy.get('#M0140_ETHNIC_HISP > input').click()
                    cy.get('#M0140_ETHNIC_NH_PI > input').click()
                    cy.get('#M0140_ETHNIC_WHITE > input').click()

                    //selecting all that apply for Current Payment Sources for Home Care
                    cy.get('#M0150_CPAY_MCARE_FFS > input').click()

                    // selecting all that apply from which of the following Inpatient Facilities was the patient discharged within the past 14 days?
                    cy.get('#M1000_DC_LTC_14_DA > input').click()
                    cy.get('#M1000_DC_SNF_14_DA > input').click()
                    cy.get('#M1000_DC_IPPS_14_DA > input').click()
                    cy.get('#M1000_DC_LTCH_14_DA > input').click()
                    cy.get('#M1000_DC_IRF_14_DA > input').click()
                    cy.get('#M1000_DC_PSYCH_14_DA > input').click()
                    cy.get('#M1000_DC_OTH_14_DA > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(34) > td.oasis__answer > table > tbody > tr > td.v-top > div > div.display-block > div.m-l-160 > input').type('Test')

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(36) > td.oasis__answer > table > tbody > tr > td.oasis-w-120-px > div > img').click()//clicking the calendar overly icon
                    cy.get('#M1005_INP_DISCHARGE_DT').type(dayjs().subtract(10, 'day').format('MM/DD/YYYY')) //inputting inpatient discharge date
                    //Save Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // Click Save Button
                    cy.wait(5000)
                    //DEMOGRAPHICS/CLINICAL RECORD TAB End -------------
        });

        it('Automating Entry Form of Diagnoses / Medical History Tab', function() {

                //DIAGNOSES/MEDICAL HISTORY TAB Start -------------
                    const dayjs = require('dayjs')
                    // Clicking and go to diagnoses/medical history
                    cy.get('#oasis-tabs > label:nth-child(2)').click()
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr > td > label > input').click() // Clicking Review OASIS Guidelines
                    // (M1021) Primary Diagnosis Information Section
                    cy.get('#tooltip_a > table > tbody > tr:nth-child(1) > td:nth-child(2) > oasisv4-icd-opt > div > div.fg-line > input').type('A');
                    cy.wait(3000)
                    cy.get('[code="A00.0"]').click()
                    cy.wait(3000)
                    // (M1028) Active Diagnoses – Comorbidities and Co-existing Conditions - Check all that apply (Note: except none of the above)
                    cy.get('#M1028_ACTV_DIAG_PVD_PAD > input').click()
                    cy.get('#M1028_ACTV_DIAG_DM > input').click()
                    // (M1030) Therapies the patient receives at home - Check all that apply (Note: except none of the above)
                    cy.get('#M1030_THH_IV_INFUSION > input').click()
                    cy.get('#M1030_THH_PAR_NUTRITION > input').click()
                    cy.get('#M1030_THH_ENT_NUTRITION > input').click()
                    // (M1033) Risk for Hospitalization: Which of the following signs or symptoms characterize this patient as at risk for hospitalization? - Check all that apply (Note: except none of the above)
                    cy.get('#M1033_HOSP_RISK_HSTRY_FALLS > input').click()
                    cy.get('#M1033_HOSP_RISK_WEIGHT_LOSS > input').click()
                    cy.get('#M1033_HOSP_RISK_MLTPL_HOSPZTN > input').click()
                    cy.get('#M1033_HOSP_RISK_MLTPL_ED_VISIT > input').click()
                    cy.get('#M1033_HOSP_RISK_MNTL_BHV_DCLN > input').click()
                    cy.get('#M1033_HOSP_RISK_COMPLIANCE > input').click()
                    cy.get('#M1033_HOSP_RISK_5PLUS_MDCTN > input').click()
                    cy.get('#M1033_HOSP_RISK_CRNT_EXHSTN > input').click()
                    cy.get('#M1033_HOSP_RISK_OTHR_RISK > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table.table.oasis__table.otv12.m-t-12.ng-scope > tbody > tr:nth-child(6) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(9) > div.m-l-230 > input').type('Test') //inputting other risk
                    // (M1060) Height and Weight Information Section
                    cy.get('#M1060_HEIGHT_A > input').type('60') //inputting height(inches)
                    cy.get('#M1060_WEIGHT_B > input').type('100') //inputting weight(pounds)
                    
                    // Past Medical History Information Section
                    // Sensory Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting other problems in sensory
                    // Integumentary Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting other problems in integumentary
                    // Endocrine Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting other problems in endocrine
                    // Respiratory Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div > input').type('Test') //inputting other problems in respiratory
                    // Cardiovascular Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(11) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div > input').type('Test') //inputting other problems in cardiovascular
                    // Gastrointestinal Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(13) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting other problems in gastrointestinal
                    // Genitourinary Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(15) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting other problems in genitourinary
                    // Neurological Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(17) > td.b-l-0 > table > tbody > tr > td:nth-child(4) > div.cont-opt.m-lopt-50.ng-isolate-scope > div > input').type('Test') //inputting other problems in neurological
                    // Musculoskeletal Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(19) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > div.p-0 > input').type('Test') //inputting other problems in musculoskeletal
                    // Circulatory Problems (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(21) > td.b-l-0 > table > tbody > tr > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(21) > td.b-l-0 > table > tbody > tr > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(21) > td.b-l-0 > table > tbody > tr > td:nth-child(3) > input').type('Test')                    
                    // Other history (Note: click all except no history)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(23) > td.b-l-0.ng-isolate-scope > table > tbody > tr:nth-child(1) > td:nth-child(3) > div.cont-opt.m-lopt-30.ng-isolate-scope > div > input').type('Test') //inputting other history
                    
                    // Surgical history
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(24) > td.subheadv3.b-l-0 > table > tbody > tr > td.ng-isolate-scope > input').type('Test') //inputting surgeries
                    // Hospitalizations
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(25) > td > table > tbody > tr > td.ng-isolate-scope > input').type('Test') //inputting hospitalizations

                    // Other Medication Information Section
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-scope > td > label:nth-child(1) > input').type('Chest X-ray') //inputting exam type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-scope > td > label:nth-child(3) > input').type('04/02/2022') //inputting exam date
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-scope > td > label:nth-child(5) > input').type('Test') // inputting exam result
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(6) > td > input').type('03/04/2022') //inputting contacted last date physician
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(7) > td > input').type('03/04/2022') //inputting visit last date physician
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(8) > td > input').type('Test') //inputting reason for contact or visit
                    
                    // Immunizations
                    //Influenza vaccine (October to March)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td > label:nth-child(1) > input').click() //clicking yes for influenza vaccine
                    cy.get('[name="SOOMEDICAL0058"]').type('02/04/2022') //inputting date of vaccine
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td > label.radio.radio-inline.display-ib.p-b-5.ng-isolate-scope > input').click() //clicking vaccine received from
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td > input:nth-child(8)').type('Test') //inputting other
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(4) > td > table > tbody > tr > td > input').type('Test') //inputting reason
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td > label.radio.radio-inline.ng-isolate-scope > input').click() //clicking offered
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td > label:nth-child(1) > input').click() //clicking Agreed to receive influenza vaccine from
                    // Pneumonia vaccine (every 5 years)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(8) > td > label:nth-child(1) > input').click() //clicking yes for pneumonia vaccine
                    cy.get('[name="SOOMEDICAL0062"]').type('03/06/2022') //inputting date of vaccine
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(8) > td > label.radio.radio-inline.display-ib.p-b-5.ng-isolate-scope > input').click() //clicking vaccine received from
                    cy.get('[name="SOOMEDICAL0152"]').type('Test') //inputting other
                    cy.get('[name="SOOMEDICAL0063"]').type('Test') //inputting reason
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(10) > td > label.radio.radio-inline.ng-isolate-scope > input').click() //clicking offered
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(11) > td > label:nth-child(1) > input').click() // clicking Agreed to receive pneumococcal vaccine from
                    //Other immunizations and tests
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(13) > td:nth-child(2) > div:nth-child(1) > label > input').click() // clicking Yes for shingles test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(13) > td:nth-child(2) > div:nth-child(1) > input').type('03/04/2022') //inputting date for shingle test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(14) > td:nth-child(2) > div:nth-child(1) > label > input').click('') // clicking Yes for TB skin test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(14) > td:nth-child(2) > div:nth-child(1) > input').type('03/04/2022') //inputting date for TB skin test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(13) > td:nth-child(4) > div:nth-child(1) > label > input').click() // clicking Yes for Hepatitis B test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(13) > td:nth-child(4) > div:nth-child(1) > input').type('03/04/2022') //inputting date for Hepatitis B test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(14) > td:nth-child(4) > div:nth-child(1) > label > input').click() //clicking Yes for Tetanus test
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(14) > td:nth-child(4) > div:nth-child(1) > input').type('03/04/2022') //inputting date for Tetanus test

                    //Save Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)

                // DIAGNOSES/MEDICAL HISTORY TAB End -------------
        });

        it('Automating Entry Form of Vital Signs / Sensory Tab', function() {
                
                // VITAL SIGNS/SENSORY TAB Start -------------
                    const dayjs = require('dayjs')
                    // Clicking and go to vital signs/sensory tab
                    cy.get('#oasis-tabs > label:nth-child(3)').click() //clicking vital signs/sensory
                    cy.wait(5000)
                    //Vitals Sign Information Section
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > input').type('98') // inputting temperature
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking oral for temperature
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > input').type('3') //inputting Pulse/HR
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking radial for Pulse/HR
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > input').type('3') //inputting respiration
                    cy.get('[name="SOOVS0009"]').type('180') //inputting Systolic - BP Left Arm
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120')  //inputting Diastolic - BP Left Arm
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
                    cy.get('[name="SOOVS0012"]').type('180') //inputting Systolic - BP Right Arm
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - BP Right Arm
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr > td.ng-isolate-scope > div > label:nth-child(1) > input').click() //clicking sitting
                    cy.get('[name="SOOVS0015"]').type('180') //inputting Systolic - BP Left Leg
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - BP Left Leg
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
                    cy.get('[name="SOOVS0018"]').type('180') //inputting Systolic - BP Right Leg
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(2) > input:nth-child(3)').type('120') //inputting Diastolic - Right Leg
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking sitting
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(9) > td > table > tbody > tr > td:nth-child(2) > input').type('2') //inputting O2 Saturation % on room air
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('2') //inputting O2 Saturation % on O2
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(3) > input').type('2') //inputting lPM on o2 Saturation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting blood sugar
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking FBS
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(12) > td > table > tbody > tr > td.p-l-5 > input').type('100') //inputting weight
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(12) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking actual
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(13) > td > table > tbody > tr > td:nth-child(2) > input').type('60') //inputting height
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(13) > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking actual
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-5.b-r-n.b-l-n > div.m-t-2.p-0.display-ib.ng-isolate-scope > label:nth-child(2) > input').click() //clicking Yes for evidence for infection
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-5.b-r-n.b-l-n > div:nth-child(2) > input').type('test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > label:nth-child(1) > input').click() //clicking MD in notified
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div > label:nth-child(2) > input').click() //clicking CM/Supervisor
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div.m-t-2.p-0.display-ib.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes for new/change/hold medications
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(2) > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr > td > table > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention

                    // (M1242) Frequency of Pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(3) > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1 for frequency pain

                    // Pain Location
                    // Location 1
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1) > input').type('Test') //inputting pain location
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > a').click() //clicking dropdown for type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > a').click() //clicking dropdown for present level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(5)').click() //clicking present level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > a').click() //clicking dropdown worst level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(5)').click() //clicking worst level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > a').click() //clicking acceptable level dropdown
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(5)').click() //clicking acceptable level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > a').click() //clicking level after meds dropdown
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(4)').click() //clicking level after meds
                    // Location 2
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1) > input').type('Test') //inputting pain location
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > a').click() //clicking dropdown for type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > a').click() //clicking dropdown for present level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(2)').click() //clicking present level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > a').click() //clicking dropdown worst level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(6)').click() //clicking worst level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > a').click() //clicking acceptable level dropdown
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(6)').click() //clicking acceptable level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > a').click() //clicking level after meds dropdown
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(7)').click() //clicking level after meds
                    //Character Pain(Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr.ng-isolate-scope > td:nth-child(4) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label > input').click()
                    //Non-verbal signs of pain (Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > label > input').click()
                    //What makes the pain better?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > input').type('Test')
                    //What makes the pain worse?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div.m-0.m-b-5.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(4) > div > input').type('Test')
                    //Pain medication
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div.display-ib.m-l-15 > label > input').click() // clicking yes for pain medication
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > div > input').type('Test') // inputting medication profile
                    //How often pain meds needed?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking how often pain meds needed
                    // Pain medication effectiveness
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label > input').click() //clicking Pain medication effectiveness
                    //Physician aware of pain?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(8) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label > input').click() //clicking yes for Physician aware of pain

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > input').type('Test') //inputting obsevation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr > td > table:nth-child(3) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td.p-5.b-l-n > input').type('Test') //inputting intervention

                    // (M1200) Vision
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(5) > table > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
                    
                    //Sensory Status
                    //Eyes
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > label > input').click() // clicking corrective glasses
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking left for cataract
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click() //clicking right for cataract
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking left for glaucoma
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click() //clicking right for glaucoma
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking left for redness
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > label > input').click() //clicking right for redness
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking left for pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(3) > label > input').click() //clicking right for pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking left for itching
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(3) > label > input').click() //clicking right for itching
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click() //clicking left for ptsosis
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > label > input').click() //clicking Right for ptosis
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //clicking left for Sclera reddened
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(6) > label > input').click() //clicking Right for Sclera reddened
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //clicking left for Edema of eyelids
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(6) > label > input').click() //clicking Right for Edema of eyelids
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //clicking left for Blurred vision
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(6) > label > input').click() //clicking Right for Blurred vision
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(5) > label > input').click() //clicking left for Blind	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(6) > label > input').click() //clicking Right for Blind	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(8) > label > input').click() //clicking left for Exudate from eyes	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(9) > label > input').click() //clicking Right for Exudate from eyes	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(8) > label > input').click() //clicking left for Excessive tearing
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(9) > label > input').click() //clicking Right for Excessive tearing
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(8) > label > input').click() //clicking left for Macular degeneration
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td:nth-child(9) > label > input').click() //clicking Right for Macular degeneration
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(8) > label > input').click() //clicking left for Retinopathy
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(5) > td:nth-child(9) > label > input').click() //clicking Right for Retinopathy
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(7) > input').type('Test') //inputting other status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(8) > label > input').click() //clicking left for other status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(9) > label > input').click() //clicking Right for other status
                    //Ears
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking left for HOH
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click() //clicking Right for HOH
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking left for Hearing aid
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click() //clicking Right for Hearing aid
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click() //clicking left for Deaf
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click() //clicking Right for Deaf
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > div > label > input').click() //clicking left for Tinnitus
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(6) > div > label > input').click() //clicking Right for Tinnitus
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > div > label > input').click() //clicking left for Drainage
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(6) > div > label > input').click() //clicking Right for Drainage
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > label > input').click() //clicking left for Pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(4) > td:nth-child(6) > div > label > input').click() //clicking Right for Pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(7) > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(7) > div > div > label > input').click()
                    //Mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > label > input').click() //clicking dentures
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label:nth-child(1) > input').click() //clicking upper
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label:nth-child(2) > input').click() //clicking lower
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > span > label.checkbox.checkbox-inline.m-r-5.ng-isolate-scope > input').click() //clicking partial
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking poor dentition for mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking gingivitis for mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click() //clicking toothache for mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking loss of taste for mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking lesions mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > input').type('Test') //inputting lesions
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking mass/tumor for mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div.cont-opt.m-lopt-105.ng-isolate-scope > input').type('Test') //inputting mass/tumor
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td.p-b-5.p-l-5 > div > label > input').click() // clicking Difficulty of chewing/swallowing for mouth
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td.p-b-5.p-l-5 > div > div.cont-opt.ng-isolate-scope > input').type('Test') //inputting others for mouth
                    //Nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking congestion for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking rhinitis for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click() //clicking sinus problem for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking loss of smell for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking epistaxis for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking noisy breathing for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.lopt-left > div > label > input').click() //clicking lesions for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > input').type('Test') //inputting lesions for nose
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > div > input').type('Test') //inputting others for nose
                    //Throat
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking sore throat for throat
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking dysphagia for throat
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking hoarseness for throat
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.lopt-left > div > label > input').click() //clicking lesions for throat
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > div.cont-opt.m-lopt-80.ng-isolate-scope > fieldset > input').type('Test') //inputting lesions for throat
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(11) > td > table > tbody > tr:nth-child(1) > td.p-b-5.p-l-5 > div > div.cont-opt > div > input').type('Test') //inputting other for throat
                    //Speech
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking slow speech for speech
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click() //clicking slured speech for speech
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking aphasia for speech
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click() //clicking mechanical assistance for speech
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(13) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > div.cont-opt > input').type('Test') //inputting other for speech
                    //Touch
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > div > div > label > input').click() //clicking paresthesia for touch
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > div > div > label > input').click() //clicking hyperesthesia for touch
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(1) > td.p-l-5.p-b-5 > div > div > div > label > input').click() //clicking peripheral neuropathy for touch
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(15) > td > table > tbody > tr:nth-child(2) > td.p-l-5 > div > div.cont-opt > input').type('Test') //inputting other for touch

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(17) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention

                    //SAVE BUTTON
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() // clicking save button
                    cy.wait(5000)
                // VITAL SIGNS/SENSORY TAB End -------------

        });
        
        it('Automating Entry Form of Integumentary / Endocrine Tab', function() {
                // INTEGUMENTARY/ENDOCRINE TAB Start -------------
                    const dayjs = require('dayjs')
                    cy.get('#oasis-tabs > label:nth-child(4)').click() //clicking integumentary/endocrine tab
                    cy.wait(5000)
                    //Integumentary Status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking skin color
                    cy.get('#SOOINT0060').type('Test') //inputting other in skin color
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking skin temp
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > div > label > input').click() //clicking moisture
                    cy.get('#SOOINT0074').type('Test') //inputting other in moisture
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking turgor
                    //Skin Integrity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > div > label > input').click() //clicking skin intact
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > div > label > input').click() //clicking lesion
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > div > a > i').click() //clicking add lesion
                    //Other Form for Lesion
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > div > div').click() //clicking dropdown (select one lesion)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(3) > div > div > div > div > ul > li:nth-child(2)').click() //selecting result dropdown (select one lesion)
                    cy.wait(2000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(5) > input').type('Test') //inputting location
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(7) > input').type('Test') //inputting comment

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(4) > div > label > input').click() //clicking wound
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(4) > div > a > i').click() //clicking add wound
                    //Other Form for Wound
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(3) > div > div.fg-line > input').type('Pressure Ulcer') //inputting result on wound #1
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(5) > div > div.fg-line > input').type('Buttock (R)') //inputting result (Location)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(7) > input').type('Test') //inputting comment on wound #1

                    cy.get('[name="SOOINT0079"]').type('Test') //inputting other in skin integrity

                    cy.get('#SOOINT0078').type('Test') //inputting observation
                    cy.get('#SOOINT0080').type('Test') //inputting intervention
                    cy.wait(5000)

                    //Braden Scale for Predicting Pressure Sore Risk
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking sensory perception
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking moisture
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking activity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking mobility
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td.p-0 > table > tbody > tr:nth-child(2) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking nutrition
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td.p-0 > table > tbody > tr:nth-child(3) > td.p-5.v-top.b-0.c-pointer.ng-isolate-scope > label > input').click() // clicking friction and shear
                    //(M1306)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1311) - Current Number of Unhealed Pressure Ulcers/Injuries at Each Stage
                    cy.get('#M1311_NBR_PRSULC_STG2_A1').type('2') //inputting A1
                    cy.get('#M1311_NBR_PRSULC_STG3_B1').type('2') //inputting B1
                    cy.get('#M1311_NBR_PRSULC_STG4_C1').type('2') //inputting C1
                    cy.get('#M1311_NSTG_DRSG_D1').type('2') //inputting D1
                    cy.get('#M1311_NSTG_CVRG_E1').type('2') //inputting E1
                    cy.get('#M1311_NSTG_DEEP_TSUE_F1').type('2') //inputting F1
                    //(M1322) - Current Number of Stage 1 Pressure Injuries
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1324) - Stage of Most Problematic Unhealed Pressure Ulcer/Injury that is Stageable
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1330) - Does this patient have a Stasis Ulcer?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
                    //(M1332) - Current Number of Stasis Ulcer(s) that are Observable
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(6) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1334) - Status of Most Problematic Stasis Ulcer that is Observable
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(7) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1340) - Does this patient have a Surgical Wound?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(8) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //M1342) - Status of Most Problematic Surgical Wound that is Observable
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(9) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
                    //Endocrine System (Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.p-b-5.ng-isolate-scope > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#SOOENDO0092').type('Test') //inputting other in endocrine system
                    //Diabetes
                    cy.get('#SOOENDO0003').type('Test') //inputting Hgb A1C
                    cy.get('#SOOENDO0004').type('04/23/2022') //inputting date tested
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking new for onset
                    cy.get('#SOOENDO0008').type('04/02/2022') //inputting onset date
                    //Diabetes management (Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-b-5 > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-l-15.p-b-5 > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-l-15.-b-5 > div > label > input').click()

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label:nth-child(2) > input').click() //clicking yes for Signs of hypoglycemia?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td:nth-child(1) > div > label:nth-child(2) > input').click() //clicking yes for Signs of hyperglycemia?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting for intervention
                    // Foot Assessment
                    cy.get('[name="SOOENDO0030"]').click() //clicking left for Thick or ingrown toenail
                    cy.get('[name="SOOENDO0031"]').click() //clicking right for Thick or ingrown toenail
                    cy.get('[name="SOOENDO0032"]').click() //clicking left for Calluses or fissures
                    cy.get('[name="SOOENDO0033"]').click() //clicking right for Calluses or fissures
                    cy.get('[name="SOOENDO0034"]').click() //clicking left for Interdigital macerations
                    cy.get('[name="SOOENDO0035"]').click() //clicking right for Interdigital macerations
                    cy.get('[name="SOOENDO0036"]').click() //clicking left for Signs of fungal infection
                    cy.get('[name="SOOENDO0037"]').click() //clicking right for Signs of fungal infection
                    cy.get('[name="SOOENDO0040"]').click() //clicking left for Absent pedal pulses
                    cy.get('[name="SOOENDO0041"]').click() //clicking right for Absent pedal pulses
                    cy.get('[name="SOOENDO0044"]').click() //clicking left for Hot, red, swollen foot
                    cy.get('[name="SOOENDO0045"]').click() //clicking right for Hot, red, swollen foot
                    cy.get('[name="SOOENDO0028"]').click() //clicking left for Foot deformity (hammer/claw toes)
                    cy.get('[name="SOOENDO0029"]').click() //clicking right for Foot deformity (hammer/claw toes)
                    cy.get('[name="SOOENDO0038"]').click() //clicking left for Limited range of motion of joints
                    cy.get('[name="SOOENDO0039"]').click() //clicking right for Limited range of motion of joints
                    cy.get('[name="SOOENDO0042"]').click() //clicking left for Decreased circulation (cold foot)
                    cy.get('[name="SOOENDO0043"]').click() //clicking right for Decreased circulation (cold foot)	
                    cy.get('[name="SOOENDO0046"]').click() //clicking left for Burning/tingling sensation, numbness	
                    cy.get('[name="SOOENDO0047"]').click() //clicking right for Burning/tingling sensation, numbness
                    cy.get('[name="SOOENDO0048"]').click() //clicking left for Loss of sensation to heat or cold
                    cy.get('[name="SOOENDO0049"]').click() //clicking right for Loss of sensation to heat or cold
                    cy.get('[name="SOOENDO0103"]').type('Test') //inputting other in foot assessment
                    cy.get('[name="SOOENDO0103L"]').click() //clicking left for other foot assessment
                    cy.get('[name="SOOENDO0103R"]').click() //clicking right for other foot assessment

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n.b-r-n > div > label:nth-child(1) > input').click() //clicking foot exam frequency
                    cy.get('#SOOENDO0019').type('Test') //inputting other for foot exam frequency
                    //Regularly done by (Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label.checkbox.checkbox-inline.m-l-15.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
                    
                    //Patient/Caregiver Competence
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent with glucometer use including control testing?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent with glucometer use including control testing?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent with insulin preparation and administration?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent with insulin preparation and administration?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(4) > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking yes for patient on Competent after instructions given and performed return demo?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking yes for caregiver on Competent after instructions given and performed return demo?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td:nth-child(4) > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking yes for patient on Level of knowledge of disease process and management
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n > label:nth-child(4) > input').click() //clicking yes for caregiver on Level of knowledge of disease process and management
                    //Blood glucose testing
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking testing frequency
                    cy.get('[name="SOOENDO0016"]').type('Test') //inputting other in testing frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div.cont-opt > div > div.fg-line > input').click() //clicking input box
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div.cont-opt > div > div.opt-list.ng-isolate-scope > ul > li:nth-child(1)').click() //clicking CGM Brand
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > div > label > input').click() //clicking control testing done
                    //Other Form of Glucometer/CGM (Control Test Result)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > input').type('1') //inputting level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting mg/dl
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(1) > tbody > tr > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking yes for within range?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(1) > input').type('1') //inputting level
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(2) > input').type('120') //inputting mg/dl
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td.p-0 > table:nth-child(2) > tbody > tr > td > table > tbody > tr > td:nth-child(3) > div > label:nth-child(1) > input').click() //clicking yes for within range?
                    cy.get('#SOOENDO0102_chosen > ul').click() //clicking inputbox/dropdown
                    cy.get('#SOOENDO0102_chosen > div > ul > li:nth-child(1)').click() //selecting result
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2) > textarea').type('Test') //inputting action taken

                    //Save Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                    cy.get('body > div.swal2-container > div.swal2-modal.show-swal2 > button.swal2-cancel.styled').click() //clicking access later in modal
                    cy.wait(5000)

                // INTEGUMENTARY/ENDOCRINE TAB End -------------
        });
        
        it('Automating Entry Form of Cardiopulmonary Tab', function() {
                // CARDIOPULMONARY TAB Start -------------
                    const dayjs = require('dayjs')
                    cy.get('#oasis-tabs > label:nth-child(5)').click() //clicking cardiopulmonary tab
                    cy.wait(5000)
                    //(M1400)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //Respiratory Status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td > div.cont-opt.m-lopt-185.p-r-5 > span > label > input').click() //clicking No Breath sounds clear, bilateral
                    cy.get('[name="SOOCARDIO0171"]').click() //clicking left for Diminished
                    cy.get('[name="SOOCARDIO0172"]').click() //clicking Right for Diminished
                    cy.get('[name="SOOCARDIO0204"]').click() //clicking Anterior for Diminished
                    cy.get('[name="SOOCARDIO0205"]').click() //clicking Posterior for Diminished
                    cy.get('[name="SOOCARDIO0206"]').click() //clicking Upper for Diminished
                    cy.get('[name="SOOCARDIO0207"]').click() //clicking Middle for Diminished
                    cy.get('[name="SOOCARDIO0208"]').click() //clicking Lower for Diminished
                    cy.get('[name="SOOCARDIO0175"]').click() //clicking left for Absent
                    cy.get('[name="SOOCARDIO0176"]').click() //clicking Right for Absent
                    cy.get('[name="SOOCARDIO0209"]').click() //clicking Anterior for Absent
                    cy.get('[name="SOOCARDIO0210"]').click() //clicking Posterior for Absent
                    cy.get('[name="SOOCARDIO0211"]').click() //clicking Upper for Absent
                    cy.get('[name="SOOCARDIO0212"]').click() //clicking Middle for Absent
                    cy.get('[name="SOOCARDIO0213"]').click() //clicking Lower for Absent
                    cy.get('[name="SOOCARDIO0179"]').click() //clicking left for Rales (crackles)
                    cy.get('[name="SOOCARDIO0180"]').click() //clicking Right for Rales (crackles)
                    cy.get('[name="SOOCARDIO0214"]').click() //clicking Anterior for Rales (crackles)
                    cy.get('[name="SOOCARDIO0215"]').click() //clicking Posterior for Rales (crackles)
                    cy.get('[name="SOOCARDIO0216"]').click() //clicking Upper for Rales (crackles)
                    cy.get('[name="SOOCARDIO0217"]').click() //clicking Middle for Rales (crackles)
                    cy.get('[name="SOOCARDIO0218"]').click() //clicking Lower for Rales (crackles)
                    cy.get('[name="SOOCARDIO0268"]').click() //clicking left for Rhonchi
                    cy.get('[name="SOOCARDIO0269"]').click() //clicking Right for Rhonchi
                    cy.get('[name="SOOCARDIO0270"]').click() //clicking Anterior for Rhonchi
                    cy.get('[name="SOOCARDIO0271"]').click() //clicking Posterior for Rhonchi
                    cy.get('[name="SOOCARDIO0272"]').click() //clicking Upper for Rhonchi
                    cy.get('[name="SOOCARDIO0273"]').click() //clicking Middle for Rhonchi
                    cy.get('[name="SOOCARDIO0274"]').click() //clicking Lower for Rhonchi
                    cy.get('[name="SOOCARDIO0183"]').click() //clicking left for Wheeze
                    cy.get('[name="SOOCARDIO0184"]').click() //clicking Right for Wheeze
                    cy.get('[name="SOOCARDIO0219"]').click() //clicking Anterior for Wheeze
                    cy.get('[name="SOOCARDIO0220"]').click() //clicking Posterior for Wheeze
                    cy.get('[name="SOOCARDIO0221"]').click() //clicking Upper for Wheeze
                    cy.get('[name="SOOCARDIO0222"]').click() //clicking Middle for Wheeze
                    cy.get('[name="SOOCARDIO0223"]').click() //clicking Lower for Wheeze
                    cy.get('[name="SOOCARDIO0187"]').click() //clicking left for Stridor
                    cy.get('[name="SOOCARDIO0188"]').click() //clicking Right for Stridor
                    cy.get('[name="SOOCARDIO0224"]').click() //clicking Anterior for Stridor
                    cy.get('[name="SOOCARDIO0225"]').click() //clicking Posterior for Stridor
                    cy.get('[name="SOOCARDIO0226"]').click() //clicking Upper for Stridor
                    cy.get('[name="SOOCARDIO0227"]').click() //clicking Middle for Stridor
                    cy.get('[name="SOOCARDIO0228"]').click() //clicking Lower for Stridor

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td > div.cont-opt.m-lopt-185.p-r-5 > span > label > input').click() //clicking yes for Abnormal breathing patterns
                    //Abnormal breathing patterns (Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div:nth-child(2) > div > input').type('Test')

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td > div.cont-opt.p-r-5 > span > span > label > input').click() //clicking yes for cough
                    //Cough
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking character of cough
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking sputum color of cough
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click() //clicking sputum character of cough
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click() //clicking sputum amount of cough
                    //Special Procedure (Select All)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr.subheadv3 > td > table > tbody > tr > td.p-l-5 > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > input').type('Test') //inputting other for special procedure
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('Test') //inputting intervention
                    // Other form of Oxygen therapy
                    //Oxygen Risk Assessment
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td:nth-child(2) > label > input').click() //click yes for Is the patient using oxygen equipment?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //click yes for Does anyone in the home smoke?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //click yes for Are oxygen signs posted in the appropriate places?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //click yes for Are oxygen tanks/concentrators stored safely?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(6) > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input').click() //click yes for Is backup O2 tank available?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr.ng-isolate-scope > td.text-center.b-l-n.b-r-n.ng-isolate-scope > label > input').click() //click yes for Does patient/PCG know how to use backup O2?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //click yes for Are all cords near or related to oxygen intact, secure & properly used?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //click yes for Is patient educated re: substances that can cause O2 to be flammable?
                    //Are there potential sources of open flames identified? (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(5) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(4) > label:nth-child(6) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //click yes for Are there potential sources of open flames identified?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(8) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
                    //Oxygen Theraphy
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking type
                    //Oxygen delivery (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(3) > td.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td.p-b-5.b-l-n > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting other oxygen delivery
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(4) > td.p-b-5.b-l-n > table > tbody > tr > td > div > input').type('10') //inputting liters/minute
                    //Oxygen source (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(5) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(3) > div > input').type('Test') //inputting other for oxygen source
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(1) > label:nth-child(1) > input').click() //clicking yes for backup O2 tank
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(6) > td.p-b-5.b-l-n > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking vendor notified
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking for intervention
                    //Other form for Tracheostomy
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-t-n.b-b-n.ng-isolate-scope > span > input').type('Test') //inputting brand/model
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.ng-isolate-scope > div > input').type('4')
                    cy.get('#SOOCARDIO0089').type('12042021') //inputting date last change (mm/dd/yyyy)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.ng-isolate-scope > span > span > input').type('Test') //inputting inner cannula
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention
                    //Other form for BiPAP/CPAP
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.ng-isolate-scope > div > input').type('Test') //inputting device brand/model
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Device working properly?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.ng-isolate-scope > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Compliant with use of device?\
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting intervention
                    //Other form for Suctioning
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > label:nth-child(1) > input').click() //clicking oral
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(3) > td > label:nth-child(1) > input').click() //clicking yes for Is the person performing the suctioning proficient? 
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(4) > td > label:nth-child(2) > input').click() //clicking yes for Is the suction equipment setup always ready for use?
                    //Suction procedure done by (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(5) > td > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
                    //Other form for Ventilator
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td.b-b-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting Brand/Model
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td.b-b-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting Tidal volume
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(2) > div > input').type('Test') //inputting FiO2
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(2) > div > input').type('Test') //inputting Assist control
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td.b-t-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting PEEP
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > div > input').type('Test') //inputting SIMV	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(4) > div > input').type('Test') //inputting Pressure control
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(4) > div > input').type('Test') //inputting PRVC
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention
                    //Other form for PleurX
                    cy.get('#SOOCARDIO0292').type('12042021') //inputting date catheter inserted (mm/dd/yyyy)
                    cy.get('#SOOCARDIO0293').click() //clicking daily for drainage frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting other for drainage frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > input').type('2') //inputting ml for amount drained
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > div > label > input').click() //clicking done
                    //Procedure done by (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(5) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(7) > td.ng-isolate-scope > input').type('Test') //inputting intervention

                    //Cardiovascular
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td:nth-child(2) > div:nth-child(1) > label > input').click() //clicking regular for Heart Rhythm
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr > td.p-r-5 > label > input').click() //clicking < 3 sec for Capillary refill
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for JVD	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for Peripheral edema
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td.ng-isolate-scope > label > input').click() //clicking yes for Chest Pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking yes for Cardiac device
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td.p-0 > div > div > div').click() //clicking dropdown for cardiac device result
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td:nth-child(2) > table > tbody > tr > td.p-0 > div > div > div > div > ul > li:nth-child(1)').click() //clicking result on cardiac device
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > label.radio.radio-inline.m-l-20.ng-isolate-scope > input').click() //clicking yes for weight gain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('Test') //inputting other for weight gain
                    //Pulses
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(2) > label > input').click() //clicking pedal-left for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(3) > label > input').click() //clicking pedal-right for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(4) > label > input').click() //clicking Popliteal-left for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(5) > label > input').click() //clicking Popliteal-right for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(6) > label > input').click() //clicking Femoral-left for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(7) > label > input').click() //clicking Femoral-right for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(8) > label > input').click() //clicking Brachial-left for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(9) > label > input').click() //clicking Brachial-right for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(10) > label > input').click() //clicking Radial-left for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(11) > td:nth-child(11) > label > input').click() //clicking Radial-right for Bounding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(15) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(16) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
                    
                    //Peripheral edema
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking +1 for Pedal edema - Left
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking +1 for Pedal edema - Right	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking +1 for Ankle edema - Left	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking +1 for Ankle edema - Right	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking +1 for Leg edema - Left
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking +1 for Leg edema - Right
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(9) > td:nth-child(2) > label > input').click() //clicking +1 for Sacral edema	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(10) > td:nth-child(2) > label > input').click() //clicking +1 for Generalized edema
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(11) > td.ng-isolate-scope > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(12) > td.ng-isolate-scope > input').type('Test') //inputting intervention
                    //Chest Pain
                    //Character (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-r-5.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to shoulder
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to jaw
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-r-5.p-b-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to neck
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for radiating to arm
                    //Accompanied by (Select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td.p-r-5.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(5) > td.ng-isolate-scope > input').type('Test') //inputting frequency of pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(6) > td.ng-isolate-scope > input').type('Test') //inputting duration of pain
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td.p-b-5.ng-isolate-scope > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(8) > td.p-b-5.ng-isolate-scope > input').type('Test') //inputting intervention
                    //Pace Maker
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('Test') //inputting brand/model
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('20') //inputting Rate setting
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > input').type('12/24/2021') //inputting Date implanted
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('12/03/2021') //inputting Date last tested
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking no
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > input').type('Test') //inputting intervention
                    
                    //Save Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                // CARDIOPULMONARY TAB End -------------
                
        });
       
        it('Automating Entry Form of Nutrition / Elimination Tab', function() {
                // NUTRITION/ELIMINATION TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(6)').click() //clicking nutrition/elimination tab
                    cy.wait(5000)
                    //Upper GI Status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-b-n > label > input').click() //click soft for abdomen
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > input').type('2') //inputting inches in girt
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-t-n.b-b-n > label > input').click() //clicking active for bowel sounds
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.b-t-n.b-b-n > label > input').click() //clicking good for appetite
                    cy.wait(5000)
                    //Other sypmtoms (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n.b-b-n.b-t-n.b-r-n > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.b-r-n > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(1) > td:nth-child(4) > input').type('Test') //inputting other symptom
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td.b-l-n.b-t-n.observation_intervention > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-l-n.b-t-n.observation_intervention > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Other form for Vomiting
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking projectile for type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(3) > td.b-l-n.b-t-n > label:nth-child(1) > input').click() //clicking small for amount
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.b-t-n.b-l-n > label:nth-child(1) > input').click() //clicking watery for character\
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(4) > td.b-t-n.b-l-n > span > input').type('Test') //inputting other character
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n > div > input').type('Test') //inputting frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(6) > td.b-t-n.b-l-n.observation_intervention > div > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n.observation_intervention > div > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Nutrition/Diet (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div.pull-left.m-tb-0.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div.m-l-120 > div > div > input').type('6') //inputting ml for fluid restriction
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(4) > div.cont-opt.m-lopt-45 > input').type('Test') //inputting other nutrition/diet
                    cy.wait(5000)
                    //Nutritional Health Screen
                    //Clicking check/Yes
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking decubitus
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking ulcer
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(1) > label:nth-child(3) > input').click() //clicking burn
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(1) > label:nth-child(4) > input').click() //clicking wound
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(7) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(6) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(6) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(6) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(6) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(6) > label > input').click()
                    cy.wait(5000)
                    
                    //Enteral Nutrition
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking dobhoff for feeding via
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('12/06/2021') //inputting tube insertion date (mm/dd/yyyy)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking pump for formula delivery system
                    
                    cy.get('[name="SOONUTRI0043"]').type('4') //inputting amout for feeding formula
                    cy.get('[name="SOONUTRI0044"]').type('4') //inputting ml for feeding formula
                    cy.get('[name="SOONUTRI0045"]').type('4') //inputting amout for liquid supplemet
                    cy.get('[name="SOONUTRI0046"]').type('4') //inputting ml for liquid supplemet
                    cy.get('[name="SOONUTRI0053"]').type('4') //inputting ml for Pump rate per hour
                    cy.get('[name="SOONUTRI0054"]').type('24') //inputting hours per day for Pump rate per hour
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking open system for enteral feeding system
                    cy.get('[name="SOONUTRI0077"]').type('4') //inputting hours if residual volume over for hold feeling for
                    cy.get('[name="SOONUTRI0078"]').type('4') //inputting ml for hold feeling for
                    cy.get('[name="SOONUTRI0049"]').type('4') //inputting ml for Gastric residual amount
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(9) > td:nth-child(2) > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for Tolerating feedings well?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > label.radio.radio-inline.m-r-10.ng-isolate-scope > input').click() //clicking yes for NPO?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > input').type('Test') //inputting NPO
                    cy.wait(5000)
                    //Ostomy care/feedings by: (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(12) > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(13) > td:nth-child(2) > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Genitourinary Status
                    //Urine clarity (select all except clear)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td.p-b-5.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td.b-r-n.b-l-n.p-b-5 > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.p-b-5 > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td.b-l-n.p-b-5.ng-isolate-scope > input').type('Test') //inputting other for urine clarity
                    //Urine color
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking straw for urine color
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(3) > input').type('Test') //inputting other urine color
                    //Urine Odor
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td.b-l-n > table > tbody > tr > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking yes for urine odor
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td.b-l-n > table > tbody > tr > td:nth-child(3) > input').type('Test') //inputting description
                    cy.wait(5000)
                    //Abnormal elimination
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td.b-l-n.subheadv3.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes Abnormal elimination
                    //Abnormal elimination (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div.m-l-50 > div > input').type('Test') //inputting other for abnormal elimination
                    cy.wait(5000)
                    //Special procedures
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td.b-l-n.subheadv3.ng-isolate-scope > label:nth-child(2) > input').click() //clicking yes for Special procedures
                    //Special procedures (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div:nth-child(2) > div > input').type('Test') //inputting other for special procedure
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td > table > tbody > tr:nth-child(2) > td.p-5.b-l-n > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Indwelling catheter
                    cy.get('#SOOELIMINATION0017').click() //clicking urethral for catheter type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr.catheter__row > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 14 for catheter size
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr.balloon_inflation__row > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 5 for balloon inflation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 2-way for catheter lumens
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('3') //inputting days for catheter change
                    cy.get('#sooelimination0156').type('05232021') //date last changed for catheter change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(6) > td:nth-child(2) > div.display-ib.checkbox.m-0.m-t-5.m-l-15 > label > input').click() //clicking done for catherter change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(7) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking bedside bag for drainage bag
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(7) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking leg bag for drainage bag
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('3') //inputting days for MD-ordered irrigation frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > label:nth-child(4) > input').click() //clicking as needed for MD-ordered irrigation frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking done for MD-ordered irrigation frequency\
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(8) > td:nth-child(2) > div > label.p-l-18.m-l-5.ng-isolate-scope > input').click() //clicking none for MD-ordered irrigation frequency
                    cy.get('[name="SOOELIMINATION0055"]').type('24') //inputting amount for MD-ordered irrigation solution
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(9) > td:nth-child(2) > input:nth-child(2)').type('4') //inputting ml for MD-ordered irrigation solution
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(9) > td:nth-child(2) > div > label > input').click() //clicking none for MD-ordered irrigation solution
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Intermittent Catheterization
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking 16 for catheter size
                    cy.get('[name="SOOELIMINATION0153"]').type('3') //inputting for frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking day for frequency
                    cy.get('[name="SOOELIMINATION0183"]').type('Test') //inputting other for frequency
                    //Done By (select all)
                    cy.get('[name="SOOELIMINATION0207"]').click()
                    cy.get('[name="SOOELIMINATION0208"]').click()
                    cy.get('[name="SOOELIMINATION0209"]').click()
                    cy.get('[name="SOOELIMINATION0187"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0210"]').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Nephrostomy
                    cy.get('[name="SOOELIMINATION0174"]').type('3') //inputting days for Nephrostomy dressing change
                    cy.get('[rname="SOOELIMINATION0197"]').click() //clicking as needed for Nephrostomy dressing change
                    cy.get('[rname="SOOELIMINATION0214"]').click() //clicking done for Nephrostomy dressing change
                    cy.get('[name="SOOELIMINATION0177"]').type('3') //inputting days for Nephrostomy bag change
                    cy.get('[rname="SOOELIMINATION0198"]').click() //clicking as needed for Nephrostomy bag change
                    cy.get('[rname="SOOELIMINATION0215"]').click() //clicking done for Nephrostomy bag change
                    cy.get('[name="SOOELIMINATION0175"]').type('3') //inputting days for MD-ordered irrigation frequency
                    cy.get('[rname="SOOELIMINATION0176"]').click() //clicking as needed for MD-ordered irrigation frequency
                    cy.get('[rname="SOOELIMINATION0216"]').click() //clicking done for MD-ordered irrigation frequency
                    cy.get('[rname="SOOELIMINATION0223"]').click() //clicking none for  MD-ordered irrigation frequency
                    cy.get('[name="SOOELIMINATION0178"]').type('3') //inputting amount for
                    cy.get('[name="SOOELIMINATION0179"]').type('3') //inputting ml for
                    cy.get('[rname="SOOELIMINATION0224"]').click() //clicking none for
                    cy.get('[name="SOOELIMINATION0185"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0200"]').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Urostomy
                    cy.get('[name="SOOELIMINATION0181"]').type('3') //inputting days for Urostomy pouch change frequency:
                    cy.get('[name="SOOELIMINATION0186"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0201"]').type('Test') //inputting intervention
                    cy.wait(5000)
                    //External Urinary Catheter
                    cy.get('[rname="SOOELIMINATION0150"]').click() //clicking daily for Change frequency
                    cy.get('[name="SOOELIMINATION0151"]').type('Test') //inputting other in Change frequency
                    //Drainage bag (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(3) > td:nth-child(2) > label.checkbox.checkbox-inline.m-r-10.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(2) > input').click()
                    cy.get('[name="SOOELIMINATION0188"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0202"]').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Hemodialysis
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking av shunt for AV access
                    cy.get('[name="SOOELIMINATION0032"]').type('Test') //inputting location for AV access
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(2) > label.checkbox.checkbox-inline.m-l-20.ng-isolate-scope > input').click() //clicking Permacath for AV access
                    cy.get('[name="SOOELIMINATION0180"]').type('Test') //inputting other in AV access
                    cy.get('#SOOELIMINATION0034').click() //clicking yes for Bruit present?
                    cy.get('#SOOELIMINATION0033').click() //clicking yes for Thrill strong?
                    cy.wait(5000)
                    //Dialysis schedule (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(5) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(6) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(7) > input').click()
                    cy.get('[name="SOOELIMINATION0036"]').type('Test') //inputting dialysis center
                    cy.get('[name="SOOELIMINATION0037"]').type('09123456789') //inputting phone
                    cy.get('[name="SOOELIMINATION0190"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0203"]').type('Test') //inputting intervention
                    //Peritoneal Dialysis
                    cy.get('#SOOELIMINATION0096').click() //clicking type
                    cy.get('[name="SOOELIMINATION0094"]').type('Test') //inputting APD machine
                    //Dialysate (select all)
                    cy.get('[rname="SOOELIMINATION0097"]').click()
                    cy.get('[rname="SOOELIMINATION0098"]').click()
                    cy.get('[rname="SOOELIMINATION0099"]').click()
                    cy.get('[rname="SOOELIMINATION0100"]').click()
                    cy.get('[name="SOOELIMINATION0066"]').type('12:00') //inputting dwell time
                    cy.get('[name="SOOELIMINATION0093"]').type('4') //inputting hours
                    //Peritoneal dialysis done by (select all)
                    cy.get('[name="SOOELIMINATION0158"]').click()
                    cy.get('[name="SOOELIMINATION0159"]').click()
                    cy.get('[name="SOOELIMINATION0160"]').click()
                    cy.get('[name="SOOELIMINATION0193"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0204"]').type('Test') //inputting intervention
                    //(M1600) - Has this patient been treated for a Urinary Tract Infection in the past 14 days?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(18) > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td:nth-child(1) > div:nth-child(2) > label > input').click()
                    //(M1610) - Urinary Incontinence or Urinary Catheter Presence
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(18) > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //Lower GI Status
                    cy.get('#SOOELIMINATION0171').type('12/04/2021') //inputting date last BM (mm/dd/yyyy)
                    cy.wait(5000)
                    //Bowel movement (select all)
                    cy.get('[name="SOOELIMINATION0161"]').click()
                    cy.get('[name="SOOELIMINATION0083"]').click()
                    cy.get('[name="SOOELIMINATION0163"]').click()
                    cy.get('[name="SOOELIMINATION0164"]').click()
                    cy.get('[name="SOOELIMINATION0043"]').type('5')
                    cy.get('[name="SOOELIMINATION0157"]').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking soft for stool character
                    cy.get('[name="SOOELIMINATION0104_OTHER"]').type('Test') //inputting other for stool character
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(4) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click() //clicking yellow/brown for stool color
                    cy.get('[name="SOOELIMINATION0088"]').type('Test') //inputting other for stool character
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking effective for
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking MD notified for
                    cy.get('[name="SOOELIMINATION0222"]').type('Test') //inputting MD notified
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td.b-r-n.b-l-n.ng-isolate-scope > label > input').click() //clicking as needed for Laxative/Enema
                    cy.get('[name="SOOELIMINATION0046_OTHER"]').type('Test') //inputting other for Laxative/Enema
                    cy.get('[name="SOOELIMINATION0196"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0205"]').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Lower GI Ostomy
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(20) > tbody > tr:nth-child(2) > td:nth-child(2) > label.radio.radio-inline.m-r-40.ng-isolate-scope > input').click() //clicking colostomy for ostomy type
                    cy.get('[name="SOOELIMINATION0068"]').type('12') //inputting cm for stoma diameter
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(20) > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking healed for ostomy wound
                    //Care done by (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(20) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(20) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(20) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(3) > input').click()
                    cy.get('[name="SOOELIMINATION0227"]').type('Test') //inputting observation
                    cy.get('[name="SOOELIMINATION0211"]').type('Test') //inputting intervention
                    //(M1620) - Bowel Incontinence Frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(21) > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click()
                    //(M1630) - Ostomy for Bowel Elimination
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset:nth-child(21) > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(1) > label > input').click()
                    //SAVE BUTTON
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                // NUTRITION/ELIMINATION TAB Start -------------

        });
        
        it('Automating Entry Form of Neurologic / Behavioral Tab', function() {

                // NEUROLOGIC/BEHAVIORAL TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(7)').click()//clicking NEUROLOGIC/BEHAVIORAL TAB
                    cy.wait(5000)
                    //Neurological Status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td.b-b-n.b-t-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left>right for size unequal
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(4) > td.b-t-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for non-reactive
                    //Mental status (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > div > span > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td.p-b-5.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(5) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td.p-b-5.ng-isolate-scope > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > label > input').click() //clicking adequate for sleep/rest
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(6) > td:nth-child(2) > table > tbody > tr > td > span:nth-child(4) > label > input').click() //clicking MD notified for sleep/rest
                    //Hand grips
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td.b-b-n.b-t-n > label:nth-child(2) > input').click() //clicking left for strong
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(7) > td.b-b-n.b-t-n > label:nth-child(3) > input').click() //clicking right for strong
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(8) > td > label:nth-child(2) > input').click() //clicking left for weak
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(8) > td > label:nth-child(3) > input').click() //clicking right for weak
                    cy.wait(5000)
                    //Other signs (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(9) > td:nth-child(2) > label:nth-child(5) > input').type('Test') //inputting other test
                    cy.wait(5000)
                    //Weakness
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(2) > input').click() //clicking left for Upper extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(3) > input').click() //clicking right for Upper extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label:nth-child(2) > input').click() //clicking left for Lower extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label:nth-child(3) > input').click() //clicking right for Lower extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(10) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td > label:nth-child(2) > input').click() //clicking left for Hemiparesis 
                    //Paralysis
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking left for hemiplegia
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(11) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label > input').click() //clicking left for Paraplegia
                    //Tremors
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking left for Upper extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking right for Upper extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking fine for Upper extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(1) > input').click() //clicking left for Lower extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(1) > label:nth-child(2) > input').click() //clicking right for Lower extremity
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(12) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label:nth-child(1) > input').click() //clicking fine for Lower extremity
                    //Seizure
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking grand mal
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label.ng-isolate-scope > input').type('12/06/2021') //inputting date of last seizure
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(13) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > label.m-l-10 > input').type('50') //inputting duration (in seconds)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(14) > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(15) > td:nth-child(2) > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //(M1700) - Cognitive Functioning
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1710) - When Confused (Reported or Observed Within the Last 14 Days)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1720) - When Anxious (Reported or Observed Within the Last 14 Days)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
                    //(M1710) - (M1730)	Depression Screening
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr:nth-child(1) > td > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click() //clicking for a) Little interest or pleasure in doing things
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click() //clicking for b) Feeling down, depressed, or hopeless?
                    //(M1740) - Cognitive, behavioral, and psychiatric symptoms
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr:nth-child(2) > td > div > label > input').click()
                    //(M1745) - Frequency of Disruptive Behavior Symptoms (Reported or Observed)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(6) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(3) > label > input').click()
                    cy.wait(5000)
                    // Knowledge Deficit (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other in Knowledge Deficit
                    cy.wait(5000)
                    //Thought Process, Affect and Behavioral Status (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.p-0 > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > input').type('Test') //inputting other in Thought Process, Affect and Behavioral Status
                    cy.wait(5000)
                    //Psychosocial Factors
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n.subheadv3 > table > tbody > tr > td:nth-child(1) > span > label > input').click() //clicking yes for language barrier
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n.subheadv3 > table > tbody > tr > td:nth-child(2) > label:nth-child(1) > input').click() //clicking patient for language barrier
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n.subheadv3 > table > tbody > tr > td:nth-child(2) > label:nth-child(2) > input').click() //clicking caregiver for language barrier
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n.subheadv3 > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting speak
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(1) > td > input').type('Test') //inputting Interpreter needed 
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(2) > td > label > input').type('Test') //inputting Sign language (Type)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(3) > td > label.m-r-10 > input').type('Test') //inputting Interpreter service provider
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(3) > td > label:nth-child(2) > input').type('1234567890') //inputting Interpreter contact number
                    cy.wait(5000)
                    //Learning barrier
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(4) > td.b-l-n.subheadv3 > table > tbody > tr > td > span > label > input').click() //clicking yes for Learning barrier
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking unable to read
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(2) > td.p-r-5 > label > input').click() //clicking functional
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(2) > td.p-r-15 > span > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(3) > td.p-r-5 > label > input').click() //clicking physical
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(3) > td:nth-child(2) > span > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(4) > td.p-r-5 > label > input').click() //clicking Mental/Cognitive
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(4) > td.p-r-15 > span > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(5) > td.p-r-5 > label > input').click() //clicking Psychosocial
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(5) > td.b-t-n.b-l-n.ng-isolate-scope > table > tbody > tr:nth-child(5) > td:nth-child(2) > span > input').type('Test') //inputting description
                    cy.wait(5000)
                    // Signs of abuse, neglect
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(6) > td.b-l-n.subheadv3 > table > tbody > tr > td > span > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking potential
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(2) > td > label.checkbox.checkbox-inline.ng-isolate-scope > input').click() //clicking physical
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(2) > td > label:nth-child(2) > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(3) > td > label.checkbox.checkbox-inline.ng-isolate-scope > input').click() //clicking verbal
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(3) > td > label:nth-child(2) > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(4) > td > label:nth-child(1) > input').click() //clicking referral to MSW
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(4) > td > span > input').type('Test') //inputting description
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(7) > td.b-t-n.b-l-n > table > tbody > tr:nth-child(4) > td > label:nth-child(3) > input').click() //clicking Referred to Child/Adult Protective Services
                    cy.wait(5000)
                    //Spiritual/cultural issues
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(8) > td:nth-child(2) > table > tbody > tr > td > span > label > input').click() //clicking yes Spiritual/cultural issues
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td.b-l-n > table > tbody > tr:nth-child(1) > td > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(1) > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(2) > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(9) > td.b-l-n > table > tbody > tr:nth-child(2) > td:nth-child(3) > input').type('1234567890')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(10) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(11) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
                    //Save Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)               
                // NEUROLOGIC/BEHAVIORAL TAB End -------------
        });

        it('Automating Entry Form of ADL / IADL / Musculoskeletal Tab', function() {
                
                // ADL/IADL/Musculoskeletal TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(8)').click() //clicking ADL/IADL/Musculoskeletal
                    cy.wait(5000)
                    //Musculoskeletal Status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.p-5.b-l-n > label > input').click() //clicking strong for muscle strength
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label.radio.radio-inline.ng-isolate-scope > input').click() //clicking limited for Range of motion\
                    //limited (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.b-l-n.ng-isolate-scope > span > label:nth-child(5) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking independent for Bed mobility
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td.b-l-n.ng-isolate-scope > label > input').click() //clicking independent for Transfer ability
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td.b-l-n > label > input').click() //clicking steady for Gait/Ambulation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.b-l-n > label > input').click() //clicking good for balance
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > span.m-l-5 > input').type('10') //inputting seconds for Timed Up & Go
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click() //clicnking Practiced once before actual test for Timed Up & Go
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td.b-l-n.ng-isolate-scope > label:nth-child(4) > input').click() //clicnking Unable to perform for Timed Up & Go
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td.b-l-n > label > input').click() //clicking low for risk for falls
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking left for amputation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(2) > input').click() //clicking right for amputation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(3) > input').click() //clicking BK
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(4) > input').click() //clicking AK
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > label:nth-child(5) > input').click() //clicking UE
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting other for amputation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > label:nth-child(1) > input').click() //clicking new for fracture
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > input').type('Test') //inputting location for fracture
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td.b-l-n.ng-isolate-scope > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click() //clicking cast?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(11) > td.p-l-5.p-r-5 > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(12) > td.p-l-5.p-r-5 > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //If cast is present, assessment of extremity distal to cast
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking pink for color
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking strong for pulses
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2) > label.radio.radio-inline.m-l-5.ng-isolate-scope > input').click() //clicking < 3 sec for Capillary refill	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking warm for temperature
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking normal for sensation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking able to move for motor function
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(4) > label:nth-child(1) > input').click() //clicking yes for swelling
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td:nth-child(4) > input').type('Test') //inputting for intervention

                    // Functional Limitations (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.valign-top.ng-isolate-scope > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for functional limitations
                    cy.wait(5000)
                    //Activities Permitted (select all)
                    
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting others for activities permitted
                    cy.wait(5000)
                    //MAHC - 10 - Fall Risk Assessment Tool (check all points)
                    
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > div > label > input').click()
                    cy.wait(5000)
                    //(M1800) - Grooming
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1810) - Current Ability to Dress Upper Body
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1820) - Current Ability to Dress Lower Body
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1830) - Bathing
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(4) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1840) - Toilet Transferring
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1845) - Toileting Hygiene
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(6) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1850) - Transferring
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(7) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1860) - Ambulation/Locomotion
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(8) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1870) - Feeding or Eating
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(9) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M1910) - Has this patient had a multi-factor Falls Risk Assessment using a standardized, validated assessment tool?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(10) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    cy.wait(5000)
                    //Section GG Functional Abilities and Goals
                    //(GG0100) - Prior Functioning
                    cy.get('#GG0100A_chosen > a').click() //clicking input box for A
                    cy.get('#GG0100A_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0100B_chosen > a').click() //clicking input box for B
                    cy.get('#GG0100B_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0100C_chosen > a').click() //clicking input box for C
                    cy.get('#GG0100C_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0100D_chosen > a').click() //clicking input box for D
                    cy.get('#GG0100D_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.wait(5000)
                    //(GG0110). Prior Device Use (check all)
                    cy.get('#GG0110A > td:nth-child(1) > label > input').click()
                    cy.get('#GG0110B > td:nth-child(1) > label > input').click()
                    cy.get('#GG0110C > td:nth-child(1) > label > input').click()
                    cy.get('#GG0110A > td:nth-child(3) > label > input').click()
                    cy.get('#GG0110B > td:nth-child(3) > label > input').click()
                    cy.get('#GG0110C > td:nth-child(3) > label > input').click()
                    cy.wait(5000)
                    //GG0130. Self‐Care
                    cy.get('#GG0130A1_chosen > a').click() //clicking input box of A for 1 SOC/ROC Performance
                    cy.get('#GG0130A1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130A2_chosen > a').click() //clicking input box of A for 2 Discharge Goal
                    cy.get('#GG0130A2_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0130B1_chosen > a').click() //clicking input box of B for 1 SOC/ROC Performance
                    cy.get('#GG0130B1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130B2_chosen > a').click() //clicking input box of B for 2 Discharge Goal
                    cy.get('#GG0130B2_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0130C1_chosen > a').click() //clicking input box of C for 1 SOC/ROC Performance
                    cy.get('#GG0130C1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130C2_chosen > a').click() //clicking input box of C for 2 Discharge Goal
                    cy.get('#GG0130C2_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0130E1_chosen > a').click() //clicking input box of E for 1 SOC/ROC Performance
                    cy.get('#GG0130E1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130E2_chosen > a').click() //clicking input box of E for 2 Discharge Goal
                    cy.get('#GG0130E2_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0130F1_chosen > a').click() //clicking input box of F for 1 SOC/ROC Performance
                    cy.get('#GG0130F1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130F2_chosen > a').click() //clicking input box of F for 2 Discharge Goal
                    cy.get('#GG0130F2_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0130G1_chosen > a').click() //clicking input box of G for 1 SOC/ROC Performance
                    cy.get('#GG0130G1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130G2_chosen > a').click() //clicking input box of G for 2 Discharge Goal
                    cy.get('#GG0130G2_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0130H1_chosen > a').click() //clicking input box of H for 1 SOC/ROC Performance
                    cy.get('#GG0130H1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0130H2_chosen > a').click() //clicking input box of H for 2 Discharge Goal
                    cy.get('#GG0130H2_chosen > div > ul > li.active-result.highlighted').click() //clicking result 
                    cy.wait(5000)
                    //GG0170. Mobility
                    cy.get('#GG0170A1_chosen > a').click() // clicking input box of A for 1 SOC/ROC Performance
                    cy.get('#GG0170A1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170A2_chosen > a').click() // clicking input box of A for 2 Discharge Goal
                    cy.get('#GG0170A2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170B1_chosen > a').click() // clicking input box of B for 1 SOC/ROC Performance
                    cy.get('#GG0170B1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170B2_chosen > a').click() // clicking input box of B for 2 Discharge Goal
                    cy.get('#GG0170B2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170C_MOBILITY_SOCROC_PERF_chosen > a').click() // clicking input box of C for 1 SOC/ROC Performance
                    cy.get('#GG0170C_MOBILITY_SOCROC_PERF_chosen > div > ul > li:nth-child(1)').click() //clicking result
                    cy.get('#GG0170C_MOBILITY_DSCHG_GOAL_chosen > a').click() // clicking input box of C for 2 Discharge Goal
                    cy.get('#GG0170C_MOBILITY_DSCHG_GOAL_chosen > div > ul > li:nth-child(1)').click() //clicking result
                    cy.get('#GG0170D1_chosen > a').click() // clicking input box of D for 1 SOC/ROC Performance
                    cy.get('#GG0170D1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170D2_chosen > a').click() // clicking input box of D for 2 Discharge Goal
                    cy.get('#GG0170D2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170E1_chosen > a').click() // clicking input box of E for 1 SOC/ROC Performance
                    cy.get('#GG0170E1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170E2_chosen > a').click() // clicking input box of E for 2 Discharge Goal
                    cy.get('#GG0170E2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170F1_chosen > a').click() // clicking input box of F for 1 SOC/ROC Performance
                    cy.get('#GG0170F1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170F2_chosen > a').click() // clicking input box of F for 2 Discharge Goal
                    cy.get('#GG0170F2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170G1_chosen > a').click() // clicking input box of G for 1 SOC/ROC Performance
                    cy.get('#GG0170G1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170G2_chosen > a').click() // clicking input box of G for 2 Discharge Goal
                    cy.get('#GG0170G2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170I1_chosen > a').click() // clicking input box of I for 1 SOC/ROC Performance
                    cy.get('#GG0170I1_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0170I2_chosen > a').click() // clicking input box of I for 2 Discharge Goal
                    cy.get('#GG0170I2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170J1_chosen > a').click() // clicking input box of J for 1 SOC/ROC Performance
                    cy.get('#GG0170J1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170J2_chosen > a').click() // clicking input box of J for 2 Discharge Goal
                    cy.get('#GG0170J2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170K1_chosen > a').click() // clicking input box of K for 1 SOC/ROC Performance
                    cy.get('#GG0170K1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170K2_chosen > a').click() // clicking input box of K for 2 Discharge Goal
                    cy.get('#GG0170K2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170L1_chosen > a').click() // clicking input box of L for 1 SOC/ROC Performance
                    cy.get('#GG0170L1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170L2_chosen > a').click() // clicking input box of L for 2 Discharge Goal
                    cy.get('#GG0170L2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170M1_chosen > a').click() // clicking input box of M for 1 SOC/ROC Performance
                    cy.get('#GG0170M1_chosen > div > ul > li.active-result.highlighted').click() //clicking result
                    cy.get('#GG0170M2_chosen > a').click() // clicking input box of M for 2 Discharge Goal
                    cy.get('#GG0170M2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170N1_chosen > a').click() // clicking input box of N for 1 SOC/ROC Performance
                    cy.get('#GG0170N1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170N2_chosen > a').click() // clicking input box of N for 2 Discharge Goal
                    cy.get('#GG0170N2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170O1_chosen > a').click() // clicking input box of O for 1 SOC/ROC Performance
                    cy.get('#GG0170O1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170O2_chosen > a').click() // clicking input box of O for 2 Discharge Goal
                    cy.get('#GG0170O2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170P1_chosen > a').click() // clicking input box of P for 1 SOC/ROC Performance
                    cy.get('#GG0170P1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170P2_chosen > a').click() // clicking input box of P for 2 Discharge Goal
                    cy.get('#GG0170P2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.wait(5000)
                    cy.get('#GG0170Q1_chosen > a').click() // clicking input box for Q
                    cy.get('#GG0170Q1_chosen > div > ul > li:nth-child(2)').click() // clicking result
                    cy.get('#GG0170R1_chosen > a').click() // clicking input box of R for 1 SOC/ROC Performance
                    cy.get('#GG0170R1_chosen > div > ul > li:nth-child(1)').click() //clicking result
                    cy.get('#GG0170R2_chosen > a').click() // clicking input box of R for 2 Discharge Goal
                    cy.get('#GG0170R2_chosen > div > ul > li:nth-child(1)').click() //clicking result
                    cy.get('#GG0170RR1_chosen > a').click() // clicking input box for RR1
                    cy.get('#GG0170RR1_chosen > div > ul > li:nth-child(1)').click() // clicking result
                    cy.get('#GG0170S1_chosen > a').click() // clicking input box of R for 1 SOC/ROC Performance
                    cy.get('#GG0170S1_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170S2_chosen > a').click() // clicking input box of R for 2 Discharge Goal
                    cy.get('#GG0170S2_chosen > div > ul > li:nth-child(2)').click() //clicking result
                    cy.get('#GG0170SS1_chosen > a').click() // clicking input box for RR1
                    cy.get('#GG0170SS1_chosen > div > ul > li:nth-child(1)').click() // clicking result
                    //SAVE BUTTON
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    // ADL/IADL/Musculoskeletal TAB End -------------

        });

        it('Automating Entry Form of Medication Tab', function() { 
                    // Medication TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(9)').click() //clicking Medication Tab
                    
                    //High-risk medications patient is currently taking (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > label > input').click()
                    //Home medication management (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(4) > label > input').click() //clicking yes for Medication discrepancy noted during this visit
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(3) > td.text.center.b-r-n.b-l-n.b-b-n.b-t-n > label > input').click() //clicking yes for Oral medications (tablets/capsules) prepared in a pill box
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(4) > td.text.center.b-r-n.b-l-n.b-t-n > label > input').click() //clicking yes for Use of medication schedule in taking medications
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(2) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
                    //(M2001) - Drug Regimen Review
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M2003) - Medication Follow-up
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(2) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M2010) - Patient/Caregiver High-risk Drug Education
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M2020) - Management of Oral Medications
                    cy.get('#M2020_CRNT_MGMT_ORAL_MDCTN > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()
                    //(M2030) - Management of Injectable Medications
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(5) > tbody > tr.molocked.ng-isolate-scope > td.oasis__answer > table > tbody > tr > td > div:nth-child(2) > label > input').click()

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(1) > td > label > input').click() //clicking Injectable medication(s) administered
                    //Form for Injectable medication(s) administered
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr.ng-scope > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting administered
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr.ng-scope > td > table > tbody > tr > td.b-r-n.b-l-n.p-l-5 > label:nth-child(1) > input').click() //clicking Intramuscular
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr.ng-scope > td > table > tbody > tr > td:nth-child(4) > input').type('Test') //inputting site
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention

                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr > td > label > input').click() //clicking Venous Access and IV Therapy
                    // Form for Venous Access and IV Therapy
                    //Access (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td > label.checkbox.checkbox-inline.m-b-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td > label.checkbox.checkbox-inline.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(3) > td > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(1) > td > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td.b-l-n > table > tbody > tr:nth-child(2) > td > label.m-l-5 > input').type('Test') //inputting other for access
                    cy.wait(5000)
                    //Purpose (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(1) > td > label.checkbox.checkbox-inline.m-b-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(2) > td > label.checkbox.checkbox-inline.m-b-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(3) > td > label.checkbox.checkbox-inline.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(1) > td > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(2) > td > label.checkbox.checkbox-inline.m-l-5.ng-isolate-scope > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr:nth-child(3) > td > label.m-l-5 > input').type('Test') //inputting other for purpose
                    cy.wait(5000)
                    //Peripheral Venous Access
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label.radio.radio-inline.m-l-10.ng-isolate-scope > input').click() //clicking short for catheter type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label.m-l-10.m-0 > input').type('Test') //inputting other for catheter type
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 24 for catheter gauge
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label.m-l-10.m-0 > input').type('Test') //inputting other for catheter gauge
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking left for insertion site
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(3) > input').click() //clicking arm for insertion site
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label.m-l-10.m-0 > input').type('Test') //inputting other for insertion site
                    cy.get('#SOOMEDICATION0024').type('12122021') //inputting insertion date
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div.display-ib.checkbox.m-0.m-l-15 > label > input').click() //clicking done
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > input').type('4') //inputting days for site change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking site condition for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting WNL for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(7) > td.p-l-5.p-r-5 > input').type('Test') //inputting for intervention
                    cy.wait(5000)
                    //Midline Catheter
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 8 cm for catheter length
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label:nth-child(3) > input').type('Test') //inputting other for catheter length
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 22 for catheter gauge
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(4) > input').type('Test') //inputting other for catheter gauge
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking left arm for insertion site
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(3) > input').type('Test') //inputting other for insertion site
                    cy.get('#SOOMEDICATION0178').type('12052021') //inputting date inserted
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > input').type('12') //inputting days dressing change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click() //clicking done for dressing change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking site condition for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting WNL for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Central Venous Access
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking PICC line for CVAD catheter
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label:nth-child(6) > input').type('Test') //inputting other for CVAD catheter
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking single for No. of lumens
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking peripheral for insertion site
                    cy.get('#SOOMEDICATION0069').type('09092021') //inputting date inserted
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > input').type('12') //inputting days for dressing change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click() //clicking done for dressing change
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('Test') //inputting Flushing solution
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('12') //inputting days for flush frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > div > label > input').click() //clicking done for flush frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(8) > td:nth-child(2) > label > input').click() //clicking site condition for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('Test') //inputing WNL for obseravtion
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(9) > td:nth-child(2) > input').type('Test') //inputing intervention
                    cy.wait(5000)
                    //Implanted Port
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking Portacath for port device
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label.m-0.m-l-20 > input').type('Test') //inputting other for port device
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking single chamber for port reservoir
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 0.50" for huber needle
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 19 for huber gauge
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > input').type('Test') //inputting flush solution
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > input').type('3') //inputting days for flush frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > label > input').click() //clicking done for flush frequency
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > label > input').click() //clicking site condition for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > input').type('Test') //inputting WNL for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr:nth-child(8) > td:nth-child(2) > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Intravenous Therapy
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr > td.p-b-5.b-r-n.p-l-5 > input').type('Test') //inputting IV medication
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr > td.p-b-5.b-l-n.text-center > a > i').click() //clicking add for IV medication
                    cy.wait(2000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.p-b-5.b-r-n.p-l-5 > input').type('Test') //inputting new IV medication
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td > table > tbody > tr > td.p-b-5.b-r-n.p-l-5 > input').type('Test') //inputting IV FLUID THERAPHY
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td > table > tbody > tr > td.p-b-5.b-l-n.text-center > a > i').click() //clicking add for FLUID THERAPHY
                    cy.wait(2000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td.p-b-5.b-r-n.p-l-5 > input').type('Test') //inputting new FLUID THERAPHY
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td.oasis__answer.b-l-n > label:nth-child(1) > input').click() //clicking IV Drip for administer via
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td.oasis__answer.b-l-n > input').type('Test') //inputting other for administer via
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(4) > td.oasis__answer.b-l-n > div > label > input').click() //clicking done for administer via
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(5) > td > table > tbody > tr > td.p-l-5 > input').type('Test') //inputting Flush procedure
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(6) > td > table > tbody > tr > td.p-l-5 > input').type('Test') //inputting Pre-infusion sol.
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(7) > td > table > tbody > tr > td.p-l-5 > input').type('Test') //inputting Post-infusion sol.
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr > td.p-l-5 > label > input').click() //clicking Patient tolerated IV therapy well with no S/S of adverse reactions for observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(8) > td > table > tbody > tr > td.p-l-5 > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(9) > td > table > tbody > tr > td.p-l-5 > input').type('Test') //inputting intervention

                    //Save Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                // Medication TAB End -------------

        });

        it('Automating Entry Form of Care Management Tab', function() {

                // CARE MANAGEMENT TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(10)').click() //clicking Care Management Tab
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click() //clicking Caregiver always available and reliable for caregiving status
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > input').type('Test') //inputting caregiver name
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > input').type('Test') //inputting relationship
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > input').type('1234567890') //inputting phone
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > input').type('Test') //inputting name of facility
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > input').type('1234567890') //inputting phone
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > input').type('Test') //inputting contact person
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > div > input').type('Test') //inputting Community resources utilized
                    // (M1100) - Patient Living Situation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > div > label > input').click() //clicking 08 for Patient Living Situation
                    //(M2102) - Types and Sources of Assistance
                    cy.get('#M2102_CARE_TYPE_SRC_SPRVSN').click()
                    //(M2200) - Therapy Need
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(3) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > label > input').type('000')
                    //Physician Orders for Disciplines and Ancillary Referrals
                    //Discipline (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(4) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(6) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(7) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(8) > label > input').click()
                    //Frequency	(input all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(2) > textarea').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(3) > textarea').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(4) > textarea').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(5) > textarea').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(6) > textarea').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(7) > textarea').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(8) > textarea').type('Test')
                    
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(3) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting Other MD Referral Orders
                    cy.wait(5000)
                    //Physical Therapy (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(4) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other physical theraphy
                    //Occupational Therapy (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other Occupational Therapy
                    //Speech Therapy/SLP (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other Speech Therapy/SLP
                    //MSW (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(7) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other MSW
                    //CHHA
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(8) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for CHHA
                    //Dietician
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(9) > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for Dietician
                    //DME
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking has for walker
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking has for cane
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking has for wheelchair
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click() //clicking has for Bedside commode	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //clicking has for Shower bench
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //clicking has for Feeding pump
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(8) > label > input').click() //clicking has for Suction machine
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(8) > label > input').click() //clicking has for O2 concentrator
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(8) > label > input').click() //clicking has for Nebulizer machine
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(11) > label > input').click() //clicking has for Hospital bed
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(11) > label > input').click() //clicking has for Low air loss mattress
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(10) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for DME
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(11) > label > input').click() //clicking has for OTHER
                    
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > label > input').click() //clicking yes for DME working properly?	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(10) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(5) > div > label > input').click() //clicking Malfunction reported to DME vendor
                    cy.wait(5000)
                    //Reasons for Home Health (Mark all that apply)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
                    //Additional reasons for home health
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(11) > tbody > tr:nth-child(4) > td > div > div > textarea').type('Test')
                    cy.wait(5000)
                    //Homebound Status (Must meet the two criteria below)
                    //Criterion One
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(3) > td > table > tbody > tr > td > div:nth-child(1) > label > input').click()
                    //Criterion Two
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(5) > td > table > tbody > tr > td > div:nth-child(1) > label > input').click()
                    //Conditions that support the additional requirements in Criterion Two (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click('')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(12) > tbody > tr:nth-child(7) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for Conditions that support the additional requirements in Criterion Two
                    cy.wait(5000)
                    //Safety Measures (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > div > label > input').click()
                    //Additional safety measures
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(13) > tbody > tr:nth-child(4) > td > div > textarea').type('Test')
                    cy.wait(5000)
                    //Home Environment Safety (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > div.lopt-left > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(1) > div > div.cont-opt.m-lopt-150 > div > input').type('5')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td:nth-child(1) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td.ng-isolate-scope > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(7) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(8) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(9) > td:nth-child(2) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(10) > td:nth-child(2) > div.cont-opt.m-lopt-50 > div > input').type('Test') //inputting other for Home Environment Safety
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(3) > td.b-l-n > input').type('Test') //inputting observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(14) > tbody > tr:nth-child(4) > td.b-l-n > input').type('Test') //inputting intervention
                    cy.wait(5000)
                    //Emergency Planning/Fire Safety
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking yes for Smoke detectors on all levels of home?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking yes for Smoke detectors tested and functioning?	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking yes for Fire extinguisher?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking yes for More than one exit?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(6) > td:nth-child(2) > label > input').click() //clicking yes for Plan for exit?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click() //clicking yes for Plan for power failure?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() //clicking yes for Carbon monoxide detector?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(4) > td:nth-child(4) > div > label > input').click() //clicking SN to assist patient to obtain ERS button
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(5) > td:nth-child(4) > div > label > input').click() //clicking SN to develop individualized emergency plan for patient
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(15) > tbody > tr:nth-child(6) > td:nth-child(4) > input').type('Test') //inputting comment
                    cy.wait(5000)
                    //Advance Directives
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking yes for Does patient have an Advanced Directive?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking yes for Is the patient DNR (Do Not Resuscitate)?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking yes for Is the patient DNI (Do Not Intubate)?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(2) > label > input').click() //clicking yes for Does the patient have a Living Will?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(2) > td:nth-child(5) > label > input').click() //clicking yes for Does patient have a Medical Power of Attorney?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(4) > input').type('Test') //inputting Name
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(3) > td:nth-child(5) > label > input').click() // clicking yes for Has a surrogate?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(4) > td:nth-child(5) > label > input').click() //clicking yes for Are copies on file with the Agency?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(16) > tbody > tr:nth-child(5) > td:nth-child(5) > label > input').click() //clicking yes for Is patient provided with written and verbal information on the subject?
                    cy.wait(5000)
                    //Patient Classification Risk Levels
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(17) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div > label > input').click() //clicking level 1(high priority)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(17) > tbody > tr:nth-child(3) > td.b-l-n > table > tbody > tr > td:nth-child(1) > label > input').click() //clicking poor for prognosis
                    
                    //SKILLED INTERVENTIONS
                    //Skilled Assessment and Observation
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(19) > tbody > tr:nth-child(2) > td > div > textarea').type('Test')
                    //Skilled Teaching and Instructions
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(20) > tbody > tr:nth-child(2) > td > div > textarea').type('Test')
                    //Patient/Caregiver Response to Skilled Interventions
                    //For Patient
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking full for Verbalized understanding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking good for Return demonstration performance
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > label.radio.radio-inline.m-l-30.m-b-5.ng-isolate-scope > input').click() //clicking yes for Need follow-up teaching/instruction
                    //For Caregiver
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking full for Verbalized understanding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > label:nth-child(1) > input').click() //clicking good for Return demonstration performance
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(3) > td:nth-child(3) > label.radio.radio-inline.m-l-30.m-b-5.ng-isolate-scope > input').click() //clicking yes for Need follow-up teaching/instruction
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(21) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(4) > td > input').type('Test') //inputting comment
                    cy.wait(5000)
                    //Care Coordination
                    cy.get('#SOOCAREMNG0001_chosen > a').click() //clicking dropdown
                    cy.get('#SOOCAREMNG0001_chosen > div > ul > li:nth-child(1)').click() //clicking result
                    cy.get('#SOOCAREMNG0014').type('09232021') //inputting date
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(5) > input').type('1200') //inputting hour
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > div > textarea').type('Test') //inputting regarding
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(3) > td > table > tbody > tr:nth-child(2) > td > div > textarea').type('Test') //inputting Additional MD orders
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(4) > td > input').type('Test') //inputting Patient’s next physician visit
                    //Case conference with (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > label:nth-child(1) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > label:nth-child(2) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > label:nth-child(3) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > label:nth-child(4) > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > label:nth-child(5) > input').click()
                    cy.get('[name="SOOCAREMNG0075"]').type('Test') //inputting other
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > label:nth-child(9) > input').click() // clicking EMR for via
                    cy.get('#SOOCAREMNG0283').type('09122021') //inputting date
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(5) > td > table > tbody > tr:nth-child(2) > td > input:nth-child(14)').type('1200') //inputting hour
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(3) > label > input').click() // clicking yes for first visit
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(5) > label > input').click() // clicking CA Identification card	
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(6) > label > input').click() // clicking Medicare card
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(7) > label > input').click() // clicking CA Driver’s license
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(8) > input').type('Test') //inputting other
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(22) > tbody > tr:nth-child(7) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting Plan for next visit
                    cy.wait(5000)
                    //Discharge Plans (select all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(1) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(2) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(3) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(4) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(5) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(6) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(7) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(8) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td > div:nth-child(9) > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > div.cont-opt > label:nth-child(1) > input').click() //clicking patient
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > div.cont-opt > label:nth-child(2) > input').click() //clicking PCG
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > div.cont-opt > label:nth-child(3) > input').click() //clicking OTHER
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > div.cont-opt > input').type('Test')
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td > div.cont-opt > label:nth-child(5) > input').click() //clicking agreeable
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td > div.cont-opt.m-lopt-45.ng-isolate-scope > label:nth-child(1) > input').click() //clicking good
                    //Additional discharge plans
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(23) > tbody > tr:nth-child(4) > td > div > textarea').type('Test')
                    //Patient Goals
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(24) > tbody > tr:nth-child(2) > td > div > textarea').type('Test')
                    //SAVE Button
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                // CARE MANAGEMENT TAB End -------------

        });

    
    })