Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of OASIS - Transfer', () => {

           
        beforeEach(() => {
            cy.login('superagent@geekers', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://app.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
               cy.get('.searchbar__content > .ng-pristine').type('Peralta')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                
                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(19) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click Transfer (not discharged)
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button').click() //click Edit Button
        })
           
        it('Automating Entry Form of Demographics / Clinical Record', function() {
            

                    const dayjs = require('dayjs')

                //DEMOGRAPHICS/CLINICAL RECORD TAB Start -------------
                    cy.get('#ti').type('1200') //inputting time in
                    cy.get('#to').type('1900') //inputting time out
                    cy.wait(2000)
                    cy.get('#M0090_INFO_COMPLETED_DT').type(dayjs().format('MM/DD/YYYY'))  //inputting date assessment completed
                    //(M0150) - Current Payment Sources for Home Care
                    cy.get('#M0150_CPAY_MCARE_FFS > input').click()
                    //(M1041) - Influenza Vaccine Data Collection Period
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(26) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1
                    //(M1046) - Influenza Vaccine Received
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(28) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1
                    //(M1051) - Pneumococcal Vaccine:
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(30) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking NO
                    //(M1056) - Reason Pneumococcal Vaccine not received
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(32) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(3) > label > input').click() //clicking 3
                    cy.wait(5000)
                    //SAVE BUTTON
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                //DEMOGRAPHICS/CLINICAL RECORD TAB End -------------

        });

        it('Automating Entry Form of Medication Record', function() {
                

                //MEDICATION TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(2)').click() //clicking medication tab
                    cy.wait(5000)
                    //(M2005) - Medication Intervention: Did the agency contact and complete physician (or physician-designee) prescribed/recommended actions by midnight of the next calendar day each time potential clinically significant medication issues were identified since the SOC/ROC?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table:nth-child(1) > tbody > tr:nth-child(2) > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-Yes
                    //(M2016) - Patient/Caregiver Drug Education Intervention: At the time of, or at any time since the most recent SOC/ROC assessment, was the patient/caregiver instructed by agency staff or other health care provider to monitor the effectiveness of drug therapy, adverse drug reactions, and significant side effects, and how and when to report problems that may occur?
                    cy.get('#M2016_DRUG_EDCTN_INTRVTN > td.oasis__answer > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-Yes
                    //SAVE BUTTON
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                //MEDICATION TAB End -------------

        });

        it('Automating Entry Form of Emergent Care Record', function() {


                //EMERGENT CARE TAB Start -------------
                    cy.get('#oasis-tabs > label:nth-child(3)').click() //clicking emergent care tab
                    cy.wait(5000)
                    //(M2301) - Emergent Care
                    cy.get('#M2301_EMER_USE_AFTR_LAST_ASMT > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click()
                    //(M2310) - Reason for Emergent Care (mark all that apply)
                    cy.get('#M2310_ECR_MEDICATION').click()
                    cy.get('#M2310_ECR_HYPOGLYC').click()
                    cy.get('#M2310_ECR_OTHER').click()
                    
                    //DATA ITEMS COLLECTED AT INPATIENT FACILITY ADMISSION OR AGENCY DISCHARGE ONLY
                    //(M2401) - Intervention Synopsis (clicking yes to all)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(3) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(4) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(5) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(6) > td:nth-child(3) > div > label > input').click()
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(7) > td.oasis__answer.p-0 > table > tbody > tr:nth-child(7) > td:nth-child(3) > div > label > input').click()
                    cy.wait(5000)
                    // /(M2410) - To which Inpatient Facility has the patient been admitted?
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(9) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(1) > label > input').click() //clicking 1 - Hospital
                    //(M0906) - Discharge/Transfer/Death Date
                    cy.get('#M0906_DC_TRAN_DTH_DT_DATEPICKER').type('05292022')
                    
                    //Section J: Health Conditions
                    //J1800 - Any Falls Since SOC/ROC
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > fieldset > table > tbody > tr:nth-child(14) > td.oasis__answer.v-top > table > tbody > tr > td > div > div:nth-child(2) > label > input').click() //clicking 1-YES
                    //J1900 - Number of Falls Since SOC/ROC
                    cy.get('[data-ng-model="data.J1900A"]').type('1')
                    cy.get('#J1900B').type('1') //inputting 1 B. Injury (except major)
                    cy.get('#J1900C').type('1') //inputting 1 C. Major injury
                    //SAVE BUTTON
                    cy.get('#titleNoteBar > div.col-sm-12.p-0.title__section.m-b-10.oasis_actionBtnTab > div:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click() //clicking save button
                    cy.wait(5000)
                //EMERGENT CARE TAB End -------------

            })

    
    })