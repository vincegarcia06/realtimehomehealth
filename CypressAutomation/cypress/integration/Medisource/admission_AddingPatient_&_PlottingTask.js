Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />

describe('Admission Test', () => {
           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@');
            // cy.login('vincegarcia@geekers', 'Tester2021@');
            
        })
        
            it('Adding Patient', function() {     
    
                var bdate = "01011955"
    
                //getting the date now
                const dayjs = require('dayjs')
                //cy.log(dayjs().format('MM/DD/YYYY'));  //Prints todays date
                
                //random lastname
                var lname = ['Messi', 'Ronaldo', 'Chuck', 'Benzama', 'Benzon', 'John', 'James']
                var x = Math.floor((Math.random() *6)) +1;
                var lastname = lname[x];
                
                
                //Adding of Patient or Pre Admission form
    
                cy.visit('https://qado.medisource.com/patient');
                cy.wait(5000);
                //Patient Information section
                cy.get('.mhc__btn-neutral').click();
                cy.get('#refDate').type(dayjs().add(1, 'day').format('MM/DD/YYYY')) ;//dd/mm/yyy
                cy.wait(5000)
                cy.get(':nth-child(2) > [colspan="2"] > table > tbody > tr > .global__table-question > .checkbox > label > .ng-pristine').click();
                cy.get('#pre_admission_date').type(dayjs().format('MM/DD/YYYY'));
                cy.get('#last_name').type('Test');
                cy.get('#first_name').type('Kovy');
                cy.get('#mi').type('M');
                cy.get('#birthdate').type('12121955');
                cy.get('.custom-input-radio > :nth-child(1) > .ng-pristine').check();
                cy.get('#marital_status_chosen > .chosen-single').click();
                cy.get('#marital_status_chosen > div > ul > li:nth-child(1)').click();
                cy.get('#ethnicities_chosen > .chosen-choices > .search-field > .default').click();
                cy.get('#ethnicities_chosen > div > ul > li:nth-child(1)').click();
    
                //Patient Address section
                cy.get('#main_line1').type("1752 Wilson Ave");
                cy.get('#main_street').type("Arcadia");
                cy.get('#main_city').type("Arcadia");
                cy.wait(5000);
                cy.get('#main_state_chosen > a').click();
                cy.get('#main_state_chosen > div > ul > li:nth-child(6)').click();
                cy.get('#main_zipcode').type("91008");
                cy.get(':nth-child(22) > .oasis__answer > .fg-line > .global__txtbox').type("3216549871");
                cy.get(':nth-child(23) > .oasis__answer > .fg-line > .global__txtbox').type("3216549871");
                cy.get('#main_email').type("arc@mailinator.com");
    
                //Service Delivery Addres Section
                cy.get(':nth-child(25) > [colspan="2"] > .btn__dark-blue_icon-bordered').click();//clicking Same as Patient Address button
    
                //Living Situation section
                cy.get(':nth-child(35) > .oasis__answer > .custom-input-radio > .checked > .ng-valid').click();
                cy.get(':nth-child(36) > .oasis__answer > .custom-input-radio > .checked > .ng-valid').click();
                cy.get('#ls_caregiver').type('James Nanny');
                cy.get('#ls_caregiver_phone').type('3258459612');
    
                //Emergency Contacts
                cy.get('#ec_name').type('James Nanny');
                cy.get('#ec_relationship').type('Nanny');
                cy.get('#ec_phone').type(1323445463);
                cy.get('#ec_other_phone').type(6574323456);
    
                //Physician Information Section
                cy.get('#physician_attending_chosen > .chosen-single').click();
                cy.get('#physician_attending_chosen > div > ul > li:nth-child(2)').click();
                cy.get('#physician_primary_chosen > .chosen-single').click();
                cy.get('#physician_primary_chosen > div > ul > li:nth-child(2)').click();
                cy.get('[style="height: unset;"] > .select > .chosen-container > .chosen-choices').click();//other physician
                cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(46) > td:nth-child(2) > div > div > div > ul > li:nth-child(2)').click();
    
                //Insurance Information Section
                cy.wait(5000)
                cy.get(':nth-child(48) > .global__table-question').scrollIntoView().should('be.visible');
                cy.get('#primary_insurance_chosen > .chosen-single').click();
                cy.get('#primary_insurance_chosen > div > ul > li:nth-child(2)').click();
                cy.get('#primary_insurance_policy').type('qwerty45678');
                cy.get('#secondary_insurance_chosen > .chosen-single').click();
                cy.get('#secondary_insurance_chosen > div > ul > li:nth-child(2)').click();
                cy.get('#secondary_insurance_policy').type('qwerty1234');
                
                //Referral Information Section
                cy.get('#admissiontype_chosen > .chosen-single').click();
                cy.get('#admissiontype_chosen > div > ul > li:nth-child(1)').click();
                cy.get('#point_of_origin_chosen > .chosen-single').click();
                cy.get('#point_of_origin_chosen > div > ul > li:nth-child(1)').click();
                cy.get('#referral_type_chosen > .chosen-single').click();
                cy.get('#referral_type_chosen > div > ul > li:nth-child(3)').click();
                cy.get('#referral_source_id_chosen > .chosen-single').click();
                cy.get('#referral_source_id_chosen > div > ul > li:nth-child(2)').click();
    
                //Hospitalization Information Section
                cy.get('#hospital_id_chosen > .chosen-single').click();
                cy.get('#hospital_id_chosen  > div > ul > li:nth-child(2)').click();
                cy.get('#admit_date').type('12122021');
                cy.get('#discharge_date').type('12122021');
    
                //Diagnosis & Pre admission Section
                cy.get('#diagnosis_surgery').type('heart failure');
                cy.get('#diagnosis_allergies > .host > .tags > .ng-pristine').type('peanut{enter}');
    
                //M0080 section
                cy.get(':nth-child(68) > .oasis__answer > .fg-line > :nth-child(1) > .ng-pristine').click();
    
                //Clinical Staff Section
                cy.get('#cs_rn_chosen > .chosen-single').click();
                cy.get('#cs_rn_chosen > div > ul > li:nth-child(1)').click();//selected REGISTERED NURSE
                cy.get('#cs_cm_chosen > .chosen-single').click();
                cy.get('#cs_cm_chosen > div > ul > li:nth-child(1)').click();//selected CASE MANAGER
    
                //SAVE THE RECORD
                cy.get('.btn__success').click();
                cy.wait(10000)
    
            });
           
            it.only('Go to Patient List and select the added patient and plotting all task', function() 
            //it.only('Go to Patient List and select the added patient and plotting all task', function() 
            {
                cy.viewport(1920, 924);//setting your windows size
                cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
                cy.wait(5000)   
                const dayjs = require('dayjs')
                    cy.get('.searchbar__content > .ng-pristine').type('Sumera, Jow P. Jr')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
                    cy.wait(5000)
                    
                    //schedule LVN/LPN - Skilled Visit
                    cy.get('#save > li > button').click()//clicking the calendar overly icon
                    cy.get('#selecteddate').type(dayjs().add(2, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                    cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                    cy.wait(5000)
                    cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(8)').click()//select task
                    cy.wait(5000)
                    cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                    cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                    cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                    cy.get('.mhc__btn-affirmative').click()//save task created
                    cy.wait(10000)
                     //schedule LVN/LPN - Wound Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(3, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(9)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule PRN - Skilled Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(4, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(10)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - Education Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(5, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(11)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - IV Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(8, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(12)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - Joint Supervisory (CHHA)
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(9, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(13)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - Joint Supervisory (LVN)
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(10, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(14)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - Skilled Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(11, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(15)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - Supervisory Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(12, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(16)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule RN - Wound Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(14, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(17)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule PT - Initial Eval
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(19, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(19)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule OT - Initial Eval
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(20, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(24)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule ST - Initial Eval
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(22, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(29)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule MSW - Assessment
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(24, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(33)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule MSW - Follow-up Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(26, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(34)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
                     //schedule CHHA - HHA Visit
                     cy.get('#save > li > button').click()//clicking the calendar overly icon
                     cy.get('#selecteddate').type(dayjs().add(31, 'day').format('MM/DD/YYYY')) ;//inputting taskdate
                     cy.get('.task-man > .select > .chosen-container > .chosen-single').click()//clicking task dropdown
                     cy.wait(5000)
                     cy.get('#tooltip_err7 > div > div > div > div > ul > li:nth-child(36)').click()//select task
                     cy.wait(5000)
                     cy.get('.staff-man > .select > .chosen-container > .chosen-single').click()//click staff dropdown
                     cy.get('#tooltip_err2 > div > div > div > div > ul > li:nth-child(2)').click()//select clinician
                     cy.get('body > div.modal.fade.ng-isolate-scope.draggable__modal-window.in > div > div > div > div > div.ng-isolate-scope.ng-empty.ng-valid > div.card-body.card-padding.modal-body.p-l-25.p-r-25.p-t-0.p-b-0 > div > div > ng-form > div.ng-scope > div:nth-child(2) > div > label > input').click()//uncheck Create an MD Order
                     cy.get('.mhc__btn-affirmative').click()//save task created
                     cy.wait(5000)
    
           
             });
    
    });