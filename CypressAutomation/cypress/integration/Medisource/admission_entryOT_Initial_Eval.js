Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of OT - Initial Eval', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Peralta M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(14) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click OT - Initial Eval
                    cy.get('.btn__warning').click() //clicking edit button
                })
           
        it('Automating Entry Form of Assessment Tab', function() {
            cy.wait(10000)
            cy.get('#timeIn').type('1200') //inputting time in
            cy.get('#timeOut').type('1900') //inputting time out
            cy.get('#Company').type('Test') //inputting company
            cy.wait(2000)
            //dIAGNOSES
            //Medical Diagnosis
            cy.get('[width="40%"] > .global__txtbox').type('Test')
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > label > input').click() //clicking Exacerbation
            cy.get('[width="10%"] > .checkbox > .ng-pristine').click() //clicking onset
            cy.get(':nth-child(2) > .p-l-5 > .global__txtbox').type('Test')
            //OT Diagnosis
            cy.get('tbody > :nth-child(4) > :nth-child(1) > .global__txtbox').type('Test')
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click() //clicking Exacerbation
            cy.get('#parent > div > div > div.max__width_wrapper.o-hidden > fieldset > form > div > div:nth-child(1) > fieldset:nth-child(3) > table > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(3) > label > input').click() //clicking onset
            cy.get(':nth-child(4) > .p-l-5 > .global__txtbox').type('Test')
            //Relevant Medical History (Select All)
            cy.get('[rname="rmh_multi"]').click({multiple:true})
            cy.get('[style="width:50%;"] > .m-t-0 > :nth-child(1) > :nth-child(1) > td > .global__txtbox').type('Test') //inputting joint replacement
            cy.get('.m-t-0 > :nth-child(1) > :nth-child(2) > :nth-child(1) > .global__inner-nb > tbody > tr > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting fall history within 60 days
            cy.get(':nth-child(4) > .fg-line > .global__txtbox').type('Test') //inputting fall history within last year
            cy.get(':nth-child(2) > .m-t-0 > :nth-child(1) > :nth-child(1) > td > .global__txtbox').type('Test') //inputting contractures
            cy.get(':nth-child(1) > .global__inner-nb > tbody > tr > :nth-child(2) > .radio > .ng-pristine').click() //clicking new
            cy.get(':nth-child(5) > .fg-line > .global__txtbox').type('Test') //inputting location
            //Prior Level of Functioning
            cy.get(':nth-child(4) > td > .fg-line > .table-input').type('Test')
            //Patient's Goals
            cy.get(':nth-child(6) > td > .fg-line > .table-input').type('Test')
        
            //Homebound Status
            cy.get('[rname="hbs_multi"]').click({multiple:true})
            cy.get(':nth-child(1) > .cont-opt > div > .global__txtbox').type('Test') //inputting other
            cy.wait(5000)
            //Patient living situation and availability of assistance (Check one box only)
            cy.get(':nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking around the clock on patient live alone
            
            //Home environment safety and risk factors predisposing for fall
            cy.get('[rname="hes_multi"]').click({multiple:true , force:true})
            cy.get('[width="50%"] > .checkbox > .ng-scope').click() //clicking no hazards identified
            cy.get('[width="70%"] > .cont-opt > div > .global__txtbox').type('Test') //inputting other
            cy.wait(5000)

            //Vitals Sign Information Section
            cy.get(':nth-child(2) > [colspan="2"] > .pull-left > .global__txtbox').type('3') //inputting Pulse/HR
            cy.get(':nth-child(2) > :nth-child(3) > :nth-child(3) > .ng-pristine').click() //clicking radial for Pulse/HR
            cy.get(':nth-child(3) > [colspan="2"] > .pull-left > .global__txtbox').type('3') //inputting respiration
            cy.get(':nth-child(10) > .p-0 > .global__inner-wb > tbody > :nth-child(3) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking regular for respiration
            cy.get(':nth-child(4) > [colspan="2"] > .pull-left > .global__txtbox').type('180') //inputting BP Right Arm
            cy.get(':nth-child(10) > .p-0 > .global__inner-wb > tbody > :nth-child(4) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking sitting
            cy.get(':nth-child(5) > [colspan="2"] > .pull-left > .global__txtbox').type('180') //inputting BP Left Arm
            cy.get(':nth-child(10) > .p-0 > .global__inner-wb > tbody > :nth-child(5) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking sitting
            cy.get('[colspan="3"] > .pull-left > .global__txtbox').type('2') //inputting O2 Saturation % on room air
            cy.get(':nth-child(7) > [colspan="3"] > :nth-child(1) > .global__txtbox').type('2') //inputting O2 Saturation % on O2
            cy.get('.m-l-10 > .global__txtbox').type('2') //inputting lPM on o2 Saturation
            cy.get('[colspan="3"] > :nth-child(5) > .ng-pristine').click() //clicking nasal cannula
            cy.wait(5000)

            //Physical Assessment
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for speech
            cy.get(':nth-child(2) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting speech
            cy.get(':nth-child(3) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for vision
            cy.get(':nth-child(3) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting vision
            cy.get(':nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for hearing
            cy.get(':nth-child(4) > :nth-child(2) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting hearing
            cy.get(':nth-child(2) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for skin
            cy.get(':nth-child(2) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting skin
            cy.get(':nth-child(3) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for orientation
            cy.get(':nth-child(3) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting orientation
            cy.get(':nth-child(4) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(1) > .checkbox > .ng-pristine').click() //clicking WFL for cognitive
            cy.get(':nth-child(4) > :nth-child(4) > .global__inner-nb > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting cognitive

            //Pain Assesment
            cy.get(':nth-child(3) > :nth-child(2) > .global__txtbox').type('Test') //inputting pain location site 1
            cy.get('.m-t-20 > tbody > :nth-child(4) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking acute type of pain site 1
            cy.get(':nth-child(5) > :nth-child(2) > :nth-child(2) > .ng-pristine').click() //clicking 1 present level site 1
            cy.get(':nth-child(3) > :nth-child(3) > .global__txtbox').type('Test') //inputting pain location site 2
            cy.get('.m-t-20 > tbody > :nth-child(4) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking acute type of pain site 2
            cy.get('.m-t-20 > tbody > :nth-child(5) > :nth-child(3) > :nth-child(2) > .ng-pristine').click() //clicking 1 present level site 2

            //What makes the pain worse?
            cy.get('[rname="pain_worse_multi"]').click({multiple:true})
            cy.get('.global__inner-nb > tbody > :nth-child(5) > td > .fg-line > .global__txtbox').type('Test')
            //What makes the pain better?	
            cy.get('[rname="pain_better_multi"]').click({multiple:true})
            cy.get(':nth-child(8) > td > .fg-line > .global__txtbox').type('Test')
            cy.wait(5000)

            //Musculoskeletal Assessment: ROM and Strength

            //NECK Start---------------------------------------------
            //Neck(Strength)
            //Flexion
            cy.get(':nth-child(5) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(5) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(6) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(6) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Lateral Flexion
            cy.get(':nth-child(7) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Rotation
            cy.get(':nth-child(8) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //Neck(ROM)
            //Flexion
            cy.get(':nth-child(5) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(5) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(6) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(6) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Lateral Flexion
            cy.get(':nth-child(7) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(7) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Rotation
            cy.get(':nth-child(8) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(8) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //NECK End---------------------------------------------

            //SHOULDER Start---------------------------------------------
            //Shoulder(Strength)
            //Flexion
            cy.get(':nth-child(10) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(11) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(11) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Abduction
            cy.get(':nth-child(12) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(12) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Adduction
            cy.get(':nth-child(13) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(13) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right            
            //External Rotation
            cy.get(':nth-child(14) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(14) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Internal Rotation
            cy.get(':nth-child(15) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(15) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Shoulder(ROM)
            //Flexion
            cy.get(':nth-child(10) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(10) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extens
            cy.get(':nth-child(11) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(11) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Abduction
            cy.get(':nth-child(12) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(12) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Adduct
            cy.get(':nth-child(13) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(13) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right            
            //External Rotation
            cy.get(':nth-child(14) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(14) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Internal Rotation
            cy.get(':nth-child(15) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(15) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //SHOULDER End---------------------------------------------

            //ELBOW Start---------------------------------------------
            //Elbow(Strength)
            //Flexion
            cy.get(':nth-child(17) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(17) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(18) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(18) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Elbow(ROM)
            //Flexion
            cy.get(':nth-child(17) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(17) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(18) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(18) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //ELBOW End---------------------------------------------

            //FOREARM Start---------------------------------------------
            //FOREARM(Strength)
            //Pronation	
            cy.get(':nth-child(20) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(20) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Supination
            cy.get(':nth-child(21) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(21) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //FOREARM(ROM)
            //Pronation	
            cy.get(':nth-child(20) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(20) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Supination
            cy.get(':nth-child(21) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(21) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //FOREARM End---------------------------------------------

            //WRIST Start---------------------------------------------
            //Wrist(Strength)
            //Flexion
            cy.get(':nth-child(23) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(23) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(24) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(24) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Radial Deviation
            cy.get(':nth-child(25) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(25) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Ulnar Deviation
            cy.get(':nth-child(26) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(26) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right            

            //Wrist(ROM)
            //Flexion
            cy.get(':nth-child(23) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(23) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Extension
            cy.get(':nth-child(24) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(24) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Radial Deviation
            cy.get(':nth-child(25) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(25) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Ulnar Deviation
            cy.get(':nth-child(26) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(26) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right            
            cy.wait(5000)
            //WRIST End---------------------------------------------

            //THUMB Start---------------------------------------------
            //THUMB(Strength)
            //Flexion (MP Joint)
            cy.get(':nth-child(28) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(28) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Flexion (IP Joint)
            cy.get(':nth-child(29) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(29) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //THUMB(ROM)
            //Flexion (MP Joint)
            cy.get(':nth-child(28) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(28) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Flexion (IP Joint)
            cy.get(':nth-child(29) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(29) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //THUMB End---------------------------------------------

            //FINGERS Start---------------------------------------------
            //FINGERS(Strength)
            //Flexion (MCPJ)	
            cy.get(':nth-child(31) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(31) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Flexion (PIPJ)	
            cy.get(':nth-child(32) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(32) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Flexion (DIPJ)
            cy.get(':nth-child(33) > :nth-child(2) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(33) > :nth-child(4) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //FINGERS(ROM)
            //Flexion (MCPJ)	
            cy.get(':nth-child(31) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(31) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Flexion (PIPJ)	
            cy.get(':nth-child(32) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(32) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            //Flexion (DIPJ)
            cy.get(':nth-child(33) > :nth-child(6) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting left
            cy.get(':nth-child(33) > :nth-child(8) > #tablenoborder > tbody > tr > td > .fg-line > .global__txtbox').type('1') //inputting right
            cy.wait(5000)
            //FINGERS End---------------------------------------------

            //Functional Assessment Codes
            //Bed Mobility
            cy.get(':nth-child(5) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(6) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(7) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(16) > .p-0 > .global__inner-wb > :nth-child(1) > :nth-child(5) > :nth-child(4) > .radio > .ng-pristine').click() //roll left and right code
            cy.get(':nth-child(6) > :nth-child(4) > .radio > .ng-pristine').click() //sit to lying code
            cy.get(':nth-child(7) > :nth-child(4) > .radio > .ng-pristine').click() //lying to sitting on side of bed code
        
            //Transfer
            cy.get(':nth-child(9) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(10) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(11) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(12) > :nth-child(2) > .fg-line > .global__txtbox').type('Trapeze{enter}') //inputting assistive device
            cy.get(':nth-child(9) > :nth-child(3) > .radio > .ng-pristine').click() //Sit to stand code
            cy.get(':nth-child(10) > :nth-child(3) > .radio > .ng-pristine').click() //Chair/bed-to-chair transfer code
            cy.get(':nth-child(11) > :nth-child(3) > .radio > .ng-pristine').click() //Shower or tub code
            cy.get(':nth-child(12) > :nth-child(3) > .radio > .ng-pristine').click() //Car transfer code

            //Gait/Ambulation
            cy.get(':nth-child(14) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(15) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(16) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(17) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(18) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(19) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(20) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(21) > :nth-child(2) > .fg-line > .global__txtbox').type('Quad-cane{enter}') //inputting assistive device
            cy.get(':nth-child(14) > :nth-child(3) > .radio > .ng-pristine').click() //Grooming (personal hygiene)	 code
            cy.get(':nth-child(15) > :nth-child(3) > .radio > .ng-pristine').click() //Eating or feeding code
            cy.get(':nth-child(16) > :nth-child(3) > .radio > .ng-pristine').click() //Oral hygiene code
            cy.get(':nth-child(17) > :nth-child(3) > .radio > .ng-pristine').click() //Toileting hygiene code
            cy.get(':nth-child(18) > :nth-child(3) > .radio > .ng-pristine').click() //Shower/bathe self code
            cy.get(':nth-child(19) > :nth-child(3) > .radio > .ng-pristine').click() //Upper body dressing code
            cy.get(':nth-child(20) > :nth-child(3) > .radio > .ng-pristine').click() //Lower body dressing code
            cy.get(':nth-child(21) > :nth-child(3) > .radio > .ng-pristine').click() //Put on/take off footwear	 code

            //Balance
            cy.get(':nth-child(23) > [colspan="11"] > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-valid').click() //Sitting (Static) Assessment
            cy.get(':nth-child(24) > [colspan="11"] > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-valid').click() //Sitting (Dynamic) Assessment
            cy.get(':nth-child(25) > [colspan="11"] > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-valid').click() //Standing (Static) Assessment
            cy.get(':nth-child(26) > [colspan="11"] > .global__inner-nb > tbody > tr > :nth-child(1) > .radio > .ng-valid').click() //Standing (Dynamic) Assessment

            cy.get('[style="width: 55%;"] > .global__txtbox').type('Test') //inputting assisstive device
            cy.get('td > :nth-child(2) > :nth-child(3) > .global__txtbox').type('Test') //inputting requirements
            cy.get(':nth-child(5) > .global__txtbox').type('Test') //inputting malfunction
            cy.get(':nth-child(4) > .m-r-15 > .ng-pristine').click() //clicking daily for frequency use

            //Summary of problems identified that need therapeutic intervention
            cy.get('[rname="spi_multi"]').click({multiple:true})
            cy.get('[style="width: 100%"] > .global__inner-nb > tbody > :nth-child(1) > :nth-child(2) > .checkbox > .ng-scope').click() //clicking pain
            cy.get('[style="width: 100%"] > .global__inner-nb > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(2) > .cont-opt > div > .global__txtbox').type('Test')
            //Treatment Plan
            //Pain
            cy.get('#notestable > tbody > tr:nth-child(2) > td > textarea').eq(0).type('Test', {force:true})
            cy.get('#notestable > tbody > tr:nth-child(3) > td > textarea').eq(0).type('Test',{force:true})
            //Assistive Device
            cy.get('#notestable > tbody > tr:nth-child(2) > td > textarea').eq(1).type('Test',{force:true})
            cy.get('#notestable > tbody > tr:nth-child(3) > td > textarea').eq(1).type('Test',{force:true})
            //Safety
            cy.get('#notestable > tbody > tr:nth-child(2) > td > textarea').eq(2).type('Test',{force:true})
            //Knowledge Deficit
            cy.get('#notestable > tbody > tr:nth-child(2) > td > textarea').eq(3).type('Test',{force:true})
            cy.get('#notestable > tbody > tr:nth-child(3) > td > textarea').eq(3).type('Test',{force:true})
            //Other Problems
            cy.get('#notestable > tbody > tr:nth-child(2) > td > textarea').eq(4).type('Test',{force:true})
            cy.get('#notestable > tbody > tr:nth-child(3) > td > textarea').eq(4).type('Test',{force:true})

            //SAVE BUTTON
            cy.get('.btn__success').click() //clicking save button
            cy.wait(10000)
    
        })

        it.only('Automating Entry Form of Short Term and Long Term Goals Tab', function() {
            cy.wait(10000)
            cy.get('.btn__success').click() //clicking save button
            cy.get('.swal2-cancel').click() //clicking continue
            cy.wait(10000)
            cy.get('.bg__wt > :nth-child(1) > .btn-group > :nth-child(2)').click({force:true}) //clicking Short Term and Long Term Goals Tab
            cy.wait(5000)

            //Add Blank (Mulitple Click)
            //Range of Motion - Muscle Strength - Bed Mobility - Transfer - ADL -Balance
            cy.get('#notestable > tbody > tr:nth-child(3) > td > button').click({multiple:true}) //clicking add blank row

            
            //Range of Motion
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(1) > div.pull-right > textarea').eq(0).type('Test') //inputting Problems Identified
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(0).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(0).type('Test') //inputting Long Term Goals
            //Muscle Strength
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(1) > div.pull-right > textarea').eq(1).type('Test') //inputting Problems Identified
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(1).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(1).type('Test') //inputting Long Term Goals
            //Bed Mobility
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(1) > div.pull-right > textarea').eq(2).type('Test') //inputting Problems Identified
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(2).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(2).type('Test') //inputting Long Term Goals
            //Transfer
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(1) > div.pull-right > textarea').eq(3).type('Test') //inputting Problems Identified
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(3).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(3).type('Test') //inputting Long Term Goals
            //ADL
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(1) > div.pull-right > textarea').eq(4).type('Test') //inputting Problems Identified
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(4).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(4).type('Test') //inputting Long Term Goals
            //Balance
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(1) > div.pull-right > textarea').eq(5).type('Test') //inputting Problems Identified
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(5).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(5).type('Test') //inputting Long Term Goals
            cy.wait(5000)
            //Pain
            //Column 1
            //Short Term Goals
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(2) > p > input:nth-child(1)').type('3') //inputting week/s
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(2) > p > input:nth-child(2)').type('3') //inputting level
            //Long Term Goals
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(3) > p > input:nth-child(1)').type('3') //inputting week/s
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(3) > p > input:nth-child(2)').type('3') //inputting level
            //Column 2
            //Short Term Goals
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(2) > p > input:nth-child(1)').type('3') //inputting week/s
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(2) > p > input:nth-child(2)').type('3') //inputting level
            //Long Term Goals
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(3) > p > input:nth-child(1)').type('3') //inputting week/s
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(3) > p > input:nth-child(2)').type('3') //inputting level
            //Assistive Device
            //Column 1
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(2) > textarea').eq(6).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr:nth-child(3) > td:nth-child(3) > textarea').eq(6).type('Test') //inputting Long Term Goals
            //Column 2
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(2) > textarea').type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr:nth-child(4) > td:nth-child(3) > textarea').type('Test') //inputting Long Term Goals
            //Safety
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(8).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(8).type('Test') //inputting Long Term Goals
            //Knowledge Deficit
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(9).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(9).type('Test') //inputting Long Term Goals
            //Other Problems
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(2) > textarea').eq(10).type('Test') //inputting Short Term Goals
            cy.get('#notestable > tbody > tr.ng-scope > td:nth-child(3) > textarea').eq(10).type('Test') //inputting Long Term Goals
            
            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button


        })

    })