Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />

describe('Wound Management Test Case/Scenarios', () => {
           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
            cy.viewport(1920, 924);//setting your windows size
            
        })
        
            it('Adding Patient', function() {     
    
                var bdate = "01011955"
    
                //getting the date now
                const dayjs = require('dayjs')
                //cy.log(dayjs().format('MM/DD/YYYY'));  //Prints todays date
                
                //random lastname
                var lname = ['Messi', 'Ronaldo', 'Chuck', 'Benzama', 'Benzon', 'John', 'James']
                var x = Math.floor((Math.random() *6)) +1;
                var lastname = lname[x];
                
                
                //Adding of Patient or Pre Admission form
    
                cy.visit('https://qado.medisource.com/patient');
                cy.wait(5000);
                //Patient Information section
                cy.get('.mhc__btn-neutral').click();
                cy.get('#refDate').type(dayjs().add(1, 'day').format('MM/DD/YYYY')) ;//dd/mm/yyy
                cy.wait(5000)
                cy.get(':nth-child(2) > [colspan="2"] > table > tbody > tr > .global__table-question > .checkbox > label > .ng-pristine').click();
                cy.get('#pre_admission_date').type(dayjs().format('MM/DD/YYYY'));
                cy.get('#last_name').type('WoundAutomated123');
                cy.get('#first_name').type('Test');
                cy.get('#mi').type('mi');
                cy.get('#birthdate').type('12121955');
                cy.get('.custom-input-radio > :nth-child(1) > .ng-pristine').check();
                cy.get('#marital_status_chosen > .chosen-single').click();
                cy.get('#marital_status_chosen > div > ul > li:nth-child(1)').click();
                cy.get('#ethnicities_chosen > .chosen-choices > .search-field > .default').click();
                cy.get('#ethnicities_chosen > div > ul > li:nth-child(1)').click();
    
                //Patient Address section
                cy.get('#main_line1').type("1752 Wilson Ave");
                cy.get('#main_street').type("Arcadia");
                cy.get('#main_city').type("Arcadia");
                cy.wait(5000);
                cy.get('#main_state_chosen > a').click();
                cy.get('#main_state_chosen > div > ul > li:nth-child(6)').click();
                cy.get('#main_zipcode').type("91008");
                cy.get(':nth-child(22) > .oasis__answer > .fg-line > .global__txtbox').type("3216549871");
                cy.get(':nth-child(23) > .oasis__answer > .fg-line > .global__txtbox').type("3216549871");
                cy.get('#main_email').type("arc@mailinator.com");
    
                //Service Delivery Addres Section
                cy.get(':nth-child(25) > [colspan="2"] > .btn__dark-blue_icon-bordered').click();//clicking Same as Patient Address button
    
                //Living Situation section
                cy.get(':nth-child(35) > .oasis__answer > .custom-input-radio > .checked > .ng-valid').click();
                cy.get(':nth-child(36) > .oasis__answer > .custom-input-radio > .checked > .ng-valid').click();
                cy.get('#ls_caregiver').type('James Nanny');
                cy.get('#ls_caregiver_phone').type('3258459612');
    
                //Emergency Contacts
                cy.get('#ec_name').type('James Nanny');
                cy.get('#ec_relationship').type('Nanny');
                cy.get('#ec_phone').type(1323445463);
                cy.get('#ec_other_phone').type(6574323456);
    
                //Physician Information Section
                cy.get('#physician_attending_chosen > .chosen-single').click();
                cy.get('#physician_attending_chosen > div > ul > li:nth-child(2)').click();
                cy.get('#physician_primary_chosen > .chosen-single').click();
                cy.get('#physician_primary_chosen > div > ul > li:nth-child(2)').click();
                cy.get('[style="height: unset;"] > .select > .chosen-container > .chosen-choices').click();//other physician
                cy.get('#content > data > div.container.p-15.ng-scope.global__form_center-false > div > ng-form > fieldset > table > tbody > tr:nth-child(46) > td:nth-child(2) > div > div > div > ul > li:nth-child(2)').click();
    
                //Insurance Information Section
                cy.wait(5000)
                cy.get(':nth-child(48) > .global__table-question').scrollIntoView().should('be.visible');
                cy.get('#primary_insurance_chosen > .chosen-single').click();
                cy.get('#primary_insurance_chosen > div > ul > li:nth-child(2)').click();
                cy.get('#primary_insurance_policy').type('qwerty45678');
                cy.get('#secondary_insurance_chosen > .chosen-single').click();
                cy.get('#secondary_insurance_chosen > div > ul > li:nth-child(2)').click();
                cy.get('#secondary_insurance_policy').type('qwerty1234');
                
                //Referral Information Section
                cy.get('#admissiontype_chosen > .chosen-single').click();
                cy.get('#admissiontype_chosen > div > ul > li:nth-child(1)').click();
                cy.get('#point_of_origin_chosen > .chosen-single').click();
                cy.get('#point_of_origin_chosen > div > ul > li:nth-child(1)').click();
                cy.get('#referral_type_chosen > .chosen-single').click();
                cy.get('#referral_type_chosen > div > ul > li:nth-child(3)').click();
                cy.get('#referral_source_id_chosen > .chosen-single').click();
                cy.get('#referral_source_id_chosen > div > ul > li:nth-child(2)').click();
    
                //Hospitalization Information Section
                cy.get('#hospital_id_chosen > .chosen-single').click();
                cy.get('#hospital_id_chosen  > div > ul > li:nth-child(2)').click();
                cy.get('#admit_date').type('12122021');
                cy.get('#discharge_date').type('12122021');
    
                //Diagnosis & Pre admission Section
                cy.get('#diagnosis_surgery').type('heart failure');
                cy.get('#diagnosis_allergies > .host > .tags > .ng-pristine').type('peanut{enter}');
    
                //M0080 section
                cy.get(':nth-child(68) > .oasis__answer > .fg-line > :nth-child(1) > .ng-pristine').click();
    
                //Clinical Staff Section
                cy.get('#cs_rn_chosen > .chosen-single').click();
                cy.get('#cs_rn_chosen > div > ul > li:nth-child(1)').click();//selected REGISTERED NURSE
                cy.get('#cs_cm_chosen > .chosen-single').click();
                cy.get('#cs_cm_chosen > div > ul > li:nth-child(1)').click();//selected CASE MANAGER
    
                //SAVE THE RECORD
                cy.get('.btn__success').click();
                cy.wait(10000)
    
            });
           
            it('W001 - Create Functionality', function()
            {            
                cy.visit('https://qado.medisource.com/patient/admitted');
                 const dayjs = require('dayjs')
    
                    cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
                    cy.wait(5000) 
                    cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
                    cy.wait(5000) 
                    cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
                    cy.wait(10000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
                    cy.wait(5000)
                    
                    //Click the OASIS
                    cy.get('.sampletd > :nth-child(2) > .ng-binding').click()
                    //click EDIT button
                    cy.get('.btn__warning').click()
                    //add time in/out
                    cy.get('#ti').type('1000');
                    cy.get('#to').type('1100');

                    //***add required field on Demographics tab**//
                    //click ssn UK
                    cy.get('[mos-name="M0064"] > .oasis__answer > .inner__table-nb > tbody > tr > :nth-child(2) > .m-0 > .checkbox > .ng-pristine').click()
                    cy.get('#M0090_INFO_COMPLETED_DT').type(dayjs().format('MM/DD/YYYY'));//M0090 entry
                    cy.get('#M0102_PHYSN_ORDRD_SOCROC_DT_NA > .ng-pristine').click(); //M0102 NA
                    cy.get('#M0104_PHYSN_RFRL_DT').type(dayjs().format('MM/DD/YYYY'));//M0104 entry
                    cy.get('.oasis-w-30 > .ng-isolate-scope > :nth-child(1) > .radio > .ng-pristine').click(); //M0110 Early
                    cy.get('#M0140_ETHNIC_AI_AN > .ng-pristine').click()//M0140 - 1
                    cy.get('#M0150_CPAY_MCARE_FFS > .ng-pristine').click()//M0150 - 1
                    cy.get('#M1000_DC_LTC_14_DA > .ng-pristine').click()//M1000 - 1
                    //click Integumentary tab
                    cy.get(':nth-child(4) > .oasis-section-tabs').click()
                    cy.wait(15000)

                    // Check "Wound" under Skin Integrity and Save 
                    cy.get(':nth-child(4) > .m-0 > .checkbox > .ng-valid').click()
                    cy.get('.btn__success').click()
                    cy.wait(5000)
                    cy.get('.swal2-modal').contains('Assess Later').click();
           
             });

            it('W002 - Adding of Wound functionality - Wound 1', function() 
            {     
                cy.visit('https://qado.medisource.com/patient/admitted');
                 //const dayjs = require('dayjs')
                 const imgFile = 'wound.png'; // This is the file path of the image which is located in the Fixtures folder
    
                    cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
                    cy.wait(5000) 
                    cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
                    cy.wait(5000) 
                    cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
                    cy.wait(10000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
                    cy.wait(15000)
                    /*
                    //Click the OASIS
                    cy.get('.sampletd > :nth-child(2) > .ng-binding').click()
                    //click EDIT button
                    cy.get('.btn__warning').click()
                    //click Integumentary tab
                    cy.get(':nth-child(4) > .oasis-section-tabs').click()
                    cy.wait(15000)

                    // Click the + icon
                    cy.get(':nth-child(4) > .m-0 > .btn > .zmdi').click()
                    cy.wait(5000)
                    //fill-out Wound, Location and Comment
                    cy.get('[style="width: 16%;"] > .input-drp > .fg-line > .global__txtbox').click();
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(3) > div > div.opt-list > ul > li:nth-child(2)').click()
                    cy.wait(5000)
                    cy.get('[style="width: 22%;"] > .input-drp > .fg-line > .global__txtbox').click();
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(5) > div > div.opt-list > ul > li:nth-child(2)').click()
                    cy.wait(5000)
                    cy.get(':nth-child(7) > .global__txtbox').type('Lorem Ipsum');
                    cy.get('.btn__success').click()
                    cy.wait(15000)
                    cy.reload();
                    cy.wait(15000)*/

                    //go to wound management list and select the oasis wound reference
                    cy.get('#profile-main-header > div > ul > li:nth-child(8) > a').click();//click wound mgmt tab
                    cy.wait(8000)
                    cy.get('.sampletd > :nth-child(2)').click();//click oasis wound reference or the first onthe list
                    cy.wait(8000)
                    cy.get('tr > :nth-child(2) > .btn').click();//click EDIT button
                    cy.wait(8000)
                    
                    // PIN WOUND 1 -------------
                    cy.get('#dragball > div').click(374, 251);//click on human image or pin wound
                    cy.wait(8000)
                    cy.get('.mhc__wound > .mhc__btn-affirmative').click();//click yes
                    cy.wait(5000)
                    //click location dropdown
                    cy.get('.li-location > .labeled-height > fieldset > table-inputs-v4.ng-pristine > .input-drp > .fg-line > .global__txtbox').click();
                    //select location
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div > div > table-column-v4 > ul > li.li-height-0.li-cont.li-location > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click();
                    //click wound type dropdown
                    //cy.get('[drop-down-select-v4="wcData"][style=""] > .wc-assessment-column > table-column-v4.ng-scope > .wc-ul-cont > .li-woundtype > .labeled-height > fieldset > table-inputs-v4.ng-pristine > .input-drp > .fg-line > .global__txtbox').click();
                    cy.get('.li-woundtype > .labeled-height > fieldset > table-inputs-v4.ng-pristine > .input-drp > .fg-line > .global__txtbox').click()
                    //select wound type
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div > div > table-column-v4 > ul > li.li-height-0.li-cont.li-woundtype > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click()
                    cy.scrollTo(0, 1500)
                    cy.wait(10000)
                    //upload file
                    cy.get('#myImg').attachFile(imgFile , { subjectType: 'drag-n-drop' }) //Drop and Drop uploading of an image
                    cy.wait(10000)
                    cy.scrollTo(1500, 0)
                    cy.wait(5000)
                    cy.get('#tdTitleAction > td:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click({ multiple: true }) //clicking save button
                    
                    // PIN WOUND 1 -------------
            });

            it(' W003 - Tracing Wound - Wound 1', function()
            {
                cy.visit('https://qado.medisource.com/patient/admitted');
                    const imgFile = 'wound.png';
                    cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
                    cy.wait(5000) 
                    cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
                    cy.wait(5000)
                    cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
                    cy.wait(10000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
                    cy.wait(15000)
                    cy.get('#profile-main-header > div > ul > li:nth-child(8) > a').click();//click wound mgmt tab
                    cy.wait(5000)
                    cy.get('#table-responsive > table > tbody.txt__bk.ng-isolate-scope > tr > td:nth-child(2) > span').click() //click wound
                    cy.get('#titleNoteBar > tbody > tr > td:nth-child(2) > button').click() //clicking edit button
                    cy.wait(10000)
                    cy.scrollTo(0, 1500)
                    cy.get('#onProcessFalse-1-400ED9BB-D030-46DE-9D4D-3ADCEE3FC468 > div.wc-img_custom.ng-scope > div:nth-child(2) > div:nth-child(2) > span > div > div > a').click() //clicking delete image
                    cy.get('body > div.swal2-container > div.swal2-modal.show-swal2 > button.swal2-confirm.styled').click() //clicking yes to delete
                    cy.wait(5000)
                    //upload file
                    cy.get('#myImg').attachFile(imgFile , { subjectType: 'drag-n-drop' }) //Drop and Drop uploading of an image
                    cy.wait(10000)
                    
                    //WOUND NUMBER 1 ----------------
                    //Tracing Wound
                    cy.get('#onProcessFalse-1-400ED9BB-D030-46DE-9D4D-3ADCEE3FC468 > div.wc-img_custom.ng-scope > div:nth-child(2) > div:nth-child(3) > span > div > div > a').click() //clicking digital measurement
                    cy.wait(20000)
                    cy.get('#onProcessFalse-1-82CB3BD6-389B-4571-829B-F09C176D5C13 > div.wc-img_custom.ng-scope > div:nth-child(2) > div:nth-child(3) > span > div > div > a').click() //clicking edit digital measurement
                    cy.wait(10000)
                    //Wound Digital Meaurement
                    cy.get('#canvas')
                    .click(480, 155, { force: true })
                    .click(580, 152, { force: true })
                    .click(680, 175, { force: true })
                    .click(780, 235, { force: true })
                    .click(815, 310, { force: true })
                    .click(820, 340, { force: true })
                    .click(780, 420, { force: true })
                    .click(700, 480, { force: true })
                    .click(620, 525, { force: true })
                    .click(540, 550, { force: true })
                    .click(480, 560, { force: true })
                    .click(420, 555, { force: true })
                    .click(320, 545, { force: true })
                    .click(220, 500, { force: true })
                    .click(160, 460, { force: true })
                    .click(138, 400, { force: true })
                    .click(135, 370, { force: true })
                    .click(140, 330, { force: true })
                    .click(150, 300, { force: true })
                    .click(190, 260, { force: true })
                    .click(250, 220, { force: true })
                    .click(310, 190, { force: true })
                    .click(360, 170, { force: true })
                    .click(480, 155, { force: true })
                    
                    cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form > div > div.modal-body.p-10 > div > div > table > tbody > tr > td:nth-child(2) > div.text-right.p-0.m-l-50.m-t-10 > button.btn.mhc__btn-affirmative.btn__success.waves-effect').click() //clicking save button on wound digital measurements
                    //WOUND NUMBER 1 ----------------
            });

            it.only('W004 - Adding of Wound functionality - Wound 2', function() 
            {     
                
                cy.visit('https://qado.medisource.com/patient/admitted');
                cy.wait(10000)
                 //const dayjs = require('dayjs')
                 const imgFile = 'wound1.png'; // This is the file path of the image which is located in the Fixtures folder
    
                    cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
                    cy.wait(5000) 
                    cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
                    cy.wait(5000) 
                    cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
                    cy.wait(10000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
                    cy.wait(15000)
                    /*
                    //Click the OASIS
                    cy.get('.sampletd > :nth-child(2) > .ng-binding').click()
                    //click EDIT button
                    cy.get('.btn__warning').click()
                    //click Integumentary tab
                    cy.get(':nth-child(4) > .oasis-section-tabs').click()
                    cy.wait(15000)

                    // Click the + icon
                    cy.get(':nth-child(4) > .m-0 > .btn > .zmdi').click()
                    cy.wait(5000)
                    //fill-out Wound, Location and Comment
                    cy.get('[style="width: 16%;"] > .input-drp > .fg-line > .global__txtbox').click();
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(3) > div > div.opt-list > ul > li:nth-child(2)').click()
                    cy.wait(5000)
                    cy.get('[style="width: 22%;"] > .input-drp > .fg-line > .global__txtbox').click();
                    cy.wait(5000)
                    cy.get('#parent > div > div > div > form > div > fieldset > div.oasis-cont > fieldset > div > table:nth-child(1) > tbody > tr.ng-scope.ng-isolate-scope > td > table > tbody > tr > td:nth-child(5) > div > div.opt-list > ul > li:nth-child(2)').click()
                    cy.wait(5000)
                    cy.get(':nth-child(7) > .global__txtbox').type('Lorem Ipsum');
                    cy.get('.btn__success').click()
                    cy.wait(15000)
                    cy.reload();
                    cy.wait(15000)*/

                    //go to wound management list and select the oasis wound reference
                    cy.get('#profile-main-header > div > ul > li:nth-child(8) > a').click();//click wound mgmt tab
                    cy.wait(8000)
                    cy.get('.sampletd > :nth-child(2)').click();//click oasis wound reference or the first onthe list
                    cy.wait(8000)
                    cy.get('tr > :nth-child(2) > .btn').click();//click EDIT button
                    cy.wait(8000)

                    // PIN WOUND 2 -------------
                    cy.get('#dragball > div').click(550, 251, { force: true });//click on human image or pin wound
                    cy.get('.mhc__wound > .mhc__btn-affirmative').click({ force: true });//click yes
                    cy.wait(5000)
                    cy.scrollTo(0, 1000)
                    //click location dropdown
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-location > span > fieldset > table-inputs-v4 > div > div.fg-line > input').click({ force: true });
                    //select location
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-location > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(3)').click({ force: true });
                    //click wound type dropdown
                    //cy.get('[drop-down-select-v4="wcData"][style=""] > .wc-assessment-column > table-column-v4.ng-scope > .wc-ul-cont > .li-woundtype > .labeled-height > fieldset > table-inputs-v4.ng-pristine > .input-drp > .fg-line > .global__txtbox').click();
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-woundtype > span > fieldset > table-inputs-v4 > div > div.fg-line > input').click({ force: true })
                    //select wound type
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-0.li-cont.li-woundtype > span > fieldset > table-inputs-v4 > div > div.opt-list > ul > li:nth-child(2)').click({ force: true })
                    cy.scrollTo(0, 1500)
                    cy.wait(10000)
                    // cy.get('#myImg').should('not.be.visible')
                    // cy.get('#myImg').attachFile(imgFile , { subjectType: 'drag-n-drop' }, {force: true}) //Drop and Drop uploading of an image
                    // cy.wait(10000)

                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-1.li-cont.li-img-block.li-photo > table-column-photo-v4')
                    .invoke('show') // call jquery method 'show' on the '#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-1.li-cont.li-img-block.li-photo > table-column-photo-v4'
                    .should('be.visible') // element is visible now
                    .find('#myImg') // drill down into a child "img" element
                    .attachFile(imgFile , { subjectType: 'drag-n-drop' }, {force: true}) //Drop and Drop uploading of an image // drill down into a child "input" element
                    cy.wait(10000)

                    cy.scrollTo(1500, 0)
                    cy.wait(5000)
                    cy.get('#tdTitleAction > td:nth-child(2) > button.btn__success.m-l-10.waves-effect.ng-scope').click({ multiple: true }) //clicking save button

                    // PIN WOUND 2 -------------
            });

            it.only(' W005 - Tracing Wound - Wound 2', function()
            {
                cy.visit('https://qado.medisource.com/patient/admitted');
                    const imgFile = 'wound1.png';
                    cy.get(':nth-child(2) > .expanded__item > .ng-binding').click()//click patient manager
                    cy.wait(5000) 
                    cy.get('.activenav > .expanded__subnav > :nth-child(1) > .expanded__subitem').click()//click patient list
                    cy.wait(5000)
                    cy.get('.searchbar__content > .ng-pristine').type('WoundAutomated123')  //search the added patient 
                    cy.wait(10000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope').click()                       
                    cy.wait(15000)
                    cy.get('#profile-main-header > div > ul > li:nth-child(8) > a').click();//click wound mgmt tab
                    cy.wait(5000)
                    cy.get('#table-responsive > table > tbody.txt__bk.ng-isolate-scope > tr > td:nth-child(2) > span').click() //click wound
                    cy.get('#titleNoteBar > tbody > tr > td:nth-child(2) > button').click() //clicking edit button
                    cy.get('.text-center.w-100 > .btn-group > :nth-child(2)').click() //clicking wound #2
                    cy.wait(10000)
                    cy.scrollTo(0, 1500)
                    cy.get('#onProcessFalse-2-13E33126-74CB-4640-BEAD-D409CF1766F1 > .wc-img_custom > [ng-show="!wc.invalidImgBase64"] > [style="display: flex;justify-content: center; left: 1.5%; top: 130%;"] > .col-sm-12 > .p-l-0 > .text-center > .m-l-0').click() //clicking delete image
                    cy.get('.swal2-confirm').click() //clicking yes to delete
                    cy.wait(5000)
                    //upload file
                    cy.get('#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-1.li-cont.li-img-block.li-photo > table-column-photo-v4')
                    .invoke('show') // call jquery method 'show' on the '#goTabSection > div > wound-table-v4 > div.wc-container.ng-scope.ng-isolate-scope.margLeftHist > div.wc-assessment-cont.ng-scope.wc-assessment-contCus > div:nth-child(2) > div > table-column-v4 > ul > li.li-height-1.li-cont.li-img-block.li-photo > table-column-photo-v4'
                    .should('be.visible') // element is visible now
                    .find('#myImg') // drill down into a child "img" element
                    .attachFile(imgFile , { subjectType: 'drag-n-drop' }, {force: true}) //Drop and Drop uploading of an image // drill down into a child "input" element
                    cy.wait(10000)
                    
                    // //WOUND NUMBER 2 ----------------
                    // //Tracing Wound
                    // cy.get('#onProcessFalse-1-400ED9BB-D030-46DE-9D4D-3ADCEE3FC468 > div.wc-img_custom.ng-scope > div:nth-child(2) > div:nth-child(3) > span > div > div > a').click() //clicking digital measurement
                    // cy.wait(20000)
                    // cy.get('#onProcessFalse-1-82CB3BD6-389B-4571-829B-F09C176D5C13 > div.wc-img_custom.ng-scope > div:nth-child(2) > div:nth-child(3) > span > div > div > a').click() //clicking edit digital measurement
                    // cy.wait(10000)
                    // //Wound Digital Meaurement
                    // cy.get('#canvas')
                    // .click(480, 155, { force: true })
                    // .click(580, 152, { force: true })
                    // .click(680, 175, { force: true })
                    // .click(780, 235, { force: true })
                    // .click(815, 310, { force: true })
                    // .click(820, 340, { force: true })
                    // .click(780, 420, { force: true })
                    // .click(700, 480, { force: true })
                    // .click(620, 525, { force: true })
                    // .click(540, 550, { force: true })
                    // .click(480, 560, { force: true })
                    // .click(420, 555, { force: true })
                    // .click(320, 545, { force: true })
                    // .click(220, 500, { force: true })
                    // .click(160, 460, { force: true })
                    // .click(138, 400, { force: true })
                    // .click(135, 370, { force: true })
                    // .click(140, 330, { force: true })
                    // .click(150, 300, { force: true })
                    // .click(190, 260, { force: true })
                    // .click(250, 220, { force: true })
                    // .click(310, 190, { force: true })
                    // .click(360, 170, { force: true })
                    // .click(480, 155, { force: true })
                    
                    // cy.get('body > div.modal.fade.ng-isolate-scope.in > div > div > form > div > div.modal-body.p-10 > div > div > table > tbody > tr > td:nth-child(2) > div.text-right.p-0.m-l-50.m-t-10 > button.btn.mhc__btn-affirmative.btn__success.waves-effect').click() //clicking save button on wound digital measurements
                    //WOUND NUMBER 2 ----------------
            });
                
    });