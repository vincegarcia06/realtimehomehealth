Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of Care Summary', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Will M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                
                    cy.get(':nth-child(4) > .navtitle').click();//Communication Note
                    cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
                    
        })
           
        it('Automating Entry Form of 30-Day Care Summary', function() {
            cy.get('[ui-sref="patientcare.careSummary30daysCreate"]').click() //clicking 30-day care summary
            cy.wait(10000)
            cy.get('#date').type('05302022')
            cy.get('.tags > .ng-pristine').type('Test') //inputting other diagnoses

            //SCIC/Change in Diagnosis/Events/Physician Orders
            cy.get('[rname="events"]').click({multiple:true})
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .row > .static > .fg-line > .global__txtbox').type('Test') //inputting other
            cy.get('[style="width: 92%"] > .global__txtbox').type('Test') //inputting comment
            //VS Monitor
            cy.get(':nth-child(4) > tbody > :nth-child(5) > td > [style="width: 90%"] > .form-control').type('Test') //inputting comment
            //Medication Management
            cy.get(':nth-child(5) > :nth-child(2) > :nth-child(1) > .ng-valid').click() //clicking yes Patient compliant?
            cy.get(':nth-child(2) > :nth-child(3) > .ng-pristine').click() //clicking yes Patient adherent? 
            //Laboratory/Diagnostic Tests
            cy.get(':nth-child(6) > :nth-child(2) > .form-control').type('Test') //inputting comment
            //Procedures
            cy.get(':nth-child(2) > td > [style="width: 90%"] > .form-control').type('Test') //inputting comment
            //Assessment Summary
            cy.get('[width="67%"] > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Sensory Status
            cy.get(':nth-child(8) > tbody > :nth-child(3) > :nth-child(4)').type('Test') //inputting comment Integumentary
            cy.get(':nth-child(4) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Endocrine
            cy.get(':nth-child(5) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Respiratory
            cy.get(':nth-child(6) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Cardiovascular
            cy.get(':nth-child(7) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Upper GI
            cy.get(':nth-child(8) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Lower GI
            cy.get(':nth-child(9) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Genitourinary
            cy.get(':nth-child(10) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Neurologic
            cy.get(':nth-child(11) > :nth-child(4) > [style="width: 88%"] > .global__txtbox').type('Test') //inputting comment Musculoskeletal
            cy.get('[colspan="3"] > .form-control').type('Test') //inputting comment
            cy.wait(5000)
            cy.get(':nth-child(9) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Reasons for Home health
            cy.get(':nth-child(10) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Homebound Status
            cy.get(':nth-child(11) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Caregiving Status
            //Physician Orders for Disciplines and Ancillary Referrals
            cy.get(':nth-child(12) > tbody > :nth-child(5) > td > [style="width: 90%"] > .form-control').type('Test') //inputting comment
            cy.get(':nth-child(13) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Care Summary
            cy.get(':nth-child(14) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Progress towards Goals
            cy.get(':nth-child(15) > tbody > :nth-child(2) > td > .form-control').type('Test') //inputting Recommendations
            cy.wait(5000)
            cy.get('#conferencereviewers_chosen > ul').click() //clicking dropdown Team Conference Reviewers
            cy.get('#conferencereviewers_chosen > div > ul > li:nth-child(3)').click() //clicking result

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button

        })

        it('Automating Entry Form of Transfer Summary', function() {
            cy.get('[ui-sref="patientcare.careSummaryTransferCreate"]').click() //clicking Transfer Summary
            cy.wait(10000)
            cy.get('#transfer_date').type('05302022') //inputting transfer date
            //Physician
            cy.get(':nth-child(2) > :nth-child(1) > .select > .chosen-container > .chosen-single').click() //clicking dropdown physician
            cy.get('#CommTransferSummary > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table.global__table.b-0 > tbody > tr:nth-child(2) > td:nth-child(1) > div > div > div > ul > li:nth-child(1)').click() //clicking result
            //Reasons for Home Health (Mark all that apply)
            cy.get('[rname="admission_reasons"]').click({multiple:true})
            cy.wait(5000)
            cy.get('.form-group > .fg-line > .form-control').type('Test') //inputting Additional reasons for home health
            //Transfer to (click all)
            cy.get('[rname="transfer_to"]').click({multiple:true})
            cy.get(':nth-child(3) > .global__txtbox').type('Test') //inputting other
            //Reason for Transfer (click all)
            cy.get('[rname="transfer_reasons"]').click({multiple:true})
            cy.get(':nth-child(3) > td > .global__txtbox').type('Test') //inputting other
            //Current Physical, Mental and Emotional Status (click all)
            cy.get('[rname="statuses"]').click({multiple:true})
            cy.get('.col-sm-9 > .fg-line > .global__txtbox').type('Test') //inputting other
            cy.wait(5000)
            //Current Functional Status
            //ADL
            cy.get(':nth-child(2) > .oasis__answer > .w-100 > tbody > :nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-valid').click() //clicking independent
            //Mobility
            cy.get(':nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-pristine').click() //clicking independent
            //Assistive Device: (click all)
            cy.get('[rname="assistive_devices"]').click({multiple:true})
            cy.get(':nth-child(2) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
            cy.get('.col-xs-12 > .fg-line > .form-control').type('Test') //inputting Summary of Care Provided
            //Disciplines/visits provided
            cy.get('[rname="disciplines"]').click({multiple:true})
            cy.get(':nth-child(10) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
            //Transfer Notification Date
            cy.get('#notification_date_physician').type('05302022') //inputting date physician
            cy.get('#notification_date_patient').type('05302022') //inputting date Patient/Family/Caregiver
            //Send to MD via	
            cy.get('.table.m-b-15 > tbody > :nth-child(2) > :nth-child(4) > div > :nth-child(1) > .ng-pristine').click() //clicking fax

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button

        })

        it('Automating Entry Form of Discharge Summary', function() {
            cy.get('[ui-sref="patientcare.careSummaryDischargeCreate()"]').click() //clicking Discharge Summary
            cy.wait(10000)
            cy.get('#dischargeDate').type('05302022') //inputting discharge date
            cy.get('.m-b-10 > .form-control').type('Test') //inputting primary diagnoses
            cy.get(':nth-child(2) > .form-control').type('Test') //inputting secondary diagnoses
            //Reason(s) for Home Health Admission (click all)
            cy.get('[rname="reasAdm"]').click({multiple:true})
            cy.get(':nth-child(2) > .fg-line > .form-control').type('Test') //inputting other reason
            //Reason(s) for Discharge (click all)
            cy.get('[rname="reasonDisList"]').click({multiple:true})
            cy.get('.col-xs-6 > .d-ib > .global__txtbox').type('Test') //inputting other reason
            //Current Physical, Mental and Emotional Status (click all)
            cy.get('[rname="physMenEmList"]').click({multiple:true})
            cy.get('.col-sm-9 > .fg-line > .global__txtbox').type('Test') //inputting other reason
            //Current Functional Status
            //ADL
            cy.get(':nth-child(2) > .oasis__answer > .w-100 > tbody > :nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-valid').click() //clicking independent
            //Mobility
            cy.get(':nth-child(3) > .oasis__answer > .w-100 > tbody > :nth-child(1) > td > .row > :nth-child(1) > .radio > .ng-valid').click() //clicking independent
            //Assistive Device: (click all)
            cy.get('[rname="assisDevList"]').click({multiple:true})
            cy.get(':nth-child(5) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
            cy.get('.m-t-0 > .ng-valid').click() //clicking 1 for Discharge Disposition
            cy.get('.oasis__answer > .col-xs-12 > .fg-line > .form-control').type('Test') //inputting Summary of Care Provided
            //Disciplines/visits provided
            cy.get('[rname="invDiscList"]').click({multiple:true})
            cy.get(':nth-child(8) > tbody > tr > .oasis__answer > div.d-ib > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(9) > tbody > tr > .oasis__answer > div > :nth-child(1) > .ng-pristine').click() //clicking goals met for Treatment Goals Achieved
            //Discharge Notification Date
            cy.get('#physicianDischargeDate').type('05302022') //inputting date physician
            cy.get('#patFamCare').type('05302022') //inputting date Patient/Family/Caregiver
            //Send to MD via	
            cy.get(':nth-child(10) > tbody > :nth-child(2) > :nth-child(4) > div > :nth-child(1) > .ng-pristine').click() //clicking fax

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button

        })

        it('Automating Entry Form of Discharge Instruction', function() {
            cy.get('[ui-sref="patientcare.careSummaryDischargeInsCreate"]').click() //clicking Discharge Instruction
            cy.wait(10000)
            cy.get('#dischargeDate').type('05302022') //inputting discharge date
            //Physician
            cy.get(':nth-child(2) > .select > .chosen-container > .chosen-single').click() //clicking dropdown physician
            cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > ul > li:nth-child(1)').click() //clicking result
            //We are pleased to have provided service to you. The following discharge instructions were reviewed with you and/or your caregiver/s during the final visit by agency staff. You are to:
            //Keep your scheduled appointment with Dr.
            cy.get(':nth-child(2) > .oasis__answer > [style="min-width:250px; margin: 6px;"] > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown doctor
            cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table.table.global__table.m-t-15.ng-scope > tbody > tr:nth-child(2) > td > span:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result doctor
            cy.get('#appointmentDate').type('05302022') //inputting date appointment
            //Keep your scheduled appointment with Dr.
            cy.get(':nth-child(3) > .oasis__answer > [style="min-width:250px; margin: 6px;"] > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown doctor
            cy.get('#parent > div > form > div > div.table-responsive.max__width_wrapper > fieldset > table > tbody > tr > td > table.table.global__table.m-t-15.ng-scope > tbody > tr:nth-child(3) > td > span:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result doctor
            cy.get('#appointmentDateTwo').type('05302022') //inputting date appointment
            cy.wait(5000)
            cy.get('.checkbox > .m-r-10 > .ng-valid').click() //clicking Continue to take the medications as prescribed by your physician.
            cy.get('[chk="data.medList"] > .ng-valid').click() //clicking Medication list attached
            cy.get(':nth-child(7) > .oasis__answer > .tblChk > .fg-line > .form-control').type('Test') //inputting Additional medication instructions:
            cy.wait(5000)
            //Follow the diet as prescribed by your Physician and instructed by your nurse/dietician.
            cy.get('.tblChk > .cont-opt > .fg-line > .global__txtbox').type('Test') //inputting diet
            //Continue with the home program as instructed by therapist/nurse. Refer to the handouts provided.
            cy.get(':nth-child(11) > .oasis__answer > .tblChk > .fg-line > .form-control').type('Test') //inputting additional instruction
            //Continue with the skin/wound care as ordered by your physician and instructed by your nurse.
            cy.get(':nth-child(13) > .oasis__answer > .tblChk > .fg-line > .form-control').type('Test') //inputting wound care instruction
            //Follow through with community resource or organization to which you been referred.
            cy.get('.oasis__answer > .cont-opt > .fg-line > .global__txtbox').type('Test')
            //Other instructions:
            cy.get('.oasis__answer > .fg-line > .form-control').type('Test')

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button

        })

        it('Automating Entry Form of Case Conference', function() {
            cy.get('[ui-sref="patientcare.careSummaryCaseConferenceCreate"]').click() //clicking Case Conference
            cy.wait(10000)
            cy.get('#order_date').type('05302022') //inputting date created
            cy.get('.b-r-n > fieldset > .form-control').type('Test') //inputting home health reason
            cy.get('.oasis__answer > fieldset > .form-control').type('Test') //inputting Purpose of the Conference
            cy.get('[style="float:left;"] > .checkbox > .ng-valid').click() //clicking Assessment of patient status and current needs by:
            cy.get('[style="margin-left:330px;"] > .global__txtbox').type('Test') //inputting Assessment of patient status and current needs by:
            cy.get('.ng-scope > .oasis__answer > fieldset > .form-control').type('Test')
            cy.get('.global__table-section > .checkbox > .ng-valid').click() //clicking Plan of actions to be taken, by whom and timeframe
            //Plan of actions to be taken, by whom and timeframe
            //Plan of actions to be taken
            cy.get(':nth-child(2) > :nth-child(1) > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(3) > :nth-child(1) > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(4) > :nth-child(1) > .fg-line > .global__txtbox').type('Test')
            //By whom
            cy.get(':nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(3) > :nth-child(2) > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(4) > :nth-child(2) > .fg-line > .global__txtbox').type('Test')
            //Time frame, if any
            cy.get(':nth-child(2) > :nth-child(3) > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(3) > :nth-child(3) > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(4) > :nth-child(3) > .fg-line > .global__txtbox').type('Test')
            //Case Conference Participants (click all)
            cy.get('.p-0 > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(2) > .ng-isolate-scope > .ng-valid').click()
            cy.get(':nth-child(3) > .ng-isolate-scope > .ng-pristine').click()
            cy.get('.p-t-0 > .ng-pristine').type('Test') //inputting comment

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button
        })


    })