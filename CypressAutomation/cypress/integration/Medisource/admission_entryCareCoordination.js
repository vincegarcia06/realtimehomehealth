Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of Care Coordination', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Will M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                
                    cy.get(':nth-child(4) > .navtitle').click();//Communication Note
                    cy.get('.col-sm-12 > .pull-right > .btn').click() //clicking new
                    
        })
           
        it('Automating Entry Form of Communication Note', function() {
            cy.get('[ui-sref="patientcare.commnotesCreate"]').click() //clicking communication note
            cy.wait(10000)
            cy.get('#date').type('05302022') //inputting date
            cy.get('.input-drp > .fg-line > .global__txtbox').type('Laboratory') //inputting title/subject
            cy.get(':nth-child(3) > .fg-line > .global__txtbox').type('1200') //inputting time
            cy.wait(2000)
            cy.get('td > .fg-line > .form-control').type('Test') //inputting Communication Notes
            //Notification and Care Coordination
            cy.get(':nth-child(1) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MD
            cy.get('[style="width: 35%"] > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > div > div > ul > li.active-result.highlighted').click() //clicking result
            cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking LVN
            cy.get(':nth-child(3) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PT
            cy.get(':nth-child(4) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking PTA
            cy.get(':nth-child(5) > :nth-child(2) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Caregiver
            cy.get('.p-l-5.ng-isolate-scope > .fg-line > .select > .form-control').type('Test')
            cy.get(':nth-child(7) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Patient
            cy.get(':nth-child(1) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking MSW
            cy.get(':nth-child(1) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(2)').click() //clicking result
            cy.get(':nth-child(2) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Dietician
            cy.get('.inner__table-nb > tbody > :nth-child(2) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(3) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking CHHA
            cy.get(':nth-child(3) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > div > ul > li').click() //clicking result
            cy.get(':nth-child(4) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Pharmacy
            cy.get(':nth-child(4) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(5) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OT
            cy.get(':nth-child(5) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(5) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(6) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking OTA
            cy.get(':nth-child(6) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(6) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(1)').click() //clicking result
            cy.get(':nth-child(7) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking ST
            cy.get(':nth-child(7) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(7) > td:nth-child(4) > div > div > div > div > ul > li').click() //clicking result
            cy.get(':nth-child(8) > :nth-child(3) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking QA Manager
            cy.get(':nth-child(8) > :nth-child(4) > .fg-line > .select > .chosen-container > .chosen-single').click() //clicking dropdown
            cy.get('#parent > div > form > div.max__width_wrapper > div > fieldset > table > tbody > tr > td > table > tbody > tr:nth-child(6) > td > table > tbody > tr:nth-child(8) > td:nth-child(4) > div > div > div > div > ul > li').click() //clicking result
            //Communicated via
            cy.get('.p-0 > .ng-isolate-scope > .ng-valid').click() //clicking phone
            cy.get(':nth-child(2) > .ng-isolate-scope > .ng-valid').click() //clicking fax
            cy.get(':nth-child(3) > .ng-isolate-scope > .ng-valid').click() //clicking mail
            cy.get(':nth-child(4) > .ng-isolate-scope > .ng-valid').click() //clicking direct

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button

        })

        it('Automating Entry Form of HHA Care Plan', function() {
            cy.get('[ng-click="createhha()"]').click() //clicking HHA Care Plan
            cy.wait(10000)

            //Functional Limitations (click all)
            cy.get('[name="function_multi"]').click({multiple:true})
            cy.get(':nth-child(3) > :nth-child(3) > .cont-opt > .fg-line > .global__txtbox').type('Test') //inputting other
            cy.wait(5000)
            //Activities Permitted (click all)
            cy.get('[rname="act_multi"]').click({multiple:true})
            cy.get(':nth-child(2) > .cont-opt > .fg-line > .global__txtbox').type('Test') //inputting other\
            cy.wait(5000)
            //Safety Measures (click all)
            cy.get('[rname="safety_multi"]').click({multiple:true})
            cy.get(':nth-child(5) > :nth-child(3) > .cont-opt > .fg-line > .global__txtbox').type('Test') //inputting other
            cy.wait(5000)

            //Care Plan Details
            //Vital Signs
            cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Temperature
            cy.get(':nth-child(4) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Blood pressure
            cy.get(':nth-child(5) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Pulse/Heart rate
            cy.get(':nth-child(6) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Respiration
            cy.get(':nth-child(7) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Weight
            //Procedures
            cy.get(':nth-child(9) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Assist with medications
            cy.get(':nth-child(10) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Assist with elimination
            cy.get(':nth-child(11) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Incontinence care
            cy.get(':nth-child(12) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Record bowel movement
            cy.get(':nth-child(13) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Urinary catheter care
            cy.get(':nth-child(14) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Record intake and output
            cy.get(':nth-child(15) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Ostomy care
            cy.get(':nth-child(16) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Empty drainage bag
            cy.get(':nth-child(17) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking each visit Inspect/reinforce dressing
            //Personal Care
            cy.get(':nth-child(3) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Bed bath
            cy.get(':nth-child(4) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Chair bath
            cy.get(':nth-child(5) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Shower
            cy.get(':nth-child(6) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Tub bath
            cy.get(':nth-child(7) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Hair Shampoo
            cy.get(':nth-child(8) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking each visit Hair care/comb hair
            cy.get(':nth-child(9) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Oral care/clean dentures
            cy.get(':nth-child(10) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Skin/Foot care
            cy.get(':nth-child(11) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Shave/Groom
            cy.get(':nth-child(12) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Nail care (Clean/File)
            cy.get(':nth-child(13) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Perineal care
            cy.get(':nth-child(14) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Assist with dressing
            //Nutrition
            cy.get(':nth-child(16) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Meal preparation
            cy.get(':nth-child(17) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Assist with feeding
            cy.get(':nth-child(2) > td > .radio > .ng-pristine').click() //clicking Limit
            cy.get('td > .fg-line > .global__txtbox').type('Test')
            cy.get(':nth-child(18) > :nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Fluid intake
            //Activities
            cy.get(':nth-child(3) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Reposition/turning in bed
            //Assist w/ mobility/transfer
            cy.get(':nth-child(4) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click() //clicking Bed
            cy.get(':nth-child(4) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click() //clicking Dangle
            cy.get(':nth-child(4) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click() //clicking Chair
            cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .p-l-25 > .ng-pristine').click() //clicking BSC
            cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .p-l-25 > .ng-pristine').click() //clicking Shower/Tub
            cy.get(':nth-child(4) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Assist w/ mobility/transfer
            //Assist in ambulation
            cy.get(':nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .m-r-10 > .ng-valid').click() //clicking Cane
            cy.get(':nth-child(2) > .checkbox > .m-r-10 > .ng-valid').click() //clicking Walker
            cy.get(':nth-child(3) > .checkbox > .m-r-10 > .ng-pristine').click() //clicking W/C
            cy.get(':nth-child(7) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Assist in ambulation
            //Range of motion
            cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click() //clicking lUE
            cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click() //clicking RUE
            cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click() //clicking LLE
            cy.get(':nth-child(9) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(4) > .checkbox > .p-l-25 > .ng-valid').click() //clicking RLE
            cy.get(':nth-child(9) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Range of motion
            //Assist in prescribed exercises per
            cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(1) > .checkbox > .p-l-25 > .ng-valid').click() //clicking POC
            cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(2) > .checkbox > .p-l-25 > .ng-valid').click() //clicking PT
            cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(3) > .checkbox > .p-l-25 > .ng-valid').click() //clicking OT
            cy.get(':nth-child(11) > :nth-child(7) > table > tbody > :nth-child(2) > :nth-child(4) > .checkbox > .p-l-25 > .ng-valid').click() //clicking ST
            cy.get(':nth-child(11) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Assist in prescribed exercises per
            cy.get(':nth-child(14) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Equipment care
            //Household Tasks
            cy.get(':nth-child(16) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Light housekeeping
            cy.get(':nth-child(17) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Change bed linen
            cy.get(':nth-child(18) > :nth-child(8) > .checkbox > .ng-pristine').click() //clicking each visit Wash clothes
            cy.get(':nth-child(5) > .checkbox > .ng-pristine').click() //clicking each visit Assist with errands
            cy.wait(5000)
            cy.get('.table-input').type('Test') //inputting Comments / Additional Instructions
            //Home Health Aide
            cy.get('#aor0005_chosen > .chosen-single').click() //clicking dropdown Home Health Aide
            cy.get('#aor0005_chosen > div > ul').click() //clicking result
            cy.get(':nth-child(4) > .ng-isolate-scope > #aor0003').type('05302022') //inputting date Care Plan reviewed with Home Health Aide
            cy.get('#aor0006').type('05302022') //inputting Date Reviewed/Revised

            //SAVE Button
            cy.get('.btn__success').click() //clicking Save Button

        })

    })