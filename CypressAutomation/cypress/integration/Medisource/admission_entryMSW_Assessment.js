Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of MSW - Assessment', () => {

           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
               cy.get('.searchbar__content > .ng-pristine').type('Test, Peralta M.')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)
                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(16) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click MSW - Assessment
                })
           
        it('Automating Entry Form of MSW - Assessment', function() {
            cy.wait(10000)
            cy.get('#time_in').type('1200') //inputting time in
            cy.get('#time_out').type('1900') //inputting time out
            cy.wait(2000)

            //Evaluation and Plan of Care
            //Plan of Care/Medical Social Work Orders(click all)
            cy.get('[rname="orders"]').click({multiple:true})
            cy.get('[chk="field.orders"] > :nth-child(2) > :nth-child(4) > :nth-child(1) > .radio > .ng-pristine').click() //clicking crisis intervention to
            //Order Crises(click all)
            cy.get('[rname="order_crises"]').click({multiple:true})
            cy.wait(5000)

            cy.get('tbody > :nth-child(4) > :nth-child(2) > .global__txtbox').type('Test') //inputting Home Setting / Environment
            cy.get(':nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') //inputting Emergency Contact
            cy.get(':nth-child(6) > :nth-child(2) > .global__txtbox').type('Test') //inputting Support System
            cy.get(':nth-child(7) > :nth-child(2) > .global__txtbox').type('Test') //inputting Homebound Status
            cy.wait(5000)

            //Mental Status / Behavior (click all)
            cy.get('[rname="mental_statuses"]').click({multiple:true})
            cy.get(':nth-child(9) > td > .row > .col-sm-6 > :nth-child(4) > .m-0 > .radio > .ng-pristine').click() //clicking other
            cy.get(':nth-child(9) > td > .row > .col-sm-6 > :nth-child(4) > .display-ib.ng-isolate-scope > .global__txtbox').type('Test') //inputting other
            cy.wait(5000)

            //Financial Status (click all)
            cy.get('[rname="financial_statuses"]').click({multiple:true})
            cy.get('td > .row > .col-sm-6 > :nth-child(4) > .m-0 > .radio > .ng-pristine').click() //clicking other
            cy.get(':nth-child(11) > td > .row > .col-sm-6 > :nth-child(4) > .display-ib.ng-isolate-scope > .global__txtbox').type('Test') //inputting other

            cy.get(':nth-child(13) > td > .fg-line > .form-control').type('Test') //inputting Clinical Findings / Problems
            cy.get(':nth-child(15) > td > .fg-line > .form-control').type('Test') //inputting Goals
            cy.get(':nth-child(17) > td > .fg-line > .form-control').type('Test') //inputting Interventions
            cy.get(':nth-child(19) > td > .fg-line > .form-control').type('Test') //inputting Referrals
            cy.get(':nth-child(21) > td > .fg-line > .form-control').type('Test') //inputting Outcome & Plan

            //Frequency
            cy.get('.m-0.pull-left > :nth-child(1) > .ng-pristine').click() //clicking 1 Visit x 60 days
            cy.get('.m-l-15 > .m-0 > .radio > .ng-pristine').click() //clicking other
            cy.get('.m-l-15 > .display-ib.ng-isolate-scope > .global__txtbox').type('Test') //inputting other

            //Discharge Summary
            //Reason for Discharge (click all)
            cy.get('[rname="dc_reasons"]').click({multiple:true})
            cy.get(':nth-child(3) > :nth-child(4) > .m-0 > .radio > .ng-valid').click() //clicking other
            cy.get(':nth-child(3) > :nth-child(4) > div.ng-isolate-scope > .global__txtbox').type('Test')

            //Overall Status of Patient at Discharge
            cy.get('[rname="dc_mental_statuses"]').click({multiple:true})
            cy.get(':nth-child(4) > .m-0 > .radio > .ng-pristine').click() //clicking other
            cy.get(':nth-child(1) > :nth-child(1) > .row > :nth-child(2) > :nth-child(4) > div.ng-isolate-scope > .global__txtbox').type('Test') //inputting other
            //Patient's Functional Status ADL's
            cy.get(':nth-child(1) > :nth-child(2) > .radio > .ng-valid').click()

            cy.get(':nth-child(30) > td > .fg-line > .form-control').type('Test') //inputting Continuing Level of In-Home Care
            cy.get(':nth-child(32) > td > .fg-line > .form-control').type('Test') //inputting Problems Identified
            cy.get(':nth-child(34) > td > .fg-line > .form-control').type('Test') //inputting Status of Problems at Discharge
            cy.get(':nth-child(36) > td > .fg-line > .form-control').type('Test') //inputting Summary of Care Provided

            cy.get(':nth-child(37) > td > :nth-child(1) > .ng-valid').click() //clicking Goals met


            //SAVE BUTTON
            cy.get('.btn__success').click() //clicking save button
            cy.wait(10000)
    
        })
    })