Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of RN - Supervisory(CHHA)', () => {
           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
            // cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
               cy.get('.searchbar__content > .ng-pristine').type('Peralta')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)

                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(8) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click RN - IV Visit
                    cy.wait(10000)
        })
        
        it('Automating Entry Form of RN - Supervisory(CHHA)', function() {
        
            //Clinician present at time of visit?
            cy.get(':nth-child(1) > .global__answer > .global__inner-nb > tbody > tr > td > :nth-child(1) > .ng-pristine').click() // clicking yes
            //Visit Assessment
            cy.get('[colspan="2"] > .select > .chosen-container > .chosen-single').click() //clicking dropdown for CHHA
            cy.get('#parent > div > div > form > div.table-responsive.max__width_wrapper > fieldset > div > table.table.global__table.m-t-20 > tbody > tr.ng-scope > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > div > ul > li').click() //clicking result for CHHA
            
            //Check Met
            cy.get(':nth-child(3) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(8) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(9) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(10) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(11) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(12) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(13) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(14) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(15) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(16) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(17) > :nth-child(1) > .radio > .ng-pristine').click()

            cy.get('.table-input').type('Test')

            cy.get('.btn__success').click() //clicking save button
            cy.wait(5000)
            
        })
    })
