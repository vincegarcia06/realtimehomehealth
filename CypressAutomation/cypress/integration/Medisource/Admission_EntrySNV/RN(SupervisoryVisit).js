Cypress.config('experimentalSessionSupport', true) //enable the experimentalSessionSupport

/// <reference types= "cypress" />


describe('Automating Entry Form of RN - Supervisory Visit', () => {
           
        beforeEach(() => {
            cy.login1('vincent@geeksnest', 'Tester2021@'); //cy.login - command located in command.js
        
            cy.viewport(1920, 924);//setting your windows size
            cy.visit('https://qado.medisource.com/patients/admitted'); //visit page for patient admitted
            cy.wait(5000)
            // cy.get('#content > data > div > div.card > div > div.patientcare__nav > div > ul > li:nth-child(2) > a').click() //click in-patient tab
               cy.get('.searchbar__content > .ng-pristine').type('Peralta')  //search the added patient 
                    cy.wait(5000) 
                    //click the added patient                    
                    cy.get('#content > data > div > div.globallist__content_block > div.table-responsive.custom-responsive_900 > table > tbody > tr.sampletd.pointer.globallist-item.ng-scope > td:nth-child(1) > a > div.wdTabCusPatient').click()                       
                    cy.wait(5000)

                    cy.get('#parent > div > div.row.ng-scope > div > div.globallist__content_block.m-b-30 > div.table-responsive > table > tbody > tr:nth-child(11) > td:nth-child(2) > a.ng-binding.ng-scope').click();//click RN - Supervisory Visit
                    cy.get('.btn__warning').click({force: true}) //click Edit Button
                    cy.wait(10000)
        })
        
        it('Automating Entry Form of VS/Sensory/Integumentary/Endocrine', function() {

            // VS/Sensory/Integumentary/Endocrine TAB Start -------------
            cy.get('#timeIn').type(1900) //inputting time in
            cy.get('#timeOut').type(1200) //inputting time out
            //Vitals Sign Information Section
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(1) > td.b1-y.b1-b > div > input').type('98') // inputting temperature
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(1) > td:nth-child(3) > div.radio.radio-inline.m-t-2.p-0 > label:nth-child(1) > input').click() //clicking oral for temperature
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td.b1-y.b1-b > div > input').type('3') //inputting Pulse/HR
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(2) > td:nth-child(3) > div.radio.radio-inline.m-t-2.p-0 > label:nth-child(1) > input').click() //clicking radial for Pulse/HR
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(3) > td.b1-y.b1-b > div > input').type('3') //inputting respiration
            cy.get(':nth-child(4) > .b1-y > .cont-opt > .text-right').type('180') //inputting Systolic - BP Left Arm
            cy.get(':nth-child(4) > .b1-y > .cont-opt > .ng-pristine').type('120')  //inputting Diastolic - BP Left Arm
            cy.get(':nth-child(4) > .p-r-0 > .radio > :nth-child(2) > .ng-valid').click() //clicking sitting
            cy.get(':nth-child(5) > .b1-y > .cont-opt > .text-right').type('180') //inputting Systolic - BP Right Arm
            cy.get('[name="VS0056"]').type('120') //inputting Diastolic - BP Right Arm
            cy.get(':nth-child(5) > .p-r-0 > .radio > :nth-child(2) > .ng-pristine').click() //clicking sitting
            cy.get(':nth-child(6) > .b1-y > .cont-opt > .text-right').type('180') //inputting Systolic - BP Left Leg
            cy.get('[name="VS0059"]').type('120') //inputting Diastolic - BP Left Leg
            cy.get(':nth-child(6) > .p-r-0 > .radio > :nth-child(2) > .ng-pristine').click() //clicking sitting
            cy.get(':nth-child(7) > .b1-y > .cont-opt > .text-right').type('180') //inputting Systolic - BP Right Leg
            cy.get('[name="VS0058"]').type('120') //inputting Diastolic - Right Leg
            cy.get('.p-r-0 > .radio > :nth-child(2) > .ng-pristine').click() //clicking sitting
            cy.get(':nth-child(8) > .b1-y > .cont-opt > .global__txtbox').type('2') //inputting O2 Saturation % on room air
            cy.get(':nth-child(9) > .b1-y > .cont-opt > .global__txtbox').type('2') //inputting O2 Saturation % on O2
            cy.get(':nth-child(9) > :nth-child(3) > :nth-child(2) > .global__txtbox').type('2') //inputting lPM on o2 Saturation
            cy.get(':nth-child(10) > .b1-y > .cont-opt > .global__txtbox').type('120') //inputting blood sugar
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(10) > td:nth-child(3) > div.radio.radio-inline.m-t-2.p-0 > label:nth-child(1) > input').click() //clicking FBS
            cy.get(':nth-child(11) > .b1-y > .cont-opt > .global__txtbox').type('100') //inputting weight
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(11) > td:nth-child(3) > div.radio.radio-inline.m-t-2.p-0 > label:nth-child(1) > input').click() //clicking actual
            cy.get(':nth-child(12) > .b1-y > .cont-opt > .global__txtbox').type('60') //inputting height
            cy.get('#vital_signs > tbody > tr:nth-child(1) > td > table > tbody > tr:nth-child(12) > td:nth-child(3) > div.radio.radio-inline.m-t-2.p-0 > label:nth-child(1) > input').click() //clicking actual
            cy.get('[opt="data.VS0085"] > .ng-pristine').click() //clicking MD in notified
            cy.get('[opt="data.VS0086"] > .ng-pristine').click() //clicking CM/Supervisor
            cy.get(':nth-child(2) > .b1-l > :nth-child(1) > .dFlex > .radio > :nth-child(2) > .ng-pristine').click() //clicking Yes for evidence for infection
            cy.get('.b1-l > .m-r-5 > .global__txtbox').type('test') //inputting description
            cy.get('.p-r-15 > .ng-pristine').click() //clicking MD in notified
            cy.get('[chk="data.VS0032"] > .ng-pristine').click() //clicking CM/Supervisor
            cy.get('.dFlex > .radio > :nth-child(2) > .ng-pristine').click() //clicking yes for new/change/hold medications
            cy.get('[style="width:100%;"] > .global__txtbox').type('Test') //inputting description
            cy.get(':nth-child(1) > :nth-child(2) > .w-100 > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(2) > :nth-child(2) > .w-100 > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)

            // Frequency of Pain
            cy.get('.b1-b > :nth-child(2) > .m-r-15 > .ng-pristine').click() //clicking 1 for frequency pain

            // Pain Location
            // Location 1
            cy.get(':nth-child(2) > .pos-r > .global__txtbox').type('Test') //inputting pain location
            cy.get(':nth-child(2) > :nth-child(2) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking dropdown for type
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
            cy.get(':nth-child(2) > :nth-child(3) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking dropdown for present level
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(5)').click() //clicking personal level
            cy.get(':nth-child(2) > :nth-child(4) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking dropdown worst level
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(5)').click() //clicking worst level
            cy.get(':nth-child(2) > :nth-child(5) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking acceptable level dropdown
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(5)').click() //clicking acceptable level
            cy.get(':nth-child(2) > :nth-child(6) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking level after meds dropdown
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(4)').click() //clicking level after meds
            cy.wait(5000)
            // Location 2
            cy.get(':nth-child(3) > .pos-r > .global__txtbox').type('Test') //inputting pain location
            cy.get(':nth-child(3) > :nth-child(2) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking dropdown for type
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > div > div > ul > li:nth-child(1)').click() //clicking type
            cy.get(':nth-child(3) > :nth-child(3) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking dropdown for present level
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(3) > td:nth-child(3) > div > div > div > div > ul > li:nth-child(6)').click() //clicking personal level
            cy.get(':nth-child(3) > :nth-child(4) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking dropdown worst level
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > div > div > ul > li:nth-child(6)').click() //clicking worst level
            cy.get(':nth-child(3) > :nth-child(5) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking acceptable level dropdown
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(3) > td:nth-child(5) > div > div > div > div > ul > li:nth-child(6)').click() //clicking acceptable level
            cy.get(':nth-child(3) > :nth-child(6) > .select > .global__select > .chosen-container > .chosen-single').click() //clicking level after meds dropdown
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(2) > td > table:nth-child(4) > tbody > tr:nth-child(3) > td:nth-child(6) > div > div > div > div > ul > li:nth-child(7)').click() //clicking level after meds
            cy.wait(5000)
            //Character Pain(Select All)
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.wait(5000)
            //Non-verbal signs of pain (Select All)
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.wait(5000)
            //What makes the pain better?
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(1) > .valign-t > .global__txtbox').type('Test')
            cy.wait(5000)
            //What makes the pain worse?
            cy.get(':nth-child(4) > .p-0 > .table-rt > tbody > tr > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(4) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(4) > .p-0 > .table-rt > tbody > tr > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(4) > .p-0 > .table-rt > tbody > tr > .valign-t > .global__txtbox').type('Test')
            //Pain medication
            cy.get('[ng-hide="hide.PA0037"] > label.ng-isolate-scope > .ng-pristine').click() // clicking yes for pain medication
            cy.get(':nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') // inputting medication profile
            //How often pain meds needed?
            cy.get(':nth-child(6) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .radio > .ng-isolate-scope > .ng-pristine').click() //clicking how often pain meds needed
            // Pain medication effectiveness
            cy.get(':nth-child(7) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .radio > label.ng-isolate-scope > .ng-pristine').click() //clicking Pain medication effectiveness
            //Physician aware of pain?
            cy.get('.p-l-0 > label.ng-isolate-scope > .ng-pristine').click() //clicking yes for Physician aware of pain
            cy.get(':nth-child(9) > .p-0 > .table-rt > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting obsevation
            cy.get(':nth-child(9) > .p-0 > .table-rt > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Sensory Status
            //Eyes
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for cataract
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking right for cataract
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for glaucoma
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking right for glaucoma
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for redness
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking right for redness
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(5) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for pain
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(5) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking right for pain
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(6) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for itching
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(6) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking right for itching
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for ptsosis
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for ptosis
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Sclera reddened
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Sclera reddened
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Edema of eyelids
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Edema of eyelids
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(5) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Blurred vision
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(5) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Blurred vision
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(6) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Blind	
            cy.get('[ng-hide="data.SENSORY0001 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(6) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Blind	
            cy.get(':nth-child(2) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Exudate from eyes	
            cy.get(':nth-child(2) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Exudate from eyes	
            cy.get(':nth-child(3) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Excessive tearing
            cy.get(':nth-child(3) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Excessive tearing
            cy.get(':nth-child(4) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Macular degeneration
            cy.get(':nth-child(4) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Macular degeneration
            cy.get(':nth-child(5) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Retinopathy
            cy.get(':nth-child(5) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Retinopathy
            cy.get('.text-r > div > .global__txtbox').type('Test') //inputting other status
            cy.get(':nth-child(6) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for other status
            cy.get(':nth-child(6) > :nth-child(3) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for other status
            cy.wait(5000)
            //Ears
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for HOH
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for HOH
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Hearing aid
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Hearing aid
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-pristine').click() //clicking left for Deaf
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(1) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-pristine').click() //clicking Right for Deaf
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-valid').click() //clicking left for Tinnitus
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-valid').click() //clicking Right for Tinnitus
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-valid').click() //clicking left for Drainage
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(3) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-valid').click() //clicking Right for Drainage
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .p-0 > .ng-valid').click() //clicking left for Pain
            cy.get('[ng-hide="data.SENSORY0039 === true"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(4) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(3) > .checkbox > .p-0 > .ng-valid').click() //clicking Right for Pain
            cy.get(':nth-child(2) > :nth-child(3) > .table-rt > tbody > tr > td > div > .global__txtbox').type('Test')
            cy.get(':nth-child(3) > .table-rt > tbody > tr > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.wait(5000)
            //Mouth
            cy.get('.m-r-10 > label > .ng-scope').click() //clicking dentures
            cy.get('[ng-hide="hide.SENSORY0036"] > :nth-child(2) > :nth-child(1) > .ng-scope').click() //clicking upper
            cy.get('[ng-hide="hide.SENSORY0036"] > :nth-child(2) > :nth-child(2) > .ng-scope').click() //clicking lowe
            cy.get('[ng-hide="hide.SENSORY0036"] > :nth-child(2) > :nth-child(3) > .ng-scope').click() //clicking partial
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking poor dentition for mouth
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking gingivitis for mouth
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking toothache for mouth
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking loss of taste for mouth
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking lesions mouth
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .di-block > .ng-isolate-scope > .global__txtbox').type('Test') //inputting lesions
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking mass/tumor for mouth
            cy.get(':nth-child(3) > :nth-child(2) > .di-block > .ng-isolate-scope > .global__txtbox').type('Test') //inputting mass/tumor
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .checkbox > label > .ng-scope').click() // clicking Difficulty of chewing/swallowing for mouth
            cy.get('[ng-hide="data.SENSORY0059 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(3) > .global__txtbox').type('Test') //inputting others for mouth
            cy.wait(5000)
            //Nose
            cy.get('[style="width:20%"] > .checkbox > label > .ng-scope').click() //clicking congestion for nose
            cy.get('[ng-hide="data.SENSORY0055 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking rhinitis for nose
            cy.get('[ng-hide="data.SENSORY0055 === true"] > .p-0 > .table-rt > tbody > :nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking sinus problem for nose
            cy.get('[ng-hide="data.SENSORY0055 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking epistaxis for nose
            cy.get('[ng-hide="data.SENSORY0055 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking loss of smell for nose
            cy.get(':nth-child(3) > [colspan="3"] > .checkbox > label > .ng-scope').click() //clicking noisy breathing for nose
            cy.get('[ng-hide="data.SENSORY0055 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .checkbox > label > .ng-scope').click() //clicking lesions for nose
            cy.get(':nth-child(3) > .di-block > .ng-isolate-scope > .global__txtbox').type('Test') //inputting lesions for nose
            cy.get('[colspan="2"] > .global__txtbox').type('Test') //inputting others for nose
            cy.wait(5000)
            //Throat
            cy.get('[ng-hide="data.SENSORY0063 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking sore throat for throat
            cy.get('[ng-hide="data.SENSORY0063 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking dysphagia for throat
            cy.get('[ng-hide="data.SENSORY0063 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking hoarseness for throat
            cy.get('[ng-hide="data.SENSORY0063 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > [colspan="3"] > .checkbox > label > .ng-scope').click() //clicking lesions for throat
            cy.get('[colspan="3"] > .di-block > .ng-isolate-scope > .global__txtbox').type('Test') //inputting lesions for throat
            cy.get('[ng-hide="data.SENSORY0063 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .global__txtbox').type('Test') //inputting other for throat
            cy.wait(5000)
            //Speech
            cy.get('[ng-hide="data.SENSORY0067"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking slow speech for speech
            cy.get('[ng-hide="data.SENSORY0067"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking slured speech for speech
            cy.get('[ng-hide="data.SENSORY0067"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking aphasia for speech
            cy.get('[ng-hide="data.SENSORY0067"] > .p-0 > .table-rt > tbody > :nth-child(2) > [colspan="3"] > .checkbox > label > .ng-scope').click() //clicking mechanical assistance for speech
            cy.get('[ng-hide="data.SENSORY0067"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(3) > .global__txtbox').type('Test') //inputting other for speech
            //Touch
            cy.get('[ng-hide="data.SENSORY0070 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking paresthesia for touch
            cy.get('[ng-hide="data.SENSORY0070 === true"] > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click() //clicking hyperesthesia for touch
            cy.get('[ng-hide="data.SENSORY0070 === true"] > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking peripheral neuropathy for touch
            cy.get('[colspan="3"] > .global__txtbox').type('Test') //inputting other for touch
            cy.get(':nth-child(3) > :nth-child(1) > .b1-t > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(3) > :nth-child(1) > .b1-t > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention

            //Integumentary Status
            cy.get('[width="90%"] > .table-rt > tbody > :nth-child(1) > [width="200px"] > div > .radio > .ng-valid').click() //clicking skin color
            cy.get(':nth-child(2) > :nth-child(2) > .table-rt > tbody > :nth-child(1) > [width="200px"] > div > .radio > .ng-pristine').click() //clicking skin temp
            cy.get(':nth-child(3) > :nth-child(2) > .table-rt > tbody > :nth-child(1) > [width="200px"] > div > .radio > .ng-pristine').click() //clicking moisture
            cy.get(':nth-child(4) > :nth-child(2) > .table-rt > tbody > :nth-child(1) > [width="200px"] > div > .radio > .ng-pristine').click() //clicking turgor
            //Skin Integrity
            cy.get('.p-l-10 > .checkbox > label.ng-isolate-scope > .ng-pristine').click() //clicking skin intact
            cy.get(':nth-child(3) > .checkbox > label.ng-isolate-scope > .ng-pristine').click() //clicking lesion
            //Other Form for Lesion
            cy.get('#integumentary_status > tbody > tr:nth-child(5) > td > table.table-rt.ng-scope > tbody > tr > td:nth-child(3) > div > div > a').click() //clicking dropdown (select one lesion)
            cy.get('#integumentary_status > tbody > tr:nth-child(5) > td > table.table-rt.ng-scope > tbody > tr > td:nth-child(3) > div > div > div > ul > li:nth-child(1)').click() //selecting result dropdown (select one lesion)
            cy.wait(2000)
            cy.get('[style="width: 150px"] > .global__txtbox').type('Test') //inputting location
            cy.get(':nth-child(7) > .global__txtbox').type('Test') //inputting comment

            cy.get('.b1-b > :nth-child(4) > .checkbox > label.ng-isolate-scope > .ng-pristine').click() //clicking wound
            cy.get(':nth-child(4) > .m-l-5 > .btn > .zmdi').click() //clicking add wound
            //Other Form for Wound
            cy.get('#integumentary_status > tbody > tr:nth-child(5) > td > table:nth-child(3) > tbody > tr > td.td-wound-type-0 > div > div.fg-line > input').type('Pressure Ulcer') //inputting result on wound #1
            cy.get('#integumentary_status > tbody > tr:nth-child(5) > td > table:nth-child(3) > tbody > tr > td.td-location-0 > div > div.fg-line > input').type('Buttock (R)') //inputting result (Location)
            cy.get('[ng-if="data.INTEG0025 === true"] > tbody > .b1-b > :nth-child(7) > .global__txtbox').type('Test') //inputting comment on wound #1
            cy.get('[style="font-weight: normal;"] > .ng-isolate-scope > .global__txtbox').type('Test') //inputting other in skin integrity
            cy.get(':nth-child(3) > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(3) > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Endocrine System (Select All)
            cy.get('[style="width: 20%"] > .checkbox > label.ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(2) > :nth-child(1) > .checkbox > .m-b-5 > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .m-b-5 > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .m-b-5 > .ng-pristine').click()
            cy.get('[style="width: 30%"] > .checkbox > .m-b-5 > .ng-pristine').click()
            cy.get(':nth-child(2) > :nth-child(2) > .checkbox > .m-b-5 > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .m-b-5 > .ng-pristine').click()
            cy.get('fieldset > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .global__txtbox').type('Test') //inputting other in endocrine system
            //Diabetes
            cy.get('[style="font-weight: normal; width: 20%;"] > .global__txtbox').type('Test') //inputting Hgb A1C
            cy.get('.sub-th > .p-0 > .table-rt > tbody > tr > [style="font-weight: normal;"] > .global__txtbox').type('04/23/2022') //inputting date tested
            cy.get('[style="width: 20%;"] > div > .radio > .ng-pristine').click() //clicking new for onset
            cy.get('[style="width: 30%;"] > .global__txtbox').type('04/02/2022') //inputting onset date
            //Diabetes management (Select All)
            cy.get(':nth-child(1) > [style="width: 20%;"] > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(2) > [style="width: 20%;"] > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()

            cy.get(':nth-child(4) > :nth-child(2) > div > :nth-child(2) > .ng-pristine').click() //clicking yes for Signs of hypoglycemia?
            cy.get(':nth-child(5) > :nth-child(2) > div > :nth-child(2) > .ng-pristine').click() //clicking yes for Signs of hyperglycemia?
            cy.get(':nth-child(2) > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting for observation
            cy.get(':nth-child(2) > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting for intervention
            // Foot Assessment
            cy.get(':nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for Thick or ingrown toenail
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(2) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for Thick or ingrown toenail
            cy.get(':nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for Calluses or fissures
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(3) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for Calluses or fissures
            cy.get(':nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for Interdigital macerations
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(4) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for Interdigital macerations
            cy.get(':nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(5) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for Signs of fungal infection
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(5) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for Signs of fungal infection
            cy.get(':nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(6) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for Absent pedal pulses
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(6) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for Absent pedal pulses
            cy.get(':nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(7) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for Hot, red, swollen foot
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(7) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for Hot, red, swollen foot
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(2) > .b1-r > .checkbox > label > .ng-scope').click() //clicking left for Foot deformity (hammer/claw toes)
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking right for Foot deformity (hammer/claw toes)
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(3) > .b1-r > .checkbox > label > .ng-scope').click() //clicking left for Limited range of motion of joints
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking right for Limited range of motion of joints
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(4) > .b1-r > .checkbox > label > .ng-scope').click() //clicking left for Decreased circulation (cold foot)
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking right for Decreased circulation (cold foot)	
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(5) > .b1-r > .checkbox > label > .ng-scope').click() //clicking left for Burning/tingling sensation, numbness	
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(5) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking right for Burning/tingling sensation, numbness
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(6) > .b1-r > .checkbox > label > .ng-scope').click() //clicking left for Loss of sensation to heat or cold
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(6) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking right for Loss of sensation to heat or cold
            cy.get(':nth-child(7) > :nth-child(1) > .global__txtbox').type('Test') //inputting other in foot assessment
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(7) > :nth-child(2) > .checkbox > label > .ng-scope').click() //clicking left for other foot assessment
            cy.get('[style="width:35%"] > .table-rt > tbody > :nth-child(7) > .b1-r > .checkbox > label > .ng-scope').click() //clicking right for other foot assessment
            
            cy.get('[style="width:50%;"] > :nth-child(1) > .ng-pristine').click() //clicking foot exam frequency
            cy.get('[style="width:50%;"] > .global__txtbox').type('Test') //inputting other for foot exam frequency
            //Regularly done by (Select All)
            cy.get('[style="width:50%;"] > :nth-child(1) > label > .ng-scope').click()
            cy.get('[style="width:50%;"] > :nth-child(2) > label > .ng-scope').click()
            cy.get('[style="width:50%;"] > :nth-child(3) > label > .ng-scope').click()
            cy.get('.p-0.b1-t > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            //Patient/Caregiver Competence
            cy.get(':nth-child(4) > tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for patient on Competent with glucometer use including control testing?
            cy.get(':nth-child(2) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking yes for caregiver on Competent with glucometer use including control testing?
            cy.get(':nth-child(4) > tbody > :nth-child(2) > :nth-child(4) > .global__txtbox').type('Test')
            cy.get(':nth-child(4) > tbody > :nth-child(3) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for patient on Competent with insulin preparation and administration?
            cy.get(':nth-child(3) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking yes for caregiver on Competent with insulin preparation and administration?
            cy.get(':nth-child(4) > tbody > :nth-child(3) > :nth-child(4) > .global__txtbox').type('Test')
            cy.get(':nth-child(4) > tbody > :nth-child(4) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for patient on Competent after instructions given and performed return demo?
            cy.get(':nth-child(4) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking yes for caregiver on Competent after instructions given and performed return demo?
            cy.get(':nth-child(4) > tbody > :nth-child(4) > :nth-child(4) > .global__txtbox').type('Test')
            cy.get('[colspan="3"] > :nth-child(1) > .ng-pristine').click() //clicking good for patient on Level of knowledge of disease process and management
            cy.get('[colspan="3"] > :nth-child(4) > .ng-pristine').click() //clicking good for caregiver on Level of knowledge of disease process and management
            //Blood glucose testing
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .radio > .ng-valid').click() //clicking testing frequency
            cy.get('#ENDO0006').type('Test') //inputting other in testing frequency
            cy.get('.cont-opt > .input-drp > .fg-line > .global__txtbox').click() //clicking input box
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table > tbody > tr:nth-child(5) > td > fieldset > div > table:nth-child(5) > tbody > tr:nth-child(3) > td.p-0 > table > tbody > tr:nth-child(1) > td > div.cont-opt > div > div.opt-list.ng-isolate-scope > ul > li:nth-child(1)').click() //clicking CGM Brand
            cy.get(':nth-child(2) > td > .checkbox > label.ng-isolate-scope > .ng-pristine').click() //clicking control testing done
            //Other Form of Glucometer/CGM (Control Test Result)
            cy.get('#ENDO0035').type('1') //inputting level
            cy.get('#ENDO0011').type('120') //inputting mg/dl
            cy.get('.b1-t > .p-0 > .table-rt > tbody > tr > [style="width:60%;"] > .m-l-50 > :nth-child(1) > .ng-pristine').click() //clicking yes for within range?
            cy.get('#ENDO0036').type('1') //inputting level
            cy.get('#ENDO0012').type('120') //inputting mg/dl
            cy.get(':nth-child(5) > .p-0 > .table-rt > tbody > tr > [style="width:60%;"] > .m-l-50 > :nth-child(1) > .ng-pristine').click() //clicking yes for within range?
            cy.get('#ENDO0025_chosen > ul > li > input').click() //clicking inputbox/dropdown
            cy.get('#ENDO0025_chosen > div > ul > li:nth-child(1)').click() //selecting result
            cy.get(':nth-child(7) > :nth-child(2) > .form-control').type('Test') //inputting action taken
            //Clinician present at time of visit?
            cy.get('.global__inner-nb > tbody > tr > td > :nth-child(1) > .ng-pristine').click() //clicking yes
            //Visit Assessment
            cy.get('[colspan="5"] > .select > .chosen-container > .chosen-single').click() //clicking dropdown for LVN
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div:nth-child(4) > table:nth-child(2) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(1) > td:nth-child(2) > div > div > div > ul > li.active-result.highlighted').click() //clicking result for LVN
            //Clicking MET (all)
            cy.get('.table > tbody > :nth-child(3) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get('.table > tbody > :nth-child(4) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(8) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get(':nth-child(9) > :nth-child(1) > .radio > .ng-pristine').click()
            cy.get('.table > tbody > :nth-child(3) > :nth-child(4) > .radio > .ng-pristine').click()
            cy.get('.table > tbody > :nth-child(4) > :nth-child(4) > .radio > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(4) > .radio > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(4) > .radio > .ng-pristine').click()
            cy.get(':nth-child(7) > :nth-child(4) > .radio > .ng-pristine').click()
            cy.get(':nth-child(8) > :nth-child(4) > .radio > .ng-pristine').click()
            //inputting comments
            cy.get('.table-input').type('Test')
            //Save Button
            cy.get('.btn__success').click() //clicking save button
            cy.wait(5000)
            cy.get('.swal2-cancel').click() //clicking access later in modal
            cy.wait(5000)

            // VS/Sensory/Integumentary/Endocrine TAB End -------------
        })

        it('Automating Entry Form of Cardiopulmonary/Nutrition/Elimination', function() {

            // Cardiopulmonary/Nutrition/Elimination TAB Start -------------
            cy.get(':nth-child(2) > .oasis-section-tabs').click() //clicking cardiopulmonary tab
            cy.wait(5000)
            //When is the patient dyspneic or noticeably Short of Breath?
            cy.get('fieldset > :nth-child(1) > .radio > .ng-valid').click()
            //Respiratory Status
            cy.get(':nth-child(2) > .oasis__supp-l > .cont-opt > span.ng-isolate-scope > .radio > .ng-valid').click() //clicking No Breath sounds clear, bilateral
            cy.get('tbody > :nth-child(2) > :nth-child(2) > div > .checkbox > .ng-pristine').click() //clicking left for Diminished
            cy.get(':nth-child(2) > :nth-child(3) > div > .checkbox > .ng-pristine').click() //clicking Right for Diminished
            cy.get(':nth-child(2) > :nth-child(4) > div > .checkbox > .ng-pristine').click() //clicking Anterior for Diminished
            cy.get(':nth-child(2) > :nth-child(5) > div > .checkbox > .ng-pristine').click() //clicking Posterior for Diminished
            cy.get(':nth-child(2) > :nth-child(6) > div > .checkbox > .ng-pristine').click() //clicking Upper for Diminished
            cy.get(':nth-child(2) > :nth-child(7) > div > .checkbox > .ng-pristine').click() //clicking Middle for Diminished
            cy.get(':nth-child(2) > :nth-child(8) > div > .checkbox > .ng-pristine').click() //clicking Lower for Diminished
            cy.get(':nth-child(3) > :nth-child(2) > div > .checkbox > .ng-pristine').click() //clicking left for Absent
            cy.get(':nth-child(3) > :nth-child(3) > div > .checkbox > .ng-pristine').click() //clicking Right for Absent
            cy.get(':nth-child(3) > :nth-child(4) > div > .checkbox > .ng-pristine').click() //clicking Anterior for Absent
            cy.get(':nth-child(3) > :nth-child(5) > div > .checkbox > .ng-pristine').click() //clicking Posterior for Absent
            cy.get(':nth-child(3) > :nth-child(6) > div > .checkbox > .ng-pristine').click() //clicking Upper for Absent
            cy.get(':nth-child(3) > :nth-child(7) > div > .checkbox > .ng-pristine').click() //clicking Middle for Absent
            cy.get(':nth-child(3) > :nth-child(8) > div > .checkbox > .ng-pristine').click() //clicking Lower for Absent
            cy.get('tbody > :nth-child(4) > :nth-child(2) > div > .checkbox > .ng-pristine').click() //clicking left for Rales (crackles)
            cy.get(':nth-child(4) > :nth-child(3) > div > .checkbox > .ng-pristine').click() //clicking Right for Rales (crackles)
            cy.get(':nth-child(4) > :nth-child(4) > div > .checkbox > .ng-pristine').click() //clicking Anterior for Rales (crackles)
            cy.get(':nth-child(4) > :nth-child(5) > div > .checkbox > .ng-pristine').click() //clicking Posterior for Rales (crackles)
            cy.get(':nth-child(4) > :nth-child(6) > div > .checkbox > .ng-pristine').click() //clicking Upper for Rales (crackles)
            cy.get(':nth-child(4) > :nth-child(7) > div > .checkbox > .ng-pristine').click() //clicking Middle for Rales (crackles)
            cy.get(':nth-child(4) > :nth-child(8) > div > .checkbox > .ng-pristine').click() //clicking Lower for Rales (crackles)
            cy.get(':nth-child(5) > :nth-child(2) > div > .checkbox > .ng-pristine').click() //clicking left for Rhonchi
            cy.get(':nth-child(5) > :nth-child(3) > div > .checkbox > .ng-pristine').click() //clicking Right for Rhonchi
            cy.get(':nth-child(5) > :nth-child(4) > div > .checkbox > .ng-pristine').click() //clicking Anterior for Rhonchi
            cy.get(':nth-child(5) > :nth-child(5) > div > .checkbox > .ng-pristine').click() //clicking Posterior for Rhonchi
            cy.get(':nth-child(5) > :nth-child(6) > div > .checkbox > .ng-pristine').click() //clicking Upper for Rhonchi
            cy.get(':nth-child(5) > :nth-child(7) > div > .checkbox > .ng-pristine').click() //clicking Middle for Rhonchi
            cy.get(':nth-child(5) > :nth-child(8) > div > .checkbox > .ng-pristine').click() //clicking Lower for Rhonchi
            cy.get(':nth-child(6) > :nth-child(2) > div > .checkbox > .ng-pristine').click() //clicking left for Wheeze
            cy.get(':nth-child(6) > :nth-child(3) > div > .checkbox > .ng-pristine').click() //clicking Right for Wheeze
            cy.get(':nth-child(6) > :nth-child(4) > div > .checkbox > .ng-pristine').click() //clicking Anterior for Wheeze
            cy.get(':nth-child(6) > :nth-child(5) > div > .checkbox > .ng-pristine').click() //clicking Posterior for Wheeze
            cy.get(':nth-child(6) > :nth-child(6) > div > .checkbox > .ng-pristine').click() //clicking Upper for Wheeze
            cy.get(':nth-child(6) > :nth-child(7) > div > .checkbox > .ng-pristine').click() //clicking Middle for Wheeze
            cy.get(':nth-child(6) > :nth-child(8) > div > .checkbox > .ng-pristine').click() //clicking Lower for Wheeze
            cy.get('.inner__table-wb > tbody > :nth-child(7) > :nth-child(2) > div > .checkbox > .ng-pristine').click() //clicking left for Stridor
            cy.get('.inner__table-wb > tbody > :nth-child(7) > :nth-child(3) > div > .checkbox > .ng-pristine').click() //clicking Right for Stridor
            cy.get(':nth-child(7) > :nth-child(4) > div > .checkbox > .ng-pristine').click() //clicking Anterior for Stridor
            cy.get(':nth-child(7) > :nth-child(5) > div > .checkbox > .ng-pristine').click() //clicking Posterior for Stridor
            cy.get(':nth-child(7) > :nth-child(6) > div > .checkbox > .ng-pristine').click() //clicking Upper for Stridor
            cy.get(':nth-child(7) > :nth-child(7) > div > .checkbox > .ng-pristine').click() //clicking Middle for Stridor
            cy.get(':nth-child(7) > :nth-child(8) > div > .checkbox > .ng-pristine').click() //clicking Lower for Stridor
            cy.wait(5000)
            cy.get(':nth-child(4) > :nth-child(1) > .oasis__supp-l > .cont-opt > span.ng-isolate-scope > .radio > .ng-valid').click() //clicking yes for Abnormal breathing patterns
            //Abnormal breathing patterns (Select All)
            cy.get(':nth-child(1) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > tbody > :nth-child(1) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > tbody > :nth-child(1) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.cont-opt > .fg-line > .global__txtbox').type('Test')
            cy.wait(5000)
            cy.get(':nth-child(6) > :nth-child(1) > .oasis__supp-l > .cont-opt > span.ng-isolate-scope > .radio > .ng-valid').click() //clicking yes for cough
            //Cough
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody.ng-scope.ng-isolate-scope > tr:nth-child(1) > td > table > tr > td:nth-child(2) > label:nth-child(1) > input').click() //clicking character of cough
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody.ng-scope.ng-isolate-scope > tr:nth-child(2) > td > table > tr > td:nth-child(2) > div > label > input').click() //clicking sputum color of cough
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody.ng-scope.ng-isolate-scope > tr:nth-child(3) > td > table > tr > td:nth-child(2) > div > label > input').click() //clicking sputum character of cough
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody.ng-scope.ng-isolate-scope > tr:nth-child(4) > td > table > tr > td:nth-child(2) > div > label > input').click() //clicking sputum amount of cough
            //Special Procedure (Select All)
            cy.get('.p-t-3 > .radio > .ng-pristine').click()
            cy.get('.p-b-5 > .radio > .ng-pristine').click()
            cy.get('[width="180px"] > .radio > .ng-pristine').click()
            cy.get('.inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click()
            cy.get(':nth-child(1) > :nth-child(4) > .radio > .ng-pristine').click()
            cy.get('tbody > :nth-child(2) > :nth-child(3) > .radio > .ng-pristine').click()
            cy.get(':nth-child(5) > .global__txtbox').type('Test') //inputting other for special procedure
            cy.get(':nth-child(9) > :nth-child(1) > .p-0 > :nth-child(1) > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(9) > :nth-child(1) > .p-0 > :nth-child(1) > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Oxygen Risk Assessment
            cy.get(':nth-child(1) > .b1-r > .radio > :nth-child(1) > .ng-pristine').click() //click yes for Is the patient using oxygen equipment?
            cy.get(':nth-child(2) > .b1-r > div.radio > .radio > .ng-pristine').click() //click yes for Does anyone in the home smoke?
            cy.get(':nth-child(3) > .b1-r > div.radio > .radio > .ng-pristine').click() //click yes for Are oxygen signs posted in the appropriate places?
            cy.get(':nth-child(4) > .b1-r > div.radio > .radio > .ng-pristine').click() //click yes for Are oxygen tanks/concentrators stored safely?
            cy.get('.p-t-5 > div.radio > .radio > .ng-pristine').click() //click yes for Is backup O2 tank available?
            cy.get('[width="142px"] > .inner__table-nb > tbody > :nth-child(1) > .p-5 > .radio > :nth-child(1) > .ng-pristine').click() //click yes for Does patient/PCG know how to use backup O2?
            cy.get(':nth-child(2) > .p-5 > div.radio > .radio > .ng-pristine').click() //click yes for Are all cords near or related to oxygen intact, secure & properly used?
            cy.get(':nth-child(3) > .p-5 > div.radio > :nth-child(1) > .ng-pristine').click() //click yes for Is patient educated re: substances that can cause O2 to be flammable?
            //Are there potential sources of open flames identified? (select all)
            cy.get('[opt="data.RESP0191"] > .ng-pristine').click()
            cy.get('[opt="data.RESP0192"] > .ng-pristine').click()
            cy.get('[opt="data.RESP0193"] > .ng-pristine').click()
            cy.get('[opt="data.RESP0194"] > .ng-pristine').click()
            cy.get('[opt="data.RESP0195"] > .ng-pristine').click()
            cy.get(':nth-child(4) > .p-5 > div.radio > .radio > .ng-pristine').click() //click yes for Are there potential sources of open flames identified?
            cy.get('.b1-t > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get('[width="142px"] > .table-rt > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Oxygen Theraphy
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(11) > tr:nth-child(2) > td > table > tr:nth-child(1) > td:nth-child(2) > div > label > input').click() //clicking type
            //Oxygen delivery (select all)
            cy.get(':nth-child(3) > .oasis__answer > .inner__table-nb > :nth-child(1) > [style="width: 17%;"] > div > .checkbox > .ng-scope').click()
            cy.get(':nth-child(2) > [style="width: 17%;"] > div > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .p-5 > div > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > .oasis__answer > .inner__table-nb > :nth-child(1) > :nth-child(3) > div > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(2) > :nth-child(2) > div > .checkbox > .ng-scope').click()
            cy.get('.table-rt > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting other oxygen delivery
            cy.get('.p-l-0 > .global__txtbox').type('10') //inputting liters/minute
            //Oxygen source (select all)
            cy.get(':nth-child(5) > .oasis__answer > .inner__table-nb > tr > :nth-child(2) > div > .checkbox > .ng-scope').click()
            cy.get(':nth-child(5) > .oasis__answer > .inner__table-nb > tr > :nth-child(3) > div > .checkbox > .ng-scope').click()
            cy.get('.d-flex > [style="display: flex;"] > .fg-line > .global__txtbox').type('Test') //inputting other for oxygen source
            cy.get(':nth-child(6) > .oasis__answer > .inner__table-nb > tr > [style="width: 17%;"] > div > :nth-child(1) > .ng-pristine').click() //clicking yes for backup O2 tank
            cy.get(':nth-child(3) > .checkbox > .ng-pristine').click() //clicking vendor notified
            cy.get(':nth-child(7) > .oasis__answer > .inner__table-nb > :nth-child(1) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking for observation
            cy.get('.inner__table-nb > :nth-child(2) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking for intervention
            cy.wait(5000)
            //Other form for Tracheostomy
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(12) > tr:nth-child(2) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting brand/model
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(12) > tr:nth-child(2) > td > table > tr:nth-child(2) > td:nth-child(2) > input').type('4')
            cy.get('#RESP0049').type('12042021') //inputting date last change (mm/dd/yyyy)
            cy.get('.inner__table-nb > :nth-child(4) > :nth-child(2) > .global__txtbox').type('Test') //inputting inner cannula
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(12) > tr:nth-child(3) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting observation
            cy.get(':nth-child(2) > [style="vertical-align:top;"] > .fg-line > .global__txtbox').type('Test') //inputting intervention
            //Other form for BiPAP/CPAP
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(13) > tr:nth-child(2) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting device brand/model
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(13) > tr:nth-child(2) > td > table > tr:nth-child(2) > td.p-5.b1-b.ng-isolate-scope > label:nth-child(1) > input').click() //clicking yes for Device working properly?
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(13) > tr:nth-child(2) > td > table > tr:nth-child(3) > td.p-5.ng-isolate-scope > label:nth-child(1) > input').click() //clicking yes for Compliant with use of device?\
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(13) > tr:nth-child(3) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(13) > tr:nth-child(3) > td > table > tr:nth-child(2) > td:nth-child(2) > div > input').type('Test') //inputting intervention
            cy.wait(5000)
            //Other form for Suctioning
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(14) > tr:nth-child(2) > td > table > tr:nth-child(1) > td > label:nth-child(1) > input').click() //clicking oral
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(14) > tr:nth-child(2) > td > table > tr:nth-child(2) > td > label.radio.radio-inline.ng-binding.ng-scope.ng-isolate-scope.m-l-15 > input').click() //clicking yes for Is the person performing the suctioning proficient? 
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(14) > tr:nth-child(2) > td > table > tr:nth-child(3) > td > label:nth-child(2) > input').click() //clicking yes for Is the suction equipment setup always ready for use?
            //Suction procedure done by (select all)
            cy.get('.inner__table-nb > :nth-child(4) > .p-5 > .checkbox > :nth-child(1) > .ng-scope').click()
            cy.get('.checkbox > :nth-child(2) > .ng-scope').click()
            cy.get('.checkbox > :nth-child(3) > .ng-scope').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(14) > tr:nth-child(3) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(14) > tr:nth-child(3) > td > table > tr:nth-child(2) > td:nth-child(2) > div > input').type('Test') //inputting intervention
            //Other form for Ventilator
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting Brand/Model
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(2) > td:nth-child(2) > div > input').type('Test') //inputting Tidal volume
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(3) > td:nth-child(2) > div > input').type('Test') //inputting FiO2
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(4) > td:nth-child(2) > div > input').type('Test') //inputting Assist control
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(1) > td:nth-child(4) > div > input').type('Test') //inputting PEEP
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(2) > td:nth-child(4) > div > input').type('Test') //inputting SIMV	
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(3) > td:nth-child(4) > div > input').type('Test') //inputting Pressure control
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(2) > td > table > tr:nth-child(4) > td:nth-child(4) > div > input').type('Test') //inputting PRVC
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(3) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(15) > tr:nth-child(3) > td > table > tr:nth-child(2) > td:nth-child(2) > div > input').type('Test') //inputting intervention
            cy.wait(5000)
            //Other form for PleurX
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(2) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('12042021') //inputting date catheter inserted (mm/dd/yyyy)
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(2) > td > table > tr:nth-child(2) > td.p-5.b1-b.ng-isolate-scope > label:nth-child(1) > input').click() //clicking daily for drainage frequency
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(2) > td > table > tr:nth-child(2) > td.p-5.b1-b.ng-isolate-scope > input').type('Test') //inputting other for drainage frequency
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(2) > td > table > tr:nth-child(3) > td:nth-child(2) > input').type('2') //inputting ml for amount drained
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(2) > td > table > tr:nth-child(3) > td:nth-child(2) > label > input').click() //clicking done
            //Procedure done by (select all)
            cy.get(':nth-child(4) > :nth-child(2) > div.ng-isolate-scope > :nth-child(1) > .ng-scope').click()
            cy.get('div.ng-isolate-scope > :nth-child(2) > .ng-scope').click()
            cy.get('div.ng-isolate-scope > :nth-child(3) > .ng-scope').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(3) > td > table > tr:nth-child(1) > td:nth-child(2) > div > input').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(16) > tr:nth-child(3) > td > table > tr:nth-child(2) > td:nth-child(2) > div > input').type('Test') //inputting intervention
            cy.wait(5000)
            //Cardiovascular
            cy.get('.inner__table-wb > tbody > tr > :nth-child(1) > div > .radio > .ng-pristine').click() //clicking regular for Heart Rhythm
            cy.get('#cardiovascular > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td.p-5.b1-b > label > input').click() //clicking < 3 sec for Capillary refill
            cy.get(':nth-child(3) > .b1-b > span.ng-isolate-scope > .radio > .ng-pristine').click() //clicking yes for JVD	
            cy.get(':nth-child(4) > .b1-b > span.ng-isolate-scope > .radio > .ng-pristine').click() //clicking yes for Peripheral edema
            cy.get(':nth-child(5) > .b1-b > span.ng-isolate-scope > .radio > .ng-pristine').click() //clicking yes for Chest Pain
            cy.get(':nth-child(6) > .b1-b > :nth-child(2) > .radio > .ng-pristine').click() //clicking yes for Cardiac device
            cy.get('.b1-b > .select > .chosen-container > .chosen-single').click() //clicking dropdown for cardiac device result
            cy.get('#cardiovascular > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td.p-5.b1-b > span.select.global__select.display-ib > div > div > ul > li:nth-child(1)').click() //clicking result on cardiac device
            cy.get(':nth-child(7) > :nth-child(2) > :nth-child(2) > .ng-valid').click() //clicking yes for weight gain
            cy.get(':nth-child(7) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for weight gain
            cy.wait(5000)
            //Pulses
            cy.get(':nth-child(8) > :nth-child(1) > .inner__table-wb > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking pedal-left for Bounding
            cy.get('.inner__table-wb > tbody > :nth-child(3) > :nth-child(3) > .radio > .ng-pristine').click() //clicking pedal-right for Bounding
            cy.get('.inner__table-wb > tbody > :nth-child(3) > :nth-child(4) > .radio > .ng-pristine').click() //clicking Popliteal-left for Bounding
            cy.get('.inner__table-wb > tbody > :nth-child(3) > :nth-child(5) > .radio > .ng-pristine').click() //clicking Popliteal-right for Bounding
            cy.get('tbody > :nth-child(3) > :nth-child(6) > .radio > .ng-pristine').click() //clicking Femoral-left for Bounding
            cy.get(':nth-child(3) > :nth-child(7) > .radio > .ng-pristine').click() //clicking Femoral-right for Bounding
            cy.get(':nth-child(3) > :nth-child(8) > .radio > .ng-pristine').click() //clicking Brachial-left for Bounding
            cy.get(':nth-child(3) > :nth-child(9) > .radio > .ng-pristine').click() //clicking Brachial-right for Bounding
            cy.get(':nth-child(3) > :nth-child(10) > .radio > .ng-pristine').click() //clicking Radial-left for Bounding
            cy.get(':nth-child(3) > :nth-child(11) > .radio > .ng-pristine').click() //clicking Radial-right for Bounding
            cy.get(':nth-child(19) > :nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(19) > :nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Peripheral edema
            cy.get('.inner__table-wb > :nth-child(2) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Pedal edema - Left
            cy.get('.inner__table-wb > :nth-child(3) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Pedal edema - Right	
            cy.get('.inner__table-wb > :nth-child(4) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Ankle edema - Left	
            cy.get(':nth-child(5) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Ankle edema - Right	
            cy.get('.inner__table-wb > :nth-child(6) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Leg edema - Left
            cy.get(':nth-child(7) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Leg edema - Right
            cy.get(':nth-child(8) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Sacral edema	
            cy.get(':nth-child(9) > :nth-child(2) > div > .radio > .ng-pristine').click() //clicking +1 for Generalized edema
            cy.get(':nth-child(3) > .p-0 > .table-rt > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(3) > .p-0 > .table-rt > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Chest Pain
            //Character (select all)
            cy.get('.inner__table-nb > :nth-child(1) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(2) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(3) > [style="width: 10%;"] > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(1) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(2) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(3) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(1) > :nth-child(4) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.inner__table-nb > :nth-child(2) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(1) > .p-l-5 > :nth-child(1) > .ng-pristine').click() //clicking left for radiating to shoulder
            cy.get(':nth-child(2) > .p-l-5 > :nth-child(1) > .ng-pristine').click() //clicking left for radiating to jaw
            cy.get(':nth-child(3) > .p-l-5 > :nth-child(1) > .ng-pristine').click() //clicking left for radiating to neck
            cy.get(':nth-child(4) > .p-l-5 > :nth-child(1) > .ng-pristine').click() //clicking left for radiating to arm
            //Accompanied by (Select all)
            cy.get('.inner__table-nb > :nth-child(5) > .p-5.ng-isolate-scope').click()
            cy.get('.p-5.ng-isolate-scope > .m-l-10 > .ng-pristine').click()
            cy.get(':nth-child(6) > .ng-isolate-scope > .fg-line > .global__txtbox').type('Test') //inputting frequency of pain
            cy.get(':nth-child(7) > .ng-isolate-scope > .fg-line > .global__txtbox').type('Test') //inputting duration of pain
            cy.get('.b1-b > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(1) > .p-0 > .table-rt > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            //Pace Maker
            cy.get('[style="width: 25%;"] > .fg-line > .global__txtbox').type('Test') //inputting brand/model
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(22) > tr:nth-child(2) > td > table > tr:nth-child(2) > td:nth-child(2) > div > input').type('20') //inputting Rate setting
            cy.get(':nth-child(1) > :nth-child(3) > .fg-line > .global__txtbox').type('12/24/2021') //inputting Date implanted
            cy.get(':nth-child(2) > :nth-child(3) > .fg-line > .global__txtbox').type('12/03/2021') //inputting Date last tested
            cy.get('[colspan="2"] > .radio > .ng-pristine').click() //clicking no
            cy.get('[colspan="2"] > .fg-line > .global__txtbox').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(22) > tr:nth-child(3) > td > table > tr > td:nth-child(2) > div > input').type('Test') //inputting intervention
            cy.wait(5000)

            //Upper GI Status
            cy.get(':nth-child(1) > :nth-child(2) > .radio > .p-r-15 > .ng-pristine').click() //click soft for abdomen
            cy.get('.p-l-25 > .global__txtbox').type('2') //inputting inches in girt
            cy.get(':nth-child(2) > .ng-scope > .radio > .ng-pristine').click() //clicking active for bowel sounds
            cy.get(':nth-child(2) > :nth-child(1) > .ng-scope > .radio > .ng-pristine').click() //clicking good for appetite
            cy.wait(5000)
            //Other sypmtoms (select all)
            cy.get(':nth-child(7) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(8) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(9) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(7) > :nth-child(3) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(8) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(9) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.p-l-30 > [style="display: flex;"] > .fg-line > .global__txtbox').type('Test') //inputting other symptom
            cy.get(':nth-child(26) > :nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(26) > :nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Other form for Vomiting
            cy.get(':nth-child(1) > :nth-child(2) > .inner__table-nb > tbody > tr > :nth-child(1) > .radio > .p-l-25 > .ng-valid').click() //clicking projectile for type
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(27) > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td > div > label:nth-child(1) > input').click() //clicking small for amount
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.oasis__table.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody:nth-child(27) > tr:nth-child(2) > td > table > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr > td.p-5 > div > label:nth-child(1) > input').click() //clicking watery for character\
            cy.get(':nth-child(2) > [style="display: flex;"] > .fg-line > .global__txtbox').type('Test') //inputting other character
            cy.get('.oasis__answer > :nth-child(1) > :nth-child(1) > :nth-child(4) > .p-5 > .fg-line > .global__txtbox').type('Test') //inputting frequency
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(1) > [style="font-weight: normal;"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > :nth-child(2) > .b1-b > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Nutrition/Diet (select all)
            cy.get('.inner__table-nb > tbody > :nth-child(1) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('.inner__table-nb > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('tbody > :nth-child(4) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('.inner__table-nb > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('.inner__table-nb > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(6) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('.inner__table-nb > tbody > :nth-child(1) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get('[colspan="2"] > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(1) > :nth-child(4) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(4) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(3) > :nth-child(4) > .checkbox > label > .ng-scope').click()
            cy.get('.checkbox > .m-b-5 > .ng-scope').click()
            cy.get('div.pull-left > .fg-line > .global__txtbox').type('6') //inputting ml forfluid restriction
            cy.get(':nth-child(4) > .cont-opt > .global__txtbox').type('Test') //inputting other nutrition/diet
            cy.wait(5000)

            //Enteral Nutrition
            cy.get('tbody > :nth-child(1) > :nth-child(2) > :nth-child(3) > .ng-pristine').click() //clicking dobhoff for feeding via
            cy.get('.table-rt-v > tbody > :nth-child(2) > :nth-child(2) > .global__txtbox').type('12/06/2021') //inputting tube insertion date (mm/dd/yyyy)
            cy.get('.table-rt-v > tbody > :nth-child(3) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking pump for formula delivery system
            cy.get('[name="NUTRI0014"]').type('4') //inputting amout for feeding formula
            cy.get('[name="NUTRI0015"]').type('4') //inputting ml for feeding formula
            cy.get('[name="NUTRI0016"]').type('4') //inputting amout for liquid supplemet
            cy.get('[name="NUTRI0017"]').type('4') //inputting ml for liquid supplemet
            cy.get('[name="NUTRI0020"]').type('4') //inputting ml for Pump rate per hour
            cy.get('[name="NUTRI0021"]').type('24') //inputting hours per day for Pump rate per hour
            cy.get('.table-rt-v > tbody > :nth-child(7) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking open system for enteral feeding system
            cy.get('[name="NUTRI0038"]').type('4') //inputting hours if residual volume over for hold feeling for
            cy.get('[name="NUTRI0039"]').type('4') //inputting ml for hold feeling for
            cy.get(':nth-child(9) > :nth-child(2) > .global__txtbox').type('4') //inputting ml for Gastric residual amount
            cy.get(':nth-child(10) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for Tolerating feedings well?
            cy.get(':nth-child(11) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking yes for NPO?
            cy.get('.table-rt-v > tbody > :nth-child(11) > :nth-child(2) > .global__txtbox').type('Test') //inputting NPO
            cy.wait(5000)
            //Ostomy care/feedings by: (select all)
            cy.get(':nth-child(12) > :nth-child(2) > .checkbox > :nth-child(1) > .ng-scope').click()
            cy.get(':nth-child(2) > .checkbox > :nth-child(2) > .ng-scope').click()
            cy.get(':nth-child(2) > .checkbox > :nth-child(3) > .ng-scope').click()
            cy.get(':nth-child(13) > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(14) > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Lower GI Status
            cy.get('#LOGAST0001').type('12/04/2021') //inputting date last BM (mm/dd/yyyy)
            cy.wait(5000)
            //Bowel movement (select all)
            cy.get('.table-rt.b1-b > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label.ng-isolate-scope > .ng-pristine').click()
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > label.ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(3) > .checkbox > label.ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(2) > :nth-child(2) > .checkbox > label.ng-isolate-scope > .ng-pristine').click()
            cy.get('.table-rt.b1-b > tbody > :nth-child(2) > :nth-child(2) > .global__txtbox').type('5')
            cy.get('[colspan="3"] > .checkbox > label.ng-isolate-scope > .ng-pristine').click()
            cy.get('.table-rt.b1-b > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-isolate-scope > .ng-pristine').click() //clicking soft for stool character
            cy.get(':nth-child(3) > [colspan="3"] > .m-l-40 > .fg-line > .global__txtbox').type('Test') //inputting other for stool character
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(1) > td > table.table-rt.b1-b > tbody > tr:nth-child(5) > td:nth-child(2) > div > label > input').click() //clicking yellow/brown for stool color
            cy.get(':nth-child(5) > [colspan="3"] > .m-l-40 > .fg-line > .global__txtbox').type('Test') //inputting other for stool character
            cy.get(':nth-child(8) > .b1-b.ng-isolate-scope > .radio > .ng-isolate-scope > .ng-pristine').click() //clicking effective for
            cy.get('.pull-left > label.ng-isolate-scope > .ng-pristine').click() //clicking MD notified for
            cy.get(':nth-child(8) > :nth-child(2) > :nth-child(2) > .fg-line > .global__txtbox').type('Test') //inputting MD notified
            cy.get(':nth-child(10) > td.ng-isolate-scope > .radio > .ng-isolate-scope > .ng-pristine').click() //clicking as needed for Laxative/Enema
            cy.get(':nth-child(10) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for Laxative/Enema
            cy.get(':nth-child(11) > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(12) > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Lower GI Ostomy
            cy.get(':nth-child(1) > #LOGASTO0001').click() //clicking colostomy for ostomy type
            cy.get('.fg-line > .d-ib > .global__txtbox').type('12') //inputting cm for stoma diameter
            cy.get('[style="margin-right: 52px;"] > .ng-pristine').click() //clicking healed for ostomy wound
            //Care done by (select all)
            cy.get('tr.p-0 > .p-0 > .table-rt > tbody > tr > td > :nth-child(1) > .ng-scope').click()
            cy.get('tr.p-0 > .p-0 > .table-rt > tbody > tr > td > :nth-child(2) > .ng-scope').click()
            cy.get('tr.p-0 > .p-0 > .table-rt > tbody > tr > td > :nth-child(3) > .ng-scope').click()
            cy.get('tr.b1-b > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') //inputting observation
            cy.get('tr.b1-b > :nth-child(1) > :nth-child(1) > :nth-child(1) > :nth-child(6) > :nth-child(2) > .global__txtbox').type('Test') //inputting intervention
            //Genitourinary Status
            //Urine clarity (select all except clear)
            cy.get('#genitourinary_status > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('[style="width: 18%;"] > .checkbox > label > .ng-scope').click()
            cy.get('#genitourinary_status > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#genitourinary_status > tbody > :nth-child(1) > :nth-child(4) > .d-ib > .fg-line > .global__txtbox').type('Test') //inputting other for urine clarity
            //Urine color
            cy.get('#genitourinary_status > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking straw for urine color
            cy.get(':nth-child(3) > .global__txtbox').type('Test') //inputting other urine color
            //Urine Odor
            cy.get('.m-t-1 > .radio > .ng-pristine').click() //clicking yes for urine odor
            cy.get('.m-t-1 > .d-ib > div > .global__txtbox').type('Test') //inputting description
            cy.wait(5000)
            //Abnormal elimination
            cy.get('.sub-th > td.ng-isolate-scope > :nth-child(2) > .ng-pristine').click() //clicking yes Abnormal elimination
            //Abnormal elimination (select all)
            cy.get('.b1-t > .p-0 > .table-rt > tbody > tr > :nth-child(1) > :nth-child(1) > label > .ng-scope').click()
            cy.get(':nth-child(1) > :nth-child(2) > label > .ng-scope').click()
            cy.get(':nth-child(1) > :nth-child(3) > label > .ng-scope').click()
            cy.get(':nth-child(1) > :nth-child(4) > label > .ng-scope').click()
            cy.get(':nth-child(1) > :nth-child(5) > label > .ng-scope').click()
            cy.get('.b1-t > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > label > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(2) > label > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(3) > label > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(4) > label > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(5) > label > .ng-scope').click()
            cy.get('.m-t-5 > .fg-line > .global__txtbox').type('Test') //inputting other for abnormal elimination
            cy.wait(5000)
            //Special procedures
            cy.get('.sub-th > :nth-child(3) > .radio > .ng-pristine').click() //clicking yes for Special procedures
            //Special procedures (select all)
            cy.get('#GENSTAS0038').click()
            cy.get('#GENSTAS0092').click()
            cy.get('#GENSTAS0093').click()
            cy.get('#GENSTAS0094').click()
            cy.get('#GENSTAS0091').click()
            cy.get('#GENSTAS0059').click()
            cy.get('#GENSTAS0095').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(5) > tbody > tr:nth-child(4) > td:nth-child(2) > div > input').type('Test') //inputting other for special procedure
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(5) > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(5) > tbody > tr:nth-child(6) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
            cy.wait(5000)
            //Indwelling catheter
            cy.get(':nth-child(1) > .radio > #GENSTAS0009').click() //clicking urethral for catheter type
            cy.get(':nth-child(1) > :nth-child(2) > .p-0 > :nth-child(1) > :nth-child(1) > :nth-child(2) > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking 14 for catheter size
            cy.get(':nth-child(3) > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking 5 for balloon inflation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div:nth-child(3) > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking 2-way for catheter lumens
            cy.get('[style="width: 19%;"] > .fg-line > .global__txtbox').type('3') //inputting days for catheter change
            cy.get('#GENSTAS0028').type('05232021') //date last changed for catheter change
            cy.get('.m-l-10 > .radio > .ng-pristine').click() //clicking done for catherter change
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div:nth-child(3) > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label:nth-child(1) > input').click() //clicking bedside bag for drainage bag
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div:nth-child(3) > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(6) > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(6) > td:nth-child(2) > label:nth-child(2) > input').click() //clicking leg bag for drainage bag
            cy.get('[ng-if="data.GENSTAS0038"] > :nth-child(1) > :nth-child(3) > :nth-child(2) > .global__txtbox').type('3') //inputting days for MD-ordered irrigation frequency
            cy.get(':nth-child(3) > :nth-child(2) > :nth-child(4) > .ng-pristine').click() //clicking as needed for MD-ordered irrigation frequency
            cy.get('[opt="data.GENSTAS0099"] > .ng-pristine').click() //clicking done for MD-ordered irrigation frequency\
            cy.get('[opt="data.GENSTAS0101"] > .ng-pristine').click() //clicking none for MD-ordered irrigation frequency
            cy.get('[ng-if="data.GENSTAS0038"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .m-r-5').type('24') //inputting amount for MD-ordered irrigation solution
            cy.get('[ng-if="data.GENSTAS0038"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .m-l-5').type('4') //inputting ml for MD-ordered irrigation solution
            cy.get('[ng-if="data.GENSTAS0038"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine').click() //clicking none for MD-ordered irrigation solution
            cy.get('[ng-if="data.GENSTAS0038"] > :nth-child(1) > :nth-child(5) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get('[ng-if="data.GENSTAS0038"] > :nth-child(1) > :nth-child(6) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Intermittent Catheterization
            cy.get('[ng-if="data.GENSTAS0092"] > :nth-child(1) > :nth-child(2) > :nth-child(2) > .table-rt > tbody > tr > :nth-child(2) > .radio > .ng-pristine').click() //clicking 16 for catheter size
            cy.get(':nth-child(2) > .fg-line > .ng-valid-maxlength').type('3') //inputting for frequency
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(7) > tbody > tr:nth-child(3) > td:nth-child(2) > div > label:nth-child(2) > input').click() //clicking day for frequency
            cy.get('.fg-line > .m-l-7').type('Test') //inputting other for frequency
            //Done By (select all)
            cy.get('[ng-if="data.GENSTAS0092"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .checkbox > :nth-child(1) > .ng-scope').click()
            cy.get(':nth-child(4) > :nth-child(2) > .checkbox > :nth-child(2) > .ng-scope').click()
            cy.get(':nth-child(4) > :nth-child(2) > .checkbox > :nth-child(3) > .ng-scope').click()
            cy.get('[ng-if="data.GENSTAS0092"] > :nth-child(1) > :nth-child(5) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get('[ng-if="data.GENSTAS0092"] > :nth-child(1) > :nth-child(6) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Nephrostomy
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(2) > :nth-child(2) > .global__txtbox').type('3') //inputting days for Nephrostomy dressing change
            cy.get('[opt="data.GENSTAS0079"] > .ng-pristine').click() //clicking as needed for Nephrostomy dressing change
            cy.get('[opt="data.GENSTAS0096"] > .ng-pristine').click() //clicking done for Nephrostomy dressing change
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(3) > :nth-child(2) > .global__txtbox').type('3') //inputting days for Nephrostomy bag change
            cy.get('[opt="data.GENSTAS0080"] > .ng-pristine').click() //clicking as needed for Nephrostomy bag change
            cy.get('[opt="data.GENSTAS0097"] > .ng-pristine').click() //clicking done for Nephrostomy bag change
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(4) > :nth-child(2) > .global__txtbox').type('3') //inputting days for MD-ordered irrigation frequency
            cy.get('[opt="data.GENSTAS0071"] > .ng-pristine').click() //clicking as needed for MD-ordered irrigation frequency
            cy.get('[opt="data.GENSTAS0098"] > .ng-pristine').click() //clicking done for MD-ordered irrigation frequency
            cy.get('[opt="data.GENSTAS0106"] > .ng-pristine').click() //clicking none for  MD-ordered irrigation frequency
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(5) > :nth-child(2) > .m-r-5').type('3') //inputting amount for MD-ordered irrigation solution
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(5) > :nth-child(2) > .m-l-5').type('3') //inputting ml for MD-ordered irrigation solution
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(5) > :nth-child(2) > .radio > .ng-pristine').click() //clicking none for MD-ordered irrigation solution
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(6) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get('[ng-if="data.GENSTAS0093"] > :nth-child(1) > :nth-child(7) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Urostomy
            cy.get('[style="width: 60%;"] > .global__txtbox').type('3') //inputting days for Urostomy pouch change frequency:
            cy.get('[ng-if="data.GENSTAS0094"] > tbody > :nth-child(3) > .b1-b > :nth-child(1) > .fg-line > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(4) > td > :nth-child(1) > .fg-line > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //External Urinary Catheter
            cy.get('[ng-if="data.GENSTAS0091"] > :nth-child(1) > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking daily for Change frequency
            cy.get('[ng-if="data.GENSTAS0091"] > :nth-child(1) > :nth-child(2) > :nth-child(2) > .global__txtbox').type('Test') //inputting other in Change frequency
            cy.get('[ng-if="data.GENSTAS0091"] > :nth-child(1) > :nth-child(4) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get('[ng-if="data.GENSTAS0091"] > :nth-child(1) > :nth-child(5) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Hemodialysis
            cy.get('.b1-l > .table-rt > tbody > tr > :nth-child(1) > .radio > .ng-pristine').click() //clicking av shunt for AV access
            cy.get(':nth-child(2) > .d-ib > .fg-line > .global__txtbox').type('Test') //inputting location for AV access
            cy.get('.b1-l > .table-rt > tbody > tr > :nth-child(3) > .radio > .ng-pristine').click() //clicking Permacath for AV access
            cy.get('[width="50%"] > .d-ib > .fg-line > .global__txtbox').type('Test') //inputting other in AV access
            cy.get(':nth-child(1) > #HEMO0007').click() //clicking yes for Bruit present?
            cy.get(':nth-child(1) > #HEMO0009').click() //clicking yes for Thrill strong?
            cy.wait(5000)
            //Dialysis schedule (select all)
            cy.get('.b1-l > :nth-child(1) > .ng-scope').click()
            cy.get('.b1-l > :nth-child(2) > .ng-scope').click()
            cy.get('.b1-l > :nth-child(3) > .ng-scope').click()
            cy.get('.b1-l > :nth-child(4) > .ng-scope').click()
            cy.get('.b1-l > :nth-child(5) > .ng-scope').click()
            cy.get('.b1-l > :nth-child(6) > .ng-scope').click()
            cy.get('.b1-l > :nth-child(7) > .ng-scope').click()
            cy.get('[style="width:260px;"] > .global__txtbox').type('Test') //inputting dialysis center
            cy.get('[style="margin-left:-230px"] > .global__txtbox').type('09123456789') //inputting phone
            cy.get('[ng-if="data.GENSTAS0059"] > :nth-child(1) > :nth-child(7) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(8) > .p-0 > .table-rt > tbody > tr > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            //Peritoneal Dialysis
            cy.get(':nth-child(1) > .b1-b > div > .radio > #PERDIA0006').click() //clicking type
            cy.get('[style="height: 33px"] > :nth-child(2) > .global__txtbox').type('Test') //inputting APD machine
            //Dialysate (select all)
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(1) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(2) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(3) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(2) > label:nth-child(4) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div:nth-child(1) > input').type('12:00') //inputting dwell time
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div:nth-child(2) > input').type('4') //inputting hours
            //Peritoneal dialysis done by (select all)
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label:nth-child(1) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label:nth-child(2) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr:nth-child(4) > td:nth-child(2) > div > label:nth-child(3) > input').click()
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(3) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting observation
            cy.get('#parent > div > div > form > fieldset > fieldset > div > div > table.global__table.table-rt.b1-0.sticky-header.w-100.ng-scope.ng-isolate-scope > tbody > tr:nth-child(2) > td > table:nth-child(12) > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > input').type('Test') //inputting intervention
            
            //Save Button
            cy.get('.btn__success').click() //clicking save button
            cy.wait(5000)
            // Cardiopulmonary/Nutrition/Elimination TAB End -------------
        })

        it('Automating Entry Form of Neurologic/Musculoskeletal', function() {

            // Neurologic/Musculoskeletal TAB Start -------------
            cy.get(':nth-child(3) > .oasis-section-tabs').click() //clicking cardiopulmonary tab
            cy.wait(5000)
            //Neurological Status
            cy.get(':nth-child(2) > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking left>right for size unequal
            cy.get('.p-r-15 > .ng-pristine').click() //clicking left for non-reactive
            //Mental status (select all)
            cy.get('.m-r-10 > .ng-scope').click()
            cy.get('span.ng-isolate-scope > :nth-child(1) > .ng-scope').click()
            cy.get('span.ng-isolate-scope > :nth-child(2) > .ng-scope').click()
            cy.get('span.ng-isolate-scope > :nth-child(3) > .ng-scope').click()
            cy.get('span.ng-isolate-scope > :nth-child(4) > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(6) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(7) > :nth-child(1) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(6) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get(':nth-child(7) > :nth-child(2) > .m-0 > .checkbox > .ng-scope').click()
            cy.get('.checkbox > .radio > .ng-valid').click() //clicking adequate for sleep/rest
            cy.get('.b1-b.ng-isolate-scope > .d-ib > label.ng-isolate-scope > .ng-pristine').click() //clicking MD notified for sleep/rest
            //Hand grips
            cy.get(':nth-child(9) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking left for strong
            cy.get(':nth-child(9) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking right for strong
            cy.get(':nth-child(10) > :nth-child(2) > .checkbox > .ng-pristine').click() //clicking left for weak
            cy.get(':nth-child(10) > :nth-child(3) > .checkbox > .ng-pristine').click() //clicking right for weak
            cy.wait(5000)
            //Other signs (select all)
            cy.get(':nth-child(11) > :nth-child(2) > .checkbox > .ng-pristine').click()
            cy.get('[chk="data.NEUSTAT0042"] > .ng-pristine').click()
            cy.get('[chk="data.NEUSTAT0044"] > .ng-pristine').click()
            cy.get('[chk="data.NEUSTAT0043"] > .ng-pristine').click()
            cy.get(':nth-child(3) > .fg-line > .global__txtbox').type('Test') //inputting other test
            cy.wait(5000)
            //Weakness
            cy.get('[chk="data.NEUSTAT0014"] > .ng-valid').click() //clicking left for Upper extremity
            cy.get('[chk="data.NEUSTAT0015"] > .ng-valid').click() //clicking right for Upper extremity
            cy.get('[chk="data.NEUSTAT0016"] > .ng-pristine').click() //clicking left for Lower extremity
            cy.get('[chk="data.NEUSTAT0017"] > .ng-pristine').click() //clicking right for Lower extremity
            cy.get(':nth-child(14) > :nth-child(2) > :nth-child(1) > .ng-valid').click() //clicking left for Hemiparesis 
            //Paralysis
            cy.get(':nth-child(15) > :nth-child(3) > :nth-child(1) > .ng-pristine').click() //clicking left for hemiplegia
            cy.get(':nth-child(16) > :nth-child(1) > .radio > .ng-valid').click() //clicking Paraplegia
            //Tremors
            cy.get('[chk="data.NEUSTAT0022"] > .ng-pristine').click() //clicking left for Upper extremity
            cy.get('[chk="data.NEUSTAT0023"] > .ng-pristine').click() //clicking right for Upper extremity
            cy.get(':nth-child(3) > .d-ib > :nth-child(1) > .ng-pristine').click() //clicking fine for Upper extremity
            cy.get('[chk="data.NEUSTAT0025"] > .ng-pristine').click() //clicking left for Lower extremity
            cy.get('[chk="data.NEUSTAT0026"] > .ng-pristine').click() //clicking right for Lower extremity
            cy.get(':nth-child(19) > :nth-child(2) > .d-ib > :nth-child(1) > .ng-pristine').click() //clicking fine for Lower extremity
            //Seizure
            cy.get('[colspan="2"] > :nth-child(1) > .ng-isolate-scope > .ng-pristine').click() //clicking grand mal
            cy.get('[data-ng-model="data.NEUSTAT0021"]').type('12/06/2021') //inputting date of last seizure
            cy.get('[data-ng-model="data.NEUSTAT0056"]').type('50') //inputting duration (in seconds)
            cy.get(':nth-child(22) > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(23) > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            // Knowledge Deficit (select all)
            cy.get('[style="width:36%"] > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get('tbody > :nth-child(2) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get('[style="width:30%"] > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(2) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(3) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(4) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(5) > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(6) > :nth-child(2) > .global__txtbox').type('Test') //inputting other in Knowledge Deficit
            cy.wait(5000)
            //Thought Process, Affect and Behavioral Status (select all)
            cy.get('[width="36%"] > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(4) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(5) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(6) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('[width="30%"] > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#cognitive_behavioral_psychological > tbody > :nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') //inputting other in Thought Process, Affect and Behavioral Status
            cy.wait(5000)
            //Musculoskeletal Status
            cy.get('[ng-disabled="data.MUSCOLO0001"] > :nth-child(1) > :nth-child(1) > :nth-child(1) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking strong for muscle strength
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .m-r-5 > .ng-pristine').click() //clicking limited for Range of motion\
            //limited (select all)
            cy.get(':nth-child(2) > :nth-child(3) > div.ng-isolate-scope > .ng-scope').click()
            cy.get(':nth-child(4) > div.ng-isolate-scope > .ng-scope').click()
            cy.get(':nth-child(2) > :nth-child(5) > div.ng-isolate-scope > .ng-scope').click()
            cy.get(':nth-child(6) > div.ng-isolate-scope > .ng-scope').click()
            cy.get(':nth-child(3) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking independent for Bed mobility
            cy.get(':nth-child(4) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking independent for Transfer ability
            cy.get(':nth-child(5) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking steady for Gait/Ambulation
            cy.get(':nth-child(6) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking good for balance
            cy.get(':nth-child(7) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .global__txtbox').type('10') //inputting seconds for Timed Up & Go
            cy.get(':nth-child(7) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(2) > .ng-isolate-scope > .ng-pristine').click() //clicking Practiced once before actual test for Timed Up & Go
            cy.get(':nth-child(7) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(3) > .ng-isolate-scope > .ng-pristine').click() //clicking Unable to perform for Timed Up & Go
            cy.get(':nth-child(8) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking low for risk for falls
            cy.get('[style="padding-right:17.5px"] > div.ng-isolate-scope > .ng-scope').click() //clicking left for amputation
            cy.get('[style="display:flex"] > :nth-child(2) > div.ng-isolate-scope > .ng-scope').click() //clicking right for amputation
            cy.get('[style="margin-left: 40.50px"] > :nth-child(1) > div.ng-isolate-scope > .ng-scope').click() //clicking BK
            cy.get('[style="margin-left: 40.50px"] > :nth-child(2) > div.ng-isolate-scope > .ng-scope').click() //clicking AK
            cy.get('[style="margin-left: 40.50px"] > :nth-child(3) > div.ng-isolate-scope > .ng-scope').click() //clicking UE
            cy.get('[style="float: right; margin-left: 30px;"] > .global__txtbox').type('Test') //inputting other for amputation
            cy.get(':nth-child(10) > td.p-0 > .table-rt > tbody > tr > :nth-child(2) > :nth-child(1) > .ng-isolate-scope > .ng-pristine').click() //clicking new for fracture
            cy.get('tr > :nth-child(2) > .d-ib > .global__txtbox').type('Test') //inputting location for fracture
            cy.get(':nth-child(10) > td.p-0 > .table-rt > tbody > tr > :nth-child(2) > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking cast?
            cy.get(':nth-child(1) > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(2) > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //If cast is present, assessment of extremity distal to cast
            cy.get('[width="30%"] > :nth-child(1) > .ng-pristine').click() //clicking pink for color
            cy.get('.table-rt.ng-scope > tbody > :nth-child(3) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking strong for pulses
            cy.get('.table-rt.ng-scope > tbody > :nth-child(4) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking < 3 sec for Capillary refill	
            cy.get('.table-rt.ng-scope > tbody > :nth-child(5) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking warm for temperature
            cy.get(':nth-child(2) > :nth-child(4) > :nth-child(1) > .ng-pristine').click() //clicking normal for sensation
            cy.get('[style="width: 300px;"] > :nth-child(1) > .ng-pristine').click() //clicking able to move for motor function
            cy.get('.table-rt.ng-scope > tbody > :nth-child(4) > :nth-child(4) > :nth-child(1) > .ng-pristine').click() //clicking yes for swelling
            cy.get(':nth-child(5) > :nth-child(4) > .global__txtbox').type('Test') //inputting for intervention
            // Functional Limitations (select all)
            cy.get('#functional_limitations > tbody > :nth-child(1) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(4) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(1) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(1) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(2) > :nth-child(3) > .checkbox > label > .ng-scope').click()
            cy.get('#functional_limitations > tbody > :nth-child(3) > :nth-child(3) > .global__txtbox').type('Test') //inputting other for functional limitations
            cy.wait(5000)
            //Activities Permitted (select all)
            cy.get('[rname="ACTI0001"]').click({multiple: true})
            cy.get(':nth-child(1) > :nth-child(4) > .global__txtbox').type('Test') //inputting others for activities permitted
            cy.wait(5000)
            //Caregiving Status
            cy.get(':nth-child(2) > :nth-child(1) > .radio > .ng-pristine').click() //clicking limited availability for caregiving status
            cy.get(':nth-child(3) > :nth-child(1) > .global__txtbox').type('Test') //inputting caregiver name
            cy.get('tbody > :nth-child(3) > :nth-child(2) > .global__txtbox').type('Test') //inputting relationship
            cy.get('[name="PSYFAC0045"]').type('1234567890') //inputting phone
            cy.get('.b1-t > td > .global__txtbox').type('Test') //inputting comments
            
            //Save Button
            cy.get('.btn__success').click() //clicking save button
            cy.wait(5000)
            // Neurologic/Musculoskeletal TAB End -------------
        })

        it('Automating Entry Form of Care Management/Interventions', function() {

            // Care Management/Interventions TAB Start -------------
            cy.get(':nth-child(4) > .oasis-section-tabs').click() //clicking cardiopulmonary tab
            cy.wait(5000)
            //Home medication management (select all)
            cy.get(':nth-child(1) > td > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get('.b1-r > .table-rt > tbody > :nth-child(2) > td > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(3) > td > .checkbox > .ng-isolate-scope > .ng-pristine').click()
            cy.get(':nth-child(1) > td.p-0 > .radio > .m-r-0 > .ng-pristine').click() //clicking yes for Medication discrepancy noted during this visit
            cy.get(':nth-child(2) > td.p-0 > .radio > .m-r-10 > .ng-pristine').click() //clicking yes for Oral medications (tablets/capsules) prepared in a pill box
            cy.get(':nth-child(3) > td.p-0 > .radio > .m-r-10 > .ng-pristine').click() //clicking yes for Use of medication schedule in taking medications
            cy.get(':nth-child(1) > [colspan="4"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(4) > tbody > :nth-child(2) > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Injectable medication(s) administered (Excludes IV medications)
            cy.get('#injectable_medication_temp > tbody > tr > th > .d-ib > .checkbox > label.ng-isolate-scope > .ng-valid').click() //clicking Injectable medication(s) administered (Excludes IV medications)
            //Form for Injectable medication(s) administered
            cy.get('tr.ng-scope > :nth-child(2) > .global__txtbox').type('Test') //inputting administered
            cy.get(':nth-child(3) > .radio > .m-r-10 > .ng-pristine').click() //clicking Intramuscular
            cy.get('tr.ng-scope > :nth-child(3) > :nth-child(2) > .global__txtbox').type('Test') //inputting site
            cy.get(':nth-child(2) > [colspan="2"] > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(3) > [colspan="2"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Venous Access and IV Therapy
            cy.get('.m-l-15 > .checkbox > .p-l-25 > .ng-pristine').click() //clicking Venous Access and IV Therapy
            // Form for Venous Access and IV Therapy
            //Access (select all)
            cy.get('[style="width:34%;"] > .cont-opt > .checkbox > .p-l-25 > .sticky-trigger').click()
            cy.get(':nth-child(2) > :nth-child(1) > .cont-opt > .checkbox > .p-l-25 > .sticky-trigger').click()
            cy.get(':nth-child(3) > td > .cont-opt > .checkbox > .p-l-25 > .sticky-trigger').click()
            cy.get(':nth-child(3) > .cont-opt > .checkbox > .p-l-25 > .sticky-trigger').click()
            cy.get('#home_medication > :nth-child(1) > :nth-child(1) > .p-0 > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for access
            cy.wait(5000)
            //Purpose (select all)
            cy.get('[rname="VAAIT0001"]').click({multiple: true})
            cy.get(':nth-child(2) > .p-0 > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for purpose
            cy.wait(5000)
            //Peripheral Venous Access
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking short for catheter type
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > .m-l-15 > .global__txtbox').type('Test') //inputting other for catheter type
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(3) > :nth-child(2) > div > :nth-child(1) > .ng-pristine').click() //clicking 24 for catheter gauge
            cy.get(':nth-child(3) > :nth-child(2) > .m-l-15 > .global__txtbox').type('Test') //inputting other for catheter gauge
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(4) > :nth-child(2) > :nth-child(1) > .ng-valid').click() //clicking left for insertion site
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(4) > :nth-child(2) > :nth-child(3) > .ng-pristine').click() //clicking arm for insertion site
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(4) > :nth-child(2) > .m-l-15 > .global__txtbox').type('Test') //inputting other for insertion site
            cy.get('#VAAIT0005').type('12122021') //inputting insertion date
            cy.get(':nth-child(5) > [style="display:flex"] > .radio > .ng-pristine').click() //clicking done
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(6) > :nth-child(2) > .global__txtbox').type('4') //inputting days for site change
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(7) > .b1-t > div > .radio > .ng-pristine').click() //clicking site condition for observation
            cy.get('[ng-if="data.VAAIT0125 && data.VAAIT0033"] > tbody > :nth-child(7) > .b1-t > div > .global__txtbox').type('Test') //inputting WNL for observation
            cy.get('[colspan="3"] > .global__txtbox').type('Test') //inputting for intervention
            cy.wait(5000)
            //Midline Catheter
            cy.get('[ng-if="data.VAAIT0130 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > div > :nth-child(1) > .ng-pristine').click() //clicking 8 cm for catheter length
            cy.get(':nth-child(2) > :nth-child(2) > div > .m-l-15 > .global__txtbox').type('Test') //inputting other for catheter length
            cy.get('[ng-if="data.VAAIT0130 && data.VAAIT0033"] > tbody > :nth-child(3) > :nth-child(2) > div > :nth-child(1) > .ng-pristine').click() //clicking 22 for catheter gauge
            cy.get(':nth-child(3) > :nth-child(2) > div > .m-l-15 > .global__txtbox').type('Test') //inputting other for catheter gauge
            cy.get('[ng-if="data.VAAIT0130 && data.VAAIT0033"] > tbody > :nth-child(4) > :nth-child(2) > .m-l-15 > .global__txtbox').type('Test') //inputting other for insertion site
            cy.get('#VAAIT0140').type('12052021') //inputting date inserted
            cy.get('[ng-if="data.VAAIT0130 && data.VAAIT0033"] > tbody > :nth-child(6) > :nth-child(2) > .global__txtbox').type('12') //inputting days dressing change
            cy.get('[ng-if="data.VAAIT0130 && data.VAAIT0033"] > tbody > :nth-child(6) > :nth-child(2) > .radio > .ng-pristine').click() //clicking done for dressing change
            cy.get(':nth-child(7) > [colspan="3"] > div > .radio > .ng-pristine').click() //clicking site condition for observation
            cy.get('[ng-if="data.VAAIT0130 && data.VAAIT0033"] > tbody > :nth-child(7) > [colspan="3"] > div > .global__txtbox').type('Test') //inputting WNL for observation
            cy.get(':nth-child(8) > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Central Venous Access
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking PICC line for CVAD catheter
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > .m-l-15 > .global__txtbox').type('Test') //inputting other for CVAD catheter
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(3) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking single for No. of lumens
            cy.get('[opt="data.VAAIT0083"] > .ng-valid').click() //clicking peripheral for insertion site
            cy.get('#VAAIT0099').type('09092021') //inputting date inserted
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(6) > :nth-child(2) > .global__txtbox').type('12') //inputting days for dressing change
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(6) > :nth-child(2) > .radio > .ng-pristine').click() //clicking done for dressing change
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(7) > :nth-child(2) > .global__txtbox').type('Test') //inputting Flushing solution
            cy.get('[ng-if="data.VAAIT0126 && data.VAAIT0033"] > tbody > :nth-child(8) > :nth-child(2) > .global__txtbox').type('12') //inputting days for flush frequency
            cy.get(':nth-child(8) > :nth-child(2) > .radio > .ng-pristine').click() //clicking done for flush frequency
            cy.get('[colspan="3"] > .fg-line > .radio > .ng-pristine').click() //clicking site condition for observation
            cy.get(':nth-child(9) > [colspan="3"] > .fg-line > .global__txtbox').type('Test') //inputing WNL for obseravtion
            cy.get(':nth-child(10) > [colspan="3"] > .fg-line > .global__txtbox').type('Test') //inputing intervention
            cy.wait(5000)
            //Implanted Port
            cy.get('[ng-if="data.VAAIT0127 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking Portacath for port device
            cy.get('[ng-if="data.VAAIT0127 && data.VAAIT0033"] > tbody > :nth-child(2) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for port device
            cy.get('[ng-if="data.VAAIT0127 && data.VAAIT0033"] > tbody > :nth-child(3) > :nth-child(2) > :nth-child(1) > .ng-pristine').click() //clicking single chamber for port reservoir
            cy.get('#home_medication > tbody > tr:nth-child(4) > td:nth-child(2) > label:nth-child(1) > input').click({multiple: true}) //clicking 1.50" for huber needle
            cy.get('#home_medication > tbody > tr:nth-child(5) > td:nth-child(2) > label:nth-child(1) > input').click({multiple: true}) //clicking 19 for huber gauge
            cy.get('[ng-if="data.VAAIT0127 && data.VAAIT0033"] > tbody > :nth-child(6) > :nth-child(2) > .global__txtbox').type('Test') //inputting flush solution
            cy.get('[ng-if="data.VAAIT0127 && data.VAAIT0033"] > tbody > :nth-child(7) > :nth-child(2) > .global__txtbox').type('3') //inputting days for flush frequency
            cy.get(':nth-child(7) > :nth-child(2) > .radio > .ng-pristine').click() //clicking done for flush frequency
            cy.get('.fg-line > .radio > .ng-pristine').click() //clicking site condition for observation
            cy.get(':nth-child(8) > [colspan="2"] > .fg-line > .global__txtbox').type('Test') //inputting WNL for observation
            cy.get(':nth-child(9) > [colspan="2"] > .fg-line > .global__txtbox').type('Test') //inputting intervention
            cy.wait(5000)
            //Intravenous Therapy
            cy.get(':nth-child(2) > :nth-child(2) > div.ng-scope > .cont-opt > .global__txtbox').type('Test') //inputting IV medication
            cy.get(':nth-child(2) > :nth-child(2) > div.ng-scope > .col-sm-1 > .btn').click() //clicking add for IV medication
            cy.wait(2000)
            cy.get(':nth-child(2) > .cont-opt > .global__txtbox').type('Test') //inputting new IV medication
            cy.get(':nth-child(3) > :nth-child(2) > div.ng-scope > .cont-opt > .global__txtbox').type('Test') //inputting IV FLUID THERAPHY
            cy.get(':nth-child(3) > :nth-child(2) > div.ng-scope > .col-sm-1 > .btn').click() //clicking add for FLUID THERAPHY
            cy.wait(2000)
            cy.get(':nth-child(3) > :nth-child(2) > :nth-child(2) > .cont-opt > .global__txtbox').type('Test') //inputting new FLUID THERAPHY
            cy.get(':nth-child(4) > :nth-child(2) > :nth-child(1) > .radio > .ng-pristine').click() //clicking IV Drip for administer via
            cy.get(':nth-child(4) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for administer via
            cy.get('#home_medication > tbody > tr:nth-child(4) > td:nth-child(2) > label > input').click({multiple:true}) //clicking done for administer via
            cy.get(':nth-child(5) > :nth-child(2) > .global__txtbox').type('Test') //inputting Flush procedure
            cy.get('[name="VAAIT0048"]').type('Test', {force: true}) //inputting Pre-infusion sol.
            cy.get('[name="VAAIT0049"]').type('Test', {force: true}) //inputting Post-infusion sol.
            cy.get('[colspan="3"] > div > .radio > .ng-pristine').click() //clicking Patient tolerated IV therapy well with no S/S of adverse reactions for observation
            cy.get(':nth-child(8) > [colspan="3"] > div > .global__txtbox').type('Test') //inputting observation
            cy.get(':nth-child(9) > [colspan="4"] > .global__txtbox').type('Test') //inputting intervention
            //DME
            cy.get('#dme > :nth-child(1) > :nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for walker
            cy.get('#dme > :nth-child(1) > :nth-child(1) > :nth-child(1) > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for cane
            cy.get(':nth-child(1) > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for wheelchair
            cy.get(':nth-child(2) > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Bedside commode	
            cy.get(':nth-child(2) > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Shower bench
            cy.get(':nth-child(2) > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Feeding pump
            cy.get(':nth-child(3) > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Suction machine
            cy.get(':nth-child(3) > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for O2 concentrator
            cy.get(':nth-child(3) > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Nebulizer machine
            cy.get(':nth-child(4) > .table-rt > tbody > :nth-child(2) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Hospital bed
            cy.get(':nth-child(4) > .table-rt > tbody > :nth-child(3) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for Low air loss mattress
            cy.get('tbody > :nth-child(4) > :nth-child(1) > .global__txtbox').type('Test') //inputting other for DME
            cy.get(':nth-child(4) > .table-rt > tbody > :nth-child(4) > :nth-child(2) > .radio > .ng-pristine').click() //clicking has for OTHER
            cy.get('#dme > :nth-child(1) > :nth-child(2) > td > .radio > :nth-child(1) > .ng-pristine').click() //clicking yes for DME working properly?	
            cy.get('td > .checkbox > .ng-isolate-scope > .ng-pristine').click() //clicking Malfunction reported to DME vendor
            cy.wait(5000)
            //Reasons for Home Health (Mark all that apply)
            cy.get(':nth-child(1) > td > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(4) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(5) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(3) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('#reasons_for_home_health > tbody > :nth-child(5) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            //Additional reasons for home health
            cy.get('#reasons_for_home_health > tbody > :nth-child(6) > td > .ng-pristine').type('Test')
            cy.wait(5000)
            //Homebound Status (Must meet the two criteria below)
            //Criterion One
            cy.get('#homebound_status > tbody > :nth-child(2) > td > :nth-child(1) > .ng-isolate-scope > .ng-pristine').click()
            //Criterion Two
            cy.get(':nth-child(3) > tbody > :nth-child(2) > td > .checkbox > label > .ng-scope').click()
            //Conditions that support the additional requirements in Criterion Two (select all)
            cy.get(':nth-child(4) > tbody > :nth-child(2) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > tbody > :nth-child(3) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > tbody > :nth-child(4) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > tbody > :nth-child(5) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(6) > :nth-child(1) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(7) > td > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > tbody > :nth-child(2) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get('[colspan="2"] > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > tbody > :nth-child(4) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(4) > tbody > :nth-child(5) > :nth-child(2) > .checkbox > label > .ng-scope').click()
            cy.get(':nth-child(6) > :nth-child(1) > :nth-child(1) > .p-0 > :nth-child(4) > tbody > :nth-child(6) > :nth-child(2) > .global__txtbox').type('Test') //inputting other for Conditions that support the additional requirements in Criterion Two
            cy.wait(5000)
            //Skilled Assessment and Observation
            cy.get('#DMEREQ0119').type('Test')
            //Skilled teaching and instructions
            cy.get('#DMEREQ0040').type('Test')
            //Patient/Caregiver Response to Skilled Interventions
            //For Patient
            cy.get('.b1-r > .radio > :nth-child(1) > .ng-pristine').click() //clicking full for Verbalized understanding
            cy.get(':nth-child(3) > :nth-child(2) > .radio > :nth-child(1) > .ng-pristine').click() //clicking good for Return demonstration performance
            cy.get('[style="margin-left:17px;"] > .ng-pristine').click() //clicking yes for Need follow-up teaching/instruction
            //For Caregiver
            cy.get(':nth-child(2) > :nth-child(3) > .radio > :nth-child(1) > .ng-pristine').click() //clicking full for Verbalized understanding
            cy.get(':nth-child(3) > :nth-child(3) > .radio > :nth-child(1) > .ng-pristine').click() //clicking good for Return demonstration performance
            cy.get('[style="margin-left:18px;"] > .ng-pristine').click() //clicking yes for Need follow-up teaching/instruction
            cy.wait(5000)
            //Care Coordination
            cy.get('#DMEREQ0131_chosen > .chosen-single').click() //clicking dropdown
            cy.get('#DMEREQ0131_chosen > div > ul > li:nth-child(1)').click() //clicking result
            cy.get('#DMEREQ0132').type('09232021') //inputting date
            cy.get('[style="width: 10%;"] > .global__txtbox').type('1200') //inputting hour
            cy.get('td > .fg-line > .form-control').type('Test') //inputting regarding
            cy.get('#DMEREQ0135').type('Test') //inputting Additional MD orders
            cy.get(':nth-child(4) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting Patient’s next physician visit
            //Case conference with (select all)
            cy.get('.b1-b > :nth-child(2) > :nth-child(1) > .ng-scope').click()
            cy.get('.b1-b > :nth-child(2) > :nth-child(2) > .ng-scope').click()
            cy.get('.b1-b > :nth-child(2) > :nth-child(3) > .ng-scope').click()
            cy.get('[style=" width:52px"] > .ng-scope').click()
            cy.get('[style=" width:125px"] > .ng-scope').click()
            cy.get(':nth-child(4) > .checkbox > :nth-child(1) > .ng-scope').click() // clicking EMR for via
            cy.get('#DMEREQ0140').type('09122021') //inputting date
            cy.get(':nth-child(5) > .m-l-15').type('1200') //inputting hour
            cy.get(':nth-child(6) > .b1-b > .radio > :nth-child(2) > .ng-pristine').click() // clicking yes for first visit
            cy.get('.b1-b > :nth-child(2) > .checkbox > :nth-child(1) > .ng-scope').click() // clicking CA Identification card	
            cy.get(':nth-child(2) > .checkbox > :nth-child(2) > .ng-scope').click() // clicking Medicare card
            cy.get(':nth-child(2) > .checkbox > :nth-child(3) > .ng-scope').click() // clicking CA Driver’s license
            cy.get('.m-l-10 > .global__txtbox').type('Test') //inputting other
            cy.get(':nth-child(7) > .p-0 > .table-rt > tbody > tr > :nth-child(2) > .global__txtbox').type('Test') //inputting Plan for next visit
            cy.wait(5000)
            cy.get(':nth-child(11) > :nth-child(1) > :nth-child(2) > td > .ng-pristine').type('Test') //inputting ptg narrative
            

            //Save Button
            cy.get('.btn__success').click() //clicking save button
            cy.wait(5000)
            // Care Management/Interventions TAB End -------------

        })
    })
