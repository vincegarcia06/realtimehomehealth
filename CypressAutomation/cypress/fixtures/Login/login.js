Given('I open Medisource Workflow login page', () => {
    cy.visit('https://qado.medisource.com/login'); //visit page   
  });
  
  When('I type in username and password', () => { 
        cy.get('#loginemail').type('vincent@geeksnest')
        cy.get('#loginpassword').type('Tester2021!')
    })
  
  When('I click on Login button', () => {
    cy.get('.btn').contains('Login').should('be.visible').click()
  });
  
  Then('It Redirect to Dashboard', () => {
    cy.url().should('be.equal', 'https://qado.medisource.com/dashboard')
  });