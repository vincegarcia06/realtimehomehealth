// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-file-upload'; //import the node package

//superagent@geekers | Tester2021@
Cypress.Commands.add('login', (name, password) => {       // command for login with session
  cy.session([name, password], () => {                    // session() restores a cached session
    cy.visit('https://app.medisource.com/login');
        cy.viewport(1920, 924);//setting your windows size
        cy.get('#loginemail').type(name);
        cy.get('#loginpassword').type(password);
        cy.get('.btn').click();
        cy.url().should('not.contain', '/login');
  })
})
//vincent@geeksnest | Tester2021!
Cypress.Commands.add('login1', (name, password) => {       // command for login with session
  cy.session([name, password], () => {                    // session() restores a cached session
    cy.visit('https://qado.medisource.com/login');
        cy.viewport(1920, 924);//setting your windows size
        cy.get('#loginemail').type(name);
        cy.get('#loginpassword').type(password);
        cy.get('.btn').click();
        cy.url().should('not.contain', '/login');
  })
})


Cypress.Commands.add('login3', (name, password) => {
  const args = { name, password }
  cy.session(
    args,
    () => {
      cy.origin('qado.medisource.com', { args }, ({ name, password }) => {
        cy.visit('https://qado.medisource.com/login');
        cy.viewport(1920, 924);//setting your windows size
        cy.get('#loginemail').type(name);
        cy.get('#loginpassword').type(password);
        cy.get('.btn').click();
        cy.url().should('not.contain', '/login');
      })
    }
  )
})
